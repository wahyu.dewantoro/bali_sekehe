<div class="my-3 p-3 bg-white rounded box-shadow">
 <form class="form-inline" id="asd" method="get" action="<?= base_url().'result/rank'?>">
	<select name="size" id="size" class="form-control mb-2 mr-sm-2" >
	  	<?php foreach($size as $size){?>
	  		<option <?php if($size->min.'_'.$size->max==$vsize){echo "selected";}?> value="<?= acak($size->min.'_'.$size->max) ?>"><?= $size->label?></option>
	  	<?php } ?>
	</select>
	<select name="class" id="class" class="form-control mb-2 mr-sm-2" >
		<option value="<?= acak('all')?>">All class</option>
		<?php foreach($kelas as $kelas){?>
			<option <?php if($kelas->kat_ikan==$vclass){echo "selected";}?> value="<?=  acak($kelas->kat_ikan )?>"><?= $kelas->alias?></option>
		<?php } ?>
	</select> 
	<select name="variety" id="variety" class="form-control mb-2 mr-sm-2" >
		<option value="<?= acak('all')?>">All variety</option>
		<?php foreach($variety as $variety){?>
		<option <?php if($variety->nm_ikan==$vvariety){echo "selected";}?> value="<?= urlencode($variety->nm_ikan) ?>"><?= $variety->nm_ikan ?></option>
		<?php } ?>
	</select> 
</form>
<?php 
	if($vclass<>'all'){
		$this->db->where('kat_ikan',$vclass);	
	}

	if($vvariety<>'iamp' ){
		$this->db->where('nm_ikan',$vvariety);
	}

	$xz=explode('_',$vsize);
	$list=$this->db->get('ms_kategoriikan')->result();
?>

	<div class="row">
		<?php foreach($list as $list){?>
		<div class="col-12">
			<p class="text-center"><b ><?= $list->nm_ikan?></b></p><hr>
			<div class="row">
				<?php 
					$aikan=$this->db->query("SELECT no_ikan,ukuran,getvariety(ms_kat_id) variety,getownerkota(ms_peserta_id) owner,gethandlinkota(ms_handling_id) handling,ukuran,nama_juara,gambar_ikan FROM tb_juara_kontes a JOIN tb_ikan b ON a.tb_ikan_id =b.id_inc JOIN tb_peserta c ON c.id_inc=b.tb_peserta_id JOIN ms_juara d ON d.id_inc =a.ms_juara_id WHERE ms_juara_id in (1,2,3) and  getvariety(ms_kat_id)='".$list->nm_ikan."' AND (ukuran >= ".$xz[0]." AND ukuran <=".$xz[1]." )")->result();
					foreach($aikan as $ri){
				?>
				<div class="col-6 col-md-4">
					<div class="text-center">
						<?php 
						$img=base_url().'noimage.png';
						if(file_exists(dirname(dirname(dirname(dirname(__FILE__)))) ."/".$ri->gambar_ikan)) { 
							$img=base_url().$ri->gambar_ikan;
						}?>
					  <img src="<?= $img ?>" class="img-thumbnail img-fluid" alt="<?= $ri->nama_juara ?>">
					  <table>
					  		<tr>
					  			<td><b><?= $ri->nama_juara?></b></td>
					  		</tr>
					  		<tr>
					  			<td>#<?= $ri->no_ikan?> - <?= $ri->ukuran ?> CM</td>
					  		</tr>
					  		<tr>
					  			<td><b>Owner</b><br>
					  				<?= $ri->owner?></td>
					  		</tr>
					  		<tr>
					  			<td><b>Handling</b><br>
					  				<?= $ri->handling?></td>
					  		</tr>
					  </table>
					</div>
				</div>
			<?php } ?>
			</div>
			<hr>
		</div>
	<?php } ?>
	</div>	
</div>

 