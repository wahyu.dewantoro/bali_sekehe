<div class="row">
	<div class="col-md-4 col-xs-6">
		<div class="pull-right">
			<img style="height:170px;max-width:120px;width: expression(this.width > 120 ? 120: true);"   src="<?php echo base_url().$rk->gambar_ikan?>">	
		</div>
	</div>
	<div class="col-md-8 col-xs-6">
		<table  style="font-size:12px">
		<tr>
			<td>ID Ikan</td>
			<td>:</td>
			<td><?= $rk->no_ikan?></td>
		</tr>
		<tr>
			<td>Owner</td>
			<td>:</td>
			<td><?= $rk->nama_peserta?> - <?= $rk->kota_peserta?></td>
		</tr>
		<tr>
			<td>Handling</td>
			<td>:</td>
			<td><?= $rk->nama_handling?> - <?= $rk->kota_handling?></td>
		</tr>
		
		<tr>
			<td>Variety</td>
			<td>:</td>
			<td><?= $rk->nm_ikan?></td>
		</tr>
		<tr>
			<td>Size</td>
			<td>:</td>
			<td><?= $rk->ukuran?> cm</td>
		</tr>
		<tr>
			<td>Breeder</td>
			<td>:</td>
			<td><?= $rk->asal?></td>
		</tr>
		<tr>
			<td>Gender</td>
			<td>:</td>
			<td><?= $rk->gender?></td>
		</tr>
		<tr>
			<td colspan="3"><a class="btn btn-xs purple" href="<?php echo base_url().'juarakontes/editikan/'.$idikan?>" data-target="#ajax" data-toggle="modal"><i class="fa fa-edit"></i> Edit</a></td>
		</tr>
	</table>		
	</div>
</div>