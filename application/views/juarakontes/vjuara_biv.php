<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            Assessment
            <i class="fa fa-circle"></i>
        </li>
        
        <li>
            <span><?php echo $button ?></span>
        </li>
    </ul>
</div>
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="fa fa-file-text font-green-haze"></i>
            <span class="caption-subject bold"> <?php echo $button ?></span>
        </div>
    </div>
    <div class="portlet-body form">
       <form class="form-inline" method="post" action="<?= $action ?>">
            <div class="form-group">
                <label >Ikan</label>
              </div>
              <div class="form-group">
                    <select class="form-control select2"  data-placeholder="silahkan pilih ikan" required name="tb_ikan_id" id="tb_ikan_id" >
                        <option value=""></option>   
                        <?php foreach($no_ikan as $rn){?>
                        <option value="<?php echo $rn->id?>"><?php echo $rn->no_ikan?> - <?php echo $rn->nm_jenis.' ['.$rn->ukuran.'] '?></option>
                        <?php }?> 
                    </select>
              </div>
              <button type="submit" class="btn btn-success">Submit</button>
              <?= anchor('juarakontes/resetBiv','Reset','class="btn   btn-danger"');?>
       </form>
    </div>
</div>
<div class="row">
    <?php foreach($biv as $rk){?>
        <div class="col-md-4">
            <div class="portlet light bordered">
                <div class="portlet-body">
                    <table>
                        <tr valign="top">
                            <td width="60%" align="left">
                                <img style="height:170px;width:120px;"  src="<?php echo base_url().$rk->gambar_ikan?>">
                            </td>
                            <td>
                                <table class=" ">
                                    <tbody>
                                        <tr>
                                            <td width="30%">Variety</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $rk->nm_jenis?></td>
                                        </tr>
                                        <tr>
                                            <td>Ukuran</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $rk->ukuran?></td>
                                        </tr>
                                        <tr>
                                            <td>Gender</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $rk->gender?></td>
                                        </tr>
                                        <tr>
                                            <td>Breeder</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $rk->asal?></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <a class="btn btn-sm btn-warning" href="<?php echo base_url().'kontes/viewikandetail/'.$rk->tb_ikan_id?>" data-target="#ajax" data-toggle="modal"><i class="fa fa-binoculars"></i> </a>
                                <?= anchor('juarakontes/simpanBiv?tb_ikan_id='.$rk->tb_ikan_id,'Pilih BIV','class="btn btn-sm btn-success"  onclick="javasciprt: return confirm(\'apakah anda yakin  ?\')"  ')  ?>
                            </td>
                            
                        </tr>
                    </table>
                  </div>
            </div>
        </div>
    <?php } ?>
</div>
 <div class="modal fade" id="ajax" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                
                <span><i class="fa fa-refresh"></i> &nbsp;&nbsp;Loading... </span>
            </div>
        </div>
    </div>
</div>
      