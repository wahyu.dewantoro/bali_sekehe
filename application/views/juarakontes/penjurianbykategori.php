<style type="text/css">
    .ribbon {
   position: absolute;
   right: 15px; 
   top: -15px;
   z-index: 1;
   overflow: hidden;
   width: 75px; height: 75px; 
   /*width: auto;*/
   text-align: right;
}
.ribbon span {
   font-size: 10px;
   color: #fff; 
   /*text-transform: capitalize; */
   text-align: center;
   font-weight: bold; 
   line-height: 20px;
   /*transform: rotate(0deg);*/
   width: 100%; 
   display: block;
   background: #79A70A;
   background: linear-gradient(#9BC90D 0%, #79A70A 100%);
   box-shadow: 0 3px 10px -5px rgba(0, 0, 0, 1);
   position: absolute;
   top: 19px; 
   /*right: -21px;*/
}
 

</style>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            Assessment
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Penjurian Kategori</span>
        </li>
    </ul>
    <div class="page-toolbar">
          <div class="pull-right">
              <form role="form">
              <div class="form-group">
                <select class="form-control select2" id='cariikan' name="id_ikan" data-placeholder="Pilih ID Ikan">
                    <option value=""></option>
                    <!-- <option value="1">Ikan 1</option> -->
                    <?php foreach($cariikan as $rci){ ?>
                    <option value="<?php echo $rci->id_inc?>"><?php echo $rci->no_ikan.' - '.$rci->nm_ikan?></option>
                    <?php } ?>
                </select>
              </div>
               
            </form>
          </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <form class="form-inline" method="post" action="<?php echo base_url().'juarakontes/noikan.html'?>" role="form">
          <div class="form-group">
            <select class="form-control" id="vukuran" name="ukuran" required data-placeholder="Ukuran ikan">
                <option value="">Ukuran </option>
                <?php foreach ($ukuran as $uk) { ?>
                <option <?php if($vk== $uk->MIN.'_'.$uk->MAX){echo "selected";}?> value="<?php echo $uk->MIN.'_'.$uk->MAX?>"><?php echo $uk->MIN.' - '.$uk->MAX.' cm'?></option>
                <?php } ?>
            </select>
          </div>
          <div class="form-group">
                <select class="form-control  " id="varietyv" name="ms_kat_id" required data-placeholder="Variety">
                        <option value="">Variety</option>
                        <?php foreach($jenis as $jn){ ?>
                        <option <?php  if($vj==$jn->id_inc){ echo 'selected'; }?> value="<?php echo $jn->id_inc?>"><?php echo $jn->nm_ikan?></option>
                        <?php } ?>
                </select>
          </div>
          <button type="submit" id="cek" name="cek" class="btn purple"><i class="fa fa-search"></i> Tampilkan</button>
          <?php if($vk <> '' && $vj <> '' ){?>
          <button type="submit" name="reset" class="btn red"><i class="fa fa-refresh"></i> Hapus Juara</button>
        <?php } ?>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <?php if(isset($ikan)){ ?>
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Data</div>
            </div>
            <div class="portlet-body">
                <div class="mt-element-overlay">
                <div class="row">
                    <?php  foreach($ikan as $ikan){ ?>
                     <div class="col-lg-2 col-md-3 col-sm-6 col-xs-6">
                        <a href="#" class="ikan" data-id="<?php echo $ikan->id_inc?>">
                        <div class="thumbnail">
                           
                            <div id="juara<?php echo $ikan->id_inc?>"><?php $this->Mjkontes->juarabyikan($ikan->id_inc); ?></div>
                             
                             
                            <img  style="margin: auto; height:170px;max-width:120px;width: expression(this.width > 120 ? 120: true);"  src="<?php echo base_url().$ikan->gambar_ikan?>">
                            
                           <div class="caption">

                                <table style="font-size:10px">
                                    <tbody>
                                   
                                        <tr>
                                            <td>ID Ikan</td>
                                            <td>:</td>
                                            <td><?= $ikan->no_ikan?></td>
                                        </tr>
                                        <tr>
                                            <td>Ukuran</td>
                                            <td>:</td>
                                            <td><?= $ikan->ukuran?> cm</td>
                                        </tr>
                                        <tr>
                                            <td>Gender</td>
                                            <td>:</td>
                                            <td><?= $ikan->gender?></td>
                                        </tr>
                                        <tr>
                                            <td>Breeder </td>
                                            <td>:</td>
                                            <td><?= $ikan->asal ?></td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                           </div>
                        </div>
                      </a>
                    </div>
                
                   <?php  } ?>
                </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Detail Ikan</h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    
                </div>
            </div>
        </div>

 


    
<div class="modal fade" id="ajax" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                
                <span><i class="fa fa-refresh"></i> &nbsp;&nbsp;Loading... </span>
            </div>
        </div>
    </div>
</div>

 <script type="text/javascript">
  $(document).ready(function(){


     $('#vukuran,#varietyv').change(function(){
      // alert('asf');
         $('#cek').click();
      });
  });


    $('#cariikan').on('change', function() {
       
      $("#myModal").modal('show');
      $.post('<?php echo base_url()."juarakontes/cariikan"?>',
          {id:$('#cariikan').val()},
          function(html){
              $(".modal-body").html(html);
          }   
      );
    })
    

    

     $(function(){
            $(document).on('click','.ikan',function(e){
                e.preventDefault();
                    var ikan=$(this).attr('data-id');
                    $.post('<?php echo base_url()."juarakontes/aksipenjurianbykategori"?>',
                    {id:$(this).attr('data-id')},
                    function(html){
                        // $(".modal-body").html(html);
                        // alert('#juara'+ikan);
                    $('#juara'+ikan).html(html);
                        // alert(html);
                    }   
                );
            });
        });
 </script>

