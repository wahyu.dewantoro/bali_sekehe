        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    Penjurian
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Data Juara</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div class="btn-group pull-right">
                                    <?php echo anchor(site_url('juarakontes/create'), '<i class="fa fa-plus"></i> Tambah','class="btn purple btn-sm btn-outline"'); ?>
                                </div>
                            </div>
                        </div>
        <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-list"></i>Data</div>
                                    </div>
                                    <div class="portlet-body">
                                        
        <table class="table table-striped table-bordered table-hover" id="tb" >
            <thead>
                <tr>
                    <th width="5%">No</th>
                    <th width="10%">No Ikan</th>
                    <th>Variety</th>
                    <th>Ukuran</th>
                    <th>Breder</th>
                    <th>Juara</th>
		    <th>Aksi</th>
                </tr>
            </thead>
	    <tbody>
            <?php
            $start = 0;
            foreach ($juarakontes_data as $juarakontes)
            {
                ?>
                <tr>
		    <td align='center'><?php echo ++$start ?></td>
            <td align="center"> <span class="badge"><?= $juarakontes->no_ikan?> </span></td>
            <td><?= $juarakontes->nm_ikan?> <small>(<?= $juarakontes->gender?>)</small></td>
            <td><?= $juarakontes->ukuran ?> </td>
            <td><?= $juarakontes->asal?> </td>
            
            
            <td><?= ucwords($juarakontes->nama_juara)?> </td>
		    <td style="text-align:center" width="10%">
            <a class="btn btn-xs green" href="<?php echo base_url().'kontes/viewikandetail/'.$juarakontes->id_ikan?>" data-target="#ajax" data-toggle="modal"><i class="fa fa-binoculars"></i> </a>
            <?php 
			 echo anchor(site_url('juarakontes/delete/'.$juarakontes->id_inc),'<i class="fa fa-trash"></i>','onclick="javasciprt: return confirm(\'apakah anda yakin hapus ?\')" class="btn btn-xs btn-danger"'); 
			?></td>
	        </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
       
    </div>
</div>
 <div class="modal fade" id="ajax" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                
                <span><i class="fa fa-refresh"></i> &nbsp;&nbsp;Loading... </span>
            </div>
        </div>
    </div>
</div>
      