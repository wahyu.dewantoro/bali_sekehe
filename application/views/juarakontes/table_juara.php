<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            Penjurian
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Data Juara</span>
        </li>
    </ul>
    <div class="page-toolbar">
        <!--  -->
    </div>
</div>
<!-- looping ukuran -->
<?php foreach($ukuran as $ukuran){?>
    <h2 class="text-center"><u><?= $ukuran->min.' - '.$ukuran->max.' cm' ?></u></h2>
    <?php for($kat=1;$kat<=2;$kat++){?>
      <b>Kelas  <?php if($kat==1){ echo "(A)"; $vk='A'; }else if($kat==2){echo "(B)"; $vk='B';} else if($kat==3){echo "(C)"; $vk='C';}else{echo "D"; $vk='D';} ?></b>
        <div class="table-responsive">
            <table border="1" cellpadding="3" cellspacing="0">
                 <thead>
                     <tr>
                         <th>Juara </th>
                         <?php $rr=$this->db->query("SELECT nm_ikan,id_inc FROM ms_kategoriikan WHERE kat_ikan='".$vk."' ORDER BY sort ASC")->result_array(); 
                         foreach($rr as $rr){?>
                            <th width="100px"><?= $rr['nm_ikan'] ?></th>
                         <?php } ?>
                     </tr>
                 </thead>
                 <tbody>
                     <?php for($u=1; $u<=5;$u++){?>
                        <tr>
                            <td class="text-center"><?= $u ?></td>
                            <?php 
                            $rra=$this->db->query("SELECT nm_ikan,id_inc FROM ms_kategoriikan WHERE kat_ikan='".$vk."' ORDER BY sort ASC")->result();
                            foreach($rra as $rra){
                                $nomer=$this->db->query("SELECT get_no_juara(".$rra->id_inc.",".$u.",".$ukuran->min.",".$ukuran->max.") nomer")->row()->nomer;
                                ?>
                            <td class="text-center"><?= empty($nomer)?'':$nomer; ?></td>
                         <?php } ?>
                        </tr>
                    <?php } ?>
                 </tbody>
             </table>
         </div>
    <?php } ?>
 <?php } ?>