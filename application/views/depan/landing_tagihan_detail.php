
 <div class="my-3 p-3 bg-white rounded box-shadow">
  <h6 class="border-bottom border-gray pb-2 mb-0">Detail Tagihan : <?= $no_pendaftaran ?></h6>
   <div class="pull-right">
  <div class="btn-group text-r" role="group" aria-label="Basic example">
      <a href="<?= base_url().'landing'?>" class="btn btn-sm btn-success" ><i class="fa fa-home"></i> Home</a>
      <a href="<?= $_SERVER['HTTP_REFERER']?>" class="btn btn-sm btn-primary" ><i class="fa fa-list"></i> Kembali</a>
      
    </div>
  </div>
    <div class="table-responsive-sm">
   <table class="table table-bordered table-striped">
           <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th>Variety</th>
                                        <th>Ukuran</th>
                                        <th>Asal</th>
                                        <th>Gender</th>
                                        <th>Biaya</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(count($listikan)>0){
                                    $total=0;
                                     $no=1; foreach($listikan as $li){?>
                                    <tr>
                                        <td style="text-align: center;"><?= $no; ?></td>
                                        <td><?= $li->nm_jenis; ?></td>
                                        <td style="text-align: center;"><?= $li->ukuran; ?> cm</td>
                                        <td><?= $li->asal; ?></td>                                                            
                                        <td><?= $li->gender; ?></td>
                                        <td style="text-align: right"><?php echo number_format($li->biaya,'0','','.')?></td>
                                        
                                    </tr>
                                    <?php $total+=$li->biaya;  $no++; } } else{ $total=0;?>
                                    <tr>
                                       <td style="text-align: center;" colspan="6">Tidak ada data !</td> 
                                    </tr>
                                    <?php } ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="5">Total</th>
                                        <td style="text-align: right"><?php echo number_format($total,'0','','.')?></td>
                                        
                                    </tr>
                                </tfoot>
   </table> 
  </div>
  
</div>

