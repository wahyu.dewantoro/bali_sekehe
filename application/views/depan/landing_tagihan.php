 <div class="my-3 p-3 bg-white rounded box-shadow">
   <h6 class="border-bottom border-gray pb-1 mb-0">Data Ikan <a href="<?= base_url().'landing'?>" class="btn btn-sm btn-success" ><i class="fa fa-home"></i> Home</a></h6>
  <ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item"> 
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Belum Lunas </a> 
  </li> 
  <li class="nav-item"> 
    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Lunas</a> 
  </li>
</ul>

<div class="tab-content" id="myTabContent">

  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

      <!-- belum bayar -->

      <div class="table-responsive">
          <table class="table table-bordered table-striped">

              <thead>

            <tr>
                <th rowspan="2" width="5%">No</th> 
                <th rowspan="2">No Pendaftaran</th> 
                <th colspan="2">Owner</th> 
                <th rowspan="2">Ikan</th> 
                <th rowspan="2">Total</th>
            </tr>
            <tr>
                <th>Nama</th>
                <th>Kota</th>
            </tr>
        </thead>
        <tbody>
            <?php $no=1; foreach($grg as $rt){?>
                <tr  id="<?= $rt->no_pendaftaran ?>">

                    <td style="text-align: center"><?= $no ?></td>

                    <td><a href="<?= base_url().'landing/detailBAyar/'.$rt->no_pendaftaran ?>"> <?= $rt->no_pendaftaran ?></a><span class="badge <?php if($rt->st_entri=='online'){ echo " badge-success";}else{echo " badge-primary";}?>"><?= $rt->st_entri ?></span></td>

                    <td><?= $rt->nama_owner?></td>

                    <td><?= $rt->kota_owner?></td>

                    <td style="text-align: center;"><?= $rt->ikan?> ekor</td>

                    <td style="text-align: right"><?= number_format($rt->jumlah,0,'','.');?></td>

            
                </tr>

            <?php $no++; } ?>

        </tbody>

          </table>

      </div>

  </div>

  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

    <!-- sudah bayar -->

    <div class="table-responsive">

          <table class="table table-bordered table-striped">

              <thead>

                <tr>

                  <th>owner</th>

                  <th>variety</th>

                  <th>gender</th>

                  <th>ukuran</th>

                  <th>biaya</th>

                </tr>

              </thead>

              <tbody>

                <?php $tac=0; foreach($lunas as $lunas){ $tac+=$lunas->biaya;?>

                  <tr>

                    <td><?= $lunas->owner ?></td>

                    <td><?= $lunas->variety ?></td>

                    <td><?= $lunas->gender ?></td>

                    <td style="text-align: center"><?= $lunas->ukuran ?> cm</td>

                    <td style="text-align: right"><?= number_format($lunas->biaya,0,'','.') ?></td>

                  </tr>

                <?php } ?>

              </tbody>

              <tfoot>

                <tr >

                  <td colspan="4"><b>Total</b></td>

                  <td style="background-color: red; text-align: right"><?= number_format($tac,0,'','.') ?></td>

                </tr>

              </tfoot>

          </table>

      </div>

  </div>

</div>

</div>



