<div class="my-3 p-3 bg-white rounded box-shadow">
  <h6 class="border-bottom border-gray pb-2 mb-0">Registrasi Account</h6>
  <form method="post" action="<?= base_url().'home/prosessignup' ?>">
      <div class="form-group">
          <label >Nama</label>
          <input type="text" name="nama" id="nama" class="form-control" required>
      </div>
      <div class="form-group">
          <label >Kota</label>
          <input type="text" name="kota" id="kota" class="form-control" required>
      </div>
      <div class="form-group">
          <label >Telp</label>
          <input type="number" name="telp"  onkeypress="return isNumberKey(event)"  id="telp" class="form-control" required>
      </div>
      <div class="form-group">
          <label >Email</label>
          <input type="email" name="email" id="email" class="form-control" required>
          <span id="notifemail"></span>
      </div>
      <div class="form-group">
          <label >Password</label>
          <input type="password" name="password" id="password" class="form-control" required>
      </div>
      <div class="form-group">
          <label >Confirm</label>
          <input type="password" name="confirm" id="confirm" class="form-control" required>
          <span id="message"></span>
      </div>
      <div class="form-group">
        <button id="btn" type="submit" class="btn btn-sm btn-info"><i class="fa fa-save"></i> Submit</button>
        <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-retweet"></i> Reset</button>
      </div>
  </form>
</div>

