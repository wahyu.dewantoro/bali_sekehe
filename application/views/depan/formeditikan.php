 <div class="my-3 p-3 bg-white rounded box-shadow">
  <h6 class="border-bottom border-gray pb-2 mb-0">Edit Ikan </h6>
   <div class="pull-right">
  <div class="btn-group text-r" role="group" aria-label="Basic example">
      
    </div>
  </div>
  <form method="post" action="<?= $action ?>"  enctype="multipart/form-data">
      <div class="form-group">
          <select class="form-control" name="ms_peserta_id" required="">
              <option value="">Pilih Owner</option>
              <?php foreach($owner as $owner){?>
                <option <?php if($ikan->ms_peserta_id==$owner->id_inc){echo "selected"; }?> value="<?= $owner->id_inc ?>"><?= $owner->nama.' - '.$owner->kota ?></option>
              <?php } ?>
          </select>
      </div>
      <div class="form-group">
        <img id="blah" src="<?=  $ikan->gambar_ikan<>''?base_url().$ikan->gambar_ikan:base_url().'noimage.png';?>" width="50%" alt="your image" />
        <input type="hidden" name="img_lawas" value="<?= $ikan->gambar_ikan ?>">
        <input type="file" accept="img/*"  name="gambar" id="gambar"  class="form-control">
      </div>
      <div class="form-group">
          <select name="ms_kat_id" class="form-control" id="ms_kat_id" required="" >
            <option value="">Variety</option>
            <?php foreach($variety as $var){?>
              <option <?php if($ikan->ms_kat_id==$var->id_inc){echo "selected";}?> value="<?= $var->id_inc ?>"><?= $var->nm_ikan?></option>
            <?php } ?>
          </select>
      </div>
      <div class="form-group">
          <div class="input-group mb-3">
            <input type="number" class="form-control" placeholder="ukuran" value="<?= $ikan->ukuran?>" name="ukuran" id="ukuran"  onkeypress="return isNumberKey(event)"> 
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1">cm</span>
            </div>
          </div>
      </div>
      <div class="form-group">

          <select name="gender" class="form-control" id="gender" required="" >
            <option value="">Female/Male</option>
            <option <?php if($ikan->gender=='Female'){echo "selected";}?> value="Female">Female</option>
            <option <?php if($ikan->gender=='Male'){echo "selected";}?> value="Male">Male</option>
          </select>
      </div>
      <div class="form-group">
          <select name="asal" class="form-control" id="asal" required="" >
            <option value="">Lokal/Import</option>
            <option <?php if($ikan->asal=='Lokal'){echo "selected";}?> value="Lokal">Lokal</option>
            <option <?php if($ikan->asal=='Import'){echo "selected";}?> value="Import">Import</option>
          </select>
      </div>


          <input value="<?= $ikan->id_inc?>" type="hidden" name="id_inc" id="id_inc" class="form-control" placeholder="id_inc" required="">
      <div class="form-group">
          <button  class="btn btn-sm btn-info" type="submit"><i class="fa fa-save"></i> Submit</button>
          <button class="btn btn-sm btn-warning" type="reset"><i class="fa fa-retweet"></i> Reset</button>
          <a href="<?= base_url().'landing'?>" class="btn btn-sm btn-success" ><i class="fa fa-home"></i> Home</a>
      </div>
  </form>
</div>
<script type="text/javascript">
$( document ).ready(function() {
    // console.log( "ready!" );
    $("#gambar").change(function(){
        readURL(this);
        // alert('asfasf');
    });


    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }else{
          $('#blah').attr('src', "<?= base_url()?>noimage.png");
        }
    }
    
});
</script>