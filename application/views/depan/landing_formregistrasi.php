 <div class="my-3 p-3 bg-white rounded box-shadow">
  <ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Cart Ikan</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Tambah Ikan</a>
  </li>
  
</ul>
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
      <?php if(empty($ikan)){?>
        <h3>Data ikan kosong !<br><?= anchor('landing','<i class="fa fa-home"></i> Home','class="btn btn-sm btn-success"'); ?>    </h3>
      <?php }else{ ?>
        

        <p class="text-right">
          <div class="btn-group">
            <?= anchor('landing','<i class="fa fa-home"></i> Home','class="btn btn-sm btn-success"'); ?>    
            <?= 
          anchor('landing/selesaicart/'.$handling,'<i class="fa fa-check"></i> Selesai','class="btn btn-primary btn-sm" onclick="return confirm(\'Apakah anda yakin?\')"'); ?>
          </div>

          
        </p>
        <?php foreach($ikan as $ikan){ ?>

        <ul class="list-unstyled">
          <li class="media">
            <img class="mr-3" src="<?= base_url().$ikan->gambar_ikan ?>" alt="Generic placeholder image" width="60px">
            <div class="media-body">
              <table border="0">
                <tr>
                  <td>  <?=  anchor('landing/hapusikanhandling/'.$ikan->id_inc,'<i class="fa fa-trash"></i>','class="badge badge-danger" onclick="return confirm(\'Apakah anda akan menghapus ikan?\')"')?>
                      
                     <?=  anchor('landing/editikanhandling/'.$ikan->id_inc,'<i class="fa fa-edit"></i>','class="badge badge-success"')?>
                  </td>
                </tr>
                <tr>
                  <td>Owner : <?= $ikan->owner ?></td>
                </tr>
                <tr>
                  <td><?= $ikan->variety.' - '.$ikan->ukuran.' cm' ?></td>
                </tr>
                <tr>
                  <td><?= $ikan->gender.' - '.$ikan->asal ?></td>
                </tr>
                <tr>
                  
                </tr>
              </table>

                

            </div>
          </li>
        
        </ul>
      <?php } } ?>
  </div>
  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
      
   
  <form method="post" action="<?= $action ?>" enctype="multipart/form-data">
      <div class="form-group">
          <select class="form-control" name="ms_peserta_id" required="">
              <option value="">Pilih Owner</option>
              <?php foreach($owner as $owner){?>

                <option value="<?= $owner->id_inc ?>"><?= $owner->nama.' - '.$owner->kota ?></option>
              <?php } ?>
          </select>
      </div>
      <div class="form-group">
        <img id="blah" src="<?= base_url().'noimage.png'?>" width="50%" alt="your image" />
        <input type="file" accept="img/*" name="gambar" id="gambar" required="" class="form-control">
      </div>
      <div class="form-group">
          <select name="ms_kat_id" class="form-control" id="ms_kat_id" required="" >
            <option value="">Variety</option>
            <?php foreach($variety as $var){?>
              <option value="<?= $var->id_inc ?>"><?= $var->nm_ikan?></option>
            <?php } ?>
          </select>
      </div>
      <div class="form-group">
          <div class="input-group mb-3">
            <input type="number" class="form-control" placeholder="ukuran"  name="ukuran" id="ukuran"  onkeypress="return isNumberKey(event)"> 
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1">cm</span>
            </div>
          </div>
      </div>
      <div class="form-group">

          <select name="gender" class="form-control" id="gender" required="" >
            <option value="">Female/Male</option>
            <option value="Female">Female</option>
            <option value="Male">Male</option>
          </select>
      </div>
      <div class="form-group">
          <select name="asal" class="form-control" id="asal" required="" >
            <option value="">Lokal/Import</option>
            <option value="Lokal">Lokal</option>
            <option value="Import">Import</option>
          </select>
      </div>
      <div class="form-group">
          <button  class="btn btn-sm btn-info" type="submit"><i class="fa fa-save"></i> Submit</button>
          <button class="btn btn-sm btn-warning" type="reset"><i class="fa fa-retweet"></i> Reset</button>
          <a href="<?= base_url().'landing'?>" class="btn btn-sm btn-success" ><i class="fa fa-home"></i> Home</a>
      </div>
  </form>
  </div>
</div>


  
</div>

 

<script type="text/javascript">
$( document ).ready(function() {
    // console.log( "ready!" );
    $("#gambar").change(function(){
        readURL(this);
        // alert('asfasf');
    });


    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }else{
          $('#blah').attr('src', "<?= base_url()?>noimage.png");
        }
    }
    
});
</script>