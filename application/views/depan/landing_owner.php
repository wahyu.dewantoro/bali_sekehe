 <div class="my-3 p-3 bg-white rounded box-shadow">
  <h6 class="border-bottom border-gray pb-2 mb-0">Data Owner </h6>
   <div class="pull-right">
  <div class="btn-group text-r" role="group" aria-label="Basic example">
      <a href="<?= base_url().'landing'?>" class="btn btn-sm btn-success" ><i class="fa fa-home"></i> Home</a>
      <a href="<?= base_url().'landing/tambahOwner'?>" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah</a>
      
    </div>
  </div>
    <div class="table-responsive-sm">
   <table class="table table-bordered table-striped">
     <thead>
       <tr>
         <th width="3%">No</th>
         <th>Nama</th>
         <th>Kota</th>
         <th></th>
       </tr>
     </thead>
     <tbody>
       <?php $no=1;
       if($owner){
       foreach($owner as $ro){?>
       <tr>
         <td style="text-align: center;"><?= $no ?></td>
         <td><?= $ro->nama?></td>
         <td><?= $ro->kota?></td>
         <td style="text-align: center;"><div class="btn  btn-group">
          <?= anchor('landing/editowner/'.urlencode($ro->id_inc),'<i class="fa fa-edit"></i>','class="badge badge-info"')?>

          <?php if($ro->jum==0){ echo anchor('landing/hapusowner/'.urlencode($ro->id_inc),'<i class="fa fa-trash"></i>','onclick="return confirm(\'apakah anda akan menghapus '.$ro->nama.' ?\')" class="badge badge-danger"');} ?> 

       </div></td>
       </tr>
       <?php $no++;}}else{ ?>
        <tr>
          <td align="center" colspan="4">Data kosong</td>
        </tr>
       <?php } ?>
     </tbody>
   </table> 
  </div>
  
</div>