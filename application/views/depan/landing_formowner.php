 <div class="my-3 p-3 bg-white rounded box-shadow">
  <h6 class="border-bottom border-gray pb-2 mb-0">Form Owner </h6>
   <div class="pull-right">
  <div class="btn-group text-r" role="group" aria-label="Basic example">
      
    </div>
  </div>
  <form method="post" action="<?= $action ?>">

      <div class="form-group">
          <label>Nama <span>*</span></label>
          <input value="<?= $nama?>" type="text" name="nama" id="nama" class="form-control" placeholder="nama" required="">
      </div>
      <div class="form-group">
          <label>Kota <span>*</span></label>
          <input value="<?= $kota?>" type="text" name="kota" id="kota" class="form-control" placeholder="kota" required="">
      </div>
      
          <input value="<?= $pengguna_id?>" type="hidden" name="pengguna_id" id="pengguna_id" class="form-control" placeholder="pengguna_id" required="">
          <input value="<?= $id_inc?>" type="hidden" name="id_inc" id="id_inc" class="form-control" placeholder="id_inc" required="">
      <div class="form-group">
          <button  class="btn btn-sm btn-info" type="submit"><i class="fa fa-save"></i> Submit</button>
          <button class="btn btn-sm btn-warning" type="reset"><i class="fa fa-retweet"></i> Reset</button>
          <a href="<?= base_url().'landing'?>" class="btn btn-sm btn-success" ><i class="fa fa-home"></i> Home</a>
      </div>
  </form>
</div>