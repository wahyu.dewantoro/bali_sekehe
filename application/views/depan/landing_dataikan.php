 <div class="my-3 p-3 bg-white rounded box-shadow">
    <h6 class="border-bottom border-gray pb-2 mb-0">Data Ikan <a href="<?= base_url().'landing'?>" class="btn btn-sm btn-success" ><i class="fa fa-home"></i> Home</a></h6>
    
     <?php foreach($ikan as $ikan){ ?>

        <ul class="list-unstyled">
          <li class="media">
            <img class="mr-3" src="<?= base_url().$ikan->gambar_ikan ?>" alt="Generic placeholder image" width="60px">
            <div class="media-body">
              <table border="0">
                <tr>
                  <td>Owner : <?= $ikan->owner ?></td>
                </tr>
                <tr>
                  <td><?= $ikan->variety.' - '.$ikan->ukuran.' cm' ?></td>
                </tr>
                <tr>
                  <td><?= $ikan->gender.' - '.$ikan->asal ?></td>
                </tr>
                <tr>
                  
                </tr>
              </table>

                

            </div>
          </li>
        
        </ul>
      <?php }  ?>  
  
</div>