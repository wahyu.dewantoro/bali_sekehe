<!doctype html>
<html lang="en">
  
<!-- Mirrored from getbootstrap.com/docs/4.0/examples/offcanvas/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 02 May 2019 13:48:09 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://getbootstrap.com/favicon.ico">

    <title>Koi Show</title>

    <link rel="canonical" href="index.html">

    <!-- Bootstrap core CSS -->
    <link href="<?= base_url('cssdepan')?>/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="<?= base_url('cssdepan')?>/offcanvas.css" rel="stylesheet">

    <script src="<?= base_url('cssdepan')?>/assets/js/vendor/jquery-slim.min.js"></script>
    <script src="<?= base_url('cssdepan')?>/assets/js/vendor/popper.min.js"></script>
    <script src="<?= base_url('cssdepan')?>/dist/js/bootstrap.min.js"></script>
    <script src="<?= base_url('cssdepan')?>/assets/js/vendor/holder.min.js"></script>
    <script src="<?= base_url('cssdepan')?>/offcanvas.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  </head>

  <body class="bg-light">

    <nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">
      <a class="navbar-brand" href="<?= base_url()?>">Sekehe Koi Bali</a>
      <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
         <!-- <li class="nav-item active">
            <a class="nav-link" href="#">Setting</a>
          </li> -->
          <li class="nav-item active">
            <a class="nav-link" href="<?= base_url().'auth/logout'?>">Logout </a>
          </li>
        </ul>
        
      </div>
    </nav>
    <main role="main" class="container">
        <?=  $contents; ?>
    </main>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
    <script type="text/javascript">
      function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
    </script>

  </body>
</html>
