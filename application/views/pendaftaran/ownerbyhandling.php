 <div class="row">
        <div class="col-md-3">
        	<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">About Me</h3>
              <div class="pull-right">
                <button class="btn btn-xs btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-edit"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            		<?= $this->session->flashdata('msg')?>
              <strong><i class="fa fa-user margin-r-5"></i> Handling</strong>

              <p class="text-muted">
                <?= $hand->nama ?>
              </p>
              <strong><i class="fa fa-map-marker margin-r-5"></i> Kota</strong>
	              <p class="text-muted">
    	          	<?= $hand->kota ?>
        	  		</p>
              <strong><i class="fa fa-map-marker margin-r-5"></i> Telepon</strong>

              <p class="text-muted"><?= $hand->telp ?></p>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        
        	 <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#activity" data-toggle="tab">Owner</a></li>
              <li><a href="#timeline" data-toggle="tab">Tambah Owner</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="activity">
                <!-- data -->
                	 <table class="table table-bordered table-hover">
	                    <thead>
	                        <tr>
	                            <th width="5%">No</th>
	                            <th>Nama</th>
	                            <th>Kota</th>
	                            <th width="30%">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        <?php $nn=1; foreach($owner as $ow){?>
	                        <tr>
	                            <td align="center"><?php echo $nn?></td>
	                            <td><?php echo  anchor('pendaftaran/daftarfromkontes/'.$ow->id_inc, $ow->nama); ?></td>
	                            <td><?php echo $ow->kota?></td>
	                            <td align="center"><?php echo anchor('pendaftaran/daftarfromkontes/'.$ow->id_inc,'<i class="fa fa-plus"></i> Ikan','class="btn btn-xs btn-success"'); ?></td>

	                        </tr>
	                        <?php $nn++; }?>
	                    </tbody>
	                </table>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="timeline">
                	<!-- form -->
                	 <form class="form-horizontal" method="post" action="<?php echo base_url().'pendaftaran/prosesaddowner'?>"> 
                    <div class="form-group ">
                        <input type="hidden" name="ms_handling_id" value="<?php echo $hand->id_inc?>">
                        <label class="col-md-3 control-label">Owner <span>*</span></label>
                        <div class="col-md-9">
                            <input type="text" name="nama" id="nama" class="form-control" required placeholder="nama owner">
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-md-3 control-label">Kota <span>*</span></label>
                        <div class="col-md-9">
                            <input type="text" name="kota" id="kota" class="form-control" required placeholder="kota owner">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button class="btn btn-sm btn-info" type="submit"><i class="fa fa-save"></i> Simpan</button>
                            <button class="btn btn-sm btn-warning" type="reset"><i class="fa fa-refresh"></i> Reset</button>
                        </div>
                    </div>
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
</div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <form method="post" action="<?= base_url().'pendaftaran/updateDataHandling' ?>">
           <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="myModalLabel">Data Handling</h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label>Nama Handling <sup>*</sup></label>
                <input type="text" name="nama" class="form-control" value="<?php echo $hand->nama?>" required="">
                <input type="hidden" name="id_inc" value="<?php echo $hand->id_inc?>">
              </div>
              <div class="form-group">
                <label>Kota <sup>*</sup></label>
                <input type="text" name="kota" class="form-control" value="<?php echo $hand->kota?>" required="">
              </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
              <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
            </div>
        </form>
    </div>
  </div>
</div>
