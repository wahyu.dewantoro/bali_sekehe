<style type="text/css">
    .responsive {
        width: auto;
          /*max-width: */
          height: 200px;
        }
</style>
<div class="row">
  
    <div class="col-md-12">
            <div class="box box-success">
            <div class="box-header with-border">
                    
                    Data Tagihan
                    
                    <div class="pull-right">
                        <?= anchor('pendaftaran/ownerbyhandling','<i class="fa fa-user"></i> Tambah Owner','class="btn btn-xs btn-success"')?>
                        
                    </div>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                           <table class="table table-striped table-bordered table-hover" id="tb" >

            <thead>

                <tr>

                    <th width="5%">No</th>

                    <th>Kode</th>
                    <th>Owner</th>
                    <th>Ikan</th>
                    <th>Biaya</th>
                  
                </tr>

            </thead>

        <tbody>

            <?php

            $start = 0;

            $jum=0;
            $bb=0;

            foreach ($daftar_data as $daftar)

            {

                ?>

                <tr>

            <td align='center'><?php echo ++$start ?></td>

            <td><?= anchor(site_url('pendaftaran/daftarfromkontes/'.$daftar->kh),$daftar->kode);  ?></td>
            <td><?= $daftar->pemilik_ikan?></td>

            <td><?= $daftar->jum_ikan?> ekor</td>           
            <td align="right"><?= number_format($daftar->biaya,'0','','.');?></td>
            

            </tr>

                <?php

                $bb+=$daftar->biaya;

                $jum+=$daftar->jum_ikan;

            }

            ?>

            </tbody>

            <tfoot>

                <tr>

                    <td colspan="3">Total  </td>

                    <td colspan="1" align="center"><?php echo $jum;?></td>
                    <td align="right"><?= number_format($bb,'0','','.');?></td>

                </tr>

            </tfoot>

        </table>

                    </div>
                </div>
            </div>
    </div>
    
</div>

 
<!-- <div class="modal fade" id="ajax" role="basic" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-body">

                <span><i class="fa fa-refresh"></i> &nbsp;&nbsp;Loading... </span>

            </div>

        </div>

    </div>

</div>
  -->