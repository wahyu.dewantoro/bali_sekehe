<div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        Sistem
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Impersonate</span>
                    </li>
                </ul>
          
            </div>
<div class="portlet box green">
<div class="portlet-title">
    <div class="caption">
        <i class="fa fa-list"></i>Data</div>
</div>
<div class="portlet-body">
                                
    <table class="table table-striped table-bordered table-hover" id="tb" >
        <thead>
            <tr>
                <th width="5%">No</th>
                <th> Nama</th>
                <th>Username</th>
                <th>Role</th>
                <th>Kontes</th>
                <th>Aksi</th>
            </tr>
        </thead>
	    <tbody>
            <?php
            $start = 0;
            foreach ($rk as $rk)
            {
                ?>
                <tr>
        		    <td align='center'><?php echo ++$start ?></td>
                     <td><?php echo $rk->nama ?></td>
                     <td><?php echo $rk->username ?></td>
                     <td><?php echo $rk->nama_role ?></td>
                     <td><?php echo $rk->nama_kontes ?></td>
                     <td><?php echo anchor('auth/impersonate/'.$rk->id_inc,'<i class="fa fa-sign-in"></i> Login as','class="btn btn-warning btn-xs"');  ?></td>
    	        </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

