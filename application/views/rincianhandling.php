<?php foreach( $handling as $handling){  ?>
<b>Handling</b> : <?= $handling->nama_handling ?><br>
<?php $rincian=$this->db->query("SELECT no_ikan,ukuran,nm_ikan variety,getownerkota(ms_peserta_id) nama_owner, gethandlinkota(ms_handling_id) nama_handling,
            CASE WHEN ms_juara_id=1 THEN f.alias 
        WHEN ms_juara_id=2 THEN  f.alias 
        WHEN ms_juara_id=3 THEN  f.alias 
        WHEN ms_juara_id=10 THEN f.alias
        WHEN ms_juara_id=11 THEN 
            CONCAT(nama_champion(ukuran),' ',e.alias)
        ELSE  CONCAT( f.alias,' ',e.alias) END juara
                                FROM tb_juara_kontes a
                                JOIN tb_ikan b ON a.tb_ikan_id=b.id_inc
                                JOIN tb_peserta c ON c.id_inc=b.tb_peserta_id
                                JOIN ms_handling d ON d.id_inc=ms_handling_id
                                JOIN ms_kategoriikan e ON e.id_inc=b.ms_kat_id
                                JOIN ms_juara f ON f.id_inc=a.ms_juara_id
                                 WHERE  ms_juara_id NOT IN (4,5) and gethandlinkota(ms_handling_id)='".$handling->nama_handling."'
                                ORDER BY  ms_kat_id asc, f.sort ASC,b.ukuran asc")->result(); ?>
		<table border="1" cellpadding="3" width="100%" cellspacing="0" style="font-size: 12px;border:1px ; border-style: groove;">
			<thead>
				<tr>
					<th>No Ikan</th>
					<th>Ukuran</th>
					<th>Variety</th>
					<th>Owner</th>
					<th>Handling</th>
					<th>Juara</th>	
				</tr>
			</thead>
			<tbody>
				<?php foreach($rincian as $rincian){?>
					<tr>
						<td align="center"><?= $rincian->no_ikan ?></td>
						<td  align="center"><?= $rincian->ukuran ?> cm</td>
						<td><?= $rincian->variety ?></td>
						<td><?= $rincian->nama_owner ?></td>
						<td><?= $rincian->nama_handling ?></td>
						<td align="center"><?= $rincian->juara?></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
			
<pagebreak>
<?php } ?>
