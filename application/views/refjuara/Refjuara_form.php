        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    Master
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Juara</span>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span><?php echo $button ?></span>
                                </li>
                            </ul>
                        </div>
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-green-haze">
                                    <i class="fa fa-file-text font-green-haze"></i>
                                    <span class="caption-subject bold"> <?php echo $button ?></span>
                                </div>
                            </div>
                            <div class="portlet-body form">
        <form action="<?php echo $action; ?>" method="post" class="form-horizontal">
	    <div class="form-group form-md-line-input  has-success">
            <label class="col-md-2 control-label" for="varchar">Kode   <span class="required"> * </span></label>
            <div class="col-md-10">
                <input type="text" class="form-control" name="kode_juara" id="kode_juara"  value="<?php echo $kode_juara; ?>" />
                <?php echo form_error('kode_juara') ?>
            </div>
        </div>
	    <div class="form-group form-md-line-input  has-success">
            <label class="col-md-2 control-label" for="varchar">  Juara <span class="required"> * </span></label>
            <div class="col-md-10">
                <input type="text" class="form-control" name="nama_juara" id="nama_juara"  value="<?php echo $nama_juara; ?>" />
                <?php echo form_error('nama_juara') ?>
            </div>
        </div>
	    <div class="form-group form-md-line-input  has-success">
            <label class="col-md-2 control-label" for="varchar">Alias <span class="required"> * </span></label>
            <div class="col-md-10">
                <input type="text" class="form-control" name="alias" id="alias"  value="<?php echo $alias; ?>" />
                <?php echo form_error('alias') ?>
            </div>
        </div>
	    
 
	   <div class="form-group"><div class="col-md-10 col-md-offset-2"> <input type="hidden" name="id_inc" value="<?php echo $id_inc; ?>" /> 
	    <button type="submit" class="btn btn-sm blue"><i class="fa fa-save"></i> Simpan</button> 
	    <a href="<?php echo site_url('refjuara') ?>" class="btn btn-sm red"><i class="fa fa-close"></i> Batal</a> </div></div>
	</form>
        </div>
</div>