        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    Participants
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Owner</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                
                            </div>
                        </div>
        <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-list"></i>Data</div>
                                    </div>
                                    <div class="portlet-body">
                                        
                                    <table class="table table-striped table-bordered table-hover" id="tb" >
                                        <thead>
                                            <tr>
                                                <th width="5%">No</th>
                            		    <th>Owner</th>
                            		    <th>Kota</th>
                                        <th>Handling</th>
                            		    <th>Aksi</th>
                                            </tr>
                                        </thead>
                            	    <tbody>
                                        <?php
                                        $start = 0;
                                        foreach ($refpeserta_data as $refpeserta)
                                        {
                                            ?>
                                            <tr>
                            		    <td align='center'><?php echo ++$start ?></td>
                            		    <td><?php echo $refpeserta->nama ?></td>
                            		    <td><?php echo $refpeserta->kota ?></td>
                                        <td>
                                            <?php 
                                                $car='';
                                                $exp=explode('#',$refpeserta->handling);
                                                foreach($exp as $exp){
                                                    $exb=explode('|',$exp);
                                                     $car.=anchor(site_url('refhandling/detail/'.$exb[0]),$exb[1]).', ';        
                                                }
                                                echo substr($car,0,-2);
                                             ?>
                                        </td>
                            		    <td style="text-align:center" width="10%"><?php 
                            			echo anchor(site_url('refpeserta/update/'.$refpeserta->id_inc),'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"'); 
                            			echo anchor(site_url('refpeserta/delete/'.$refpeserta->id_inc),'<i class="fa fa-trash"></i>','onclick="javasciprt: return confirm(\'apakah anda yakin hapus ?\')" class="btn btn-xs btn-danger"'); 
                            			?></td>
                            	        </tr>
                                            <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
       
    </div>
</div>
       