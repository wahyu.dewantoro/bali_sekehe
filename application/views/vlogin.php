<div class="row  justify-content-md-center">
  <div class="col-md-6 col-md-offset-6">  
    <div class="my-3 p-3 bg-white rounded box-shadow">
    <h6 class="border-bottom border-gray pb-2 mb-0">Login</h6>
    <?= $this->session->flashdata('msg') ?>
    <form action="<?= $action ?>" method="post">

      <div class="form-group">
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1"><span class="fa fa-user form-control-feedback"></span></span>
            </div>
            <input type="text" autofocus="true" type="text" autocomplete="off" placeholder="Username" name="username" class="form-control" placeholder="Username">
          </div>
      </div>
      <div class="form-group">
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1"><span class="fa fa-lock form-control-feedback"></span></span>
            </div>
            <input type="password" autocomplete="off" placeholder="Password" name="password" class="form-control" placeholder="Password ..">
          </div>
      </div>
 

      <div class="row">
        <!-- /.col -->
        <div class="col-xs-6">
          <a href="<?= base_url().'home/signup'?>" class="btn btn-info btn-block btn-flat"><i class="fa fa-user-plus"></i> Registrasi</a>
        </div>
        <div class="col-xs-6"  >
          <p style="text-align: right"><button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-sign-in"></i> Login</button></p>
        </div>
        
        <!-- /.col -->
      </div>
    </form>
 </div>
 
    </div>
</div>


