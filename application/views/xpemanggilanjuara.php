<style type="text/css">
	.tengah{
		text-align:center;
	}

</style>
<h2 class="tengah">LAPORAN JUARA KONTES</h3><br>
<h3 class="tengah">Sekehe Koi Bali 2019</h3><br>

<strong class="tengah">STAGE 1</strong>
<table width="100%" border="1" cellpadding="3" cellspacing="0">
	<tr>
		<th colspan="6">Juara 1 A</th>
	</tr>
	<tr>
		<th>#</th>
		<th>Size</th>
		<th>ID Ikan</th>
		<th>Variety</th>
		<th>Owner</th>
		<th>Handling</th>
	</tr>

	<?php $j1=$this->db->query("SELECT no_ikan,b.ukuran,getvariety(ms_kat_id) variety,getownerkota(ms_peserta_id) OWNER,gethandlinkota(ms_handling_id) handling,nama_juara
											FROM tb_juara_kontes a
											JOIN tb_ikan b ON a.tb_ikan_id =b.id_inc
											JOIN tb_peserta c ON c.id_inc=b.tb_peserta_id
											JOIN ms_juara d ON d.id_inc =a.ms_juara_id
											WHERE ms_juara_id IN (1)  AND ms_kat_id IN (SELECT id_inc FROM ms_kategoriikan WHERE kat_ikan='a')
											ORDER BY ukuran ASC,ms_kat_id ASC")->result(); 
		$b1=1; foreach($j1 as $j1){ ?>
			<tr>
				<td align="center"><?= $b1 ?></td>
				<td align="center"><?= $j1->ukuran.' CM'?></td>
				<td align="center"><?= $j1->no_ikan ?></td>
				<td><?= $j1->variety ?></td>
				<td><?= $j1->OWNER ?></td>
				<td><?= $j1->handling ?></td>
			</tr>
	<?php $b1++; } ?>

</table><br>
<table width="100%" border="1" cellpadding="3" cellspacing="0">
	<tr>
		<th colspan="6">Juara 1 B</th>
	</tr>
	<tr>
		<th>#</th>
		<th>Size</th>
		<th>ID Ikan</th>
		<th>Variety</th>
		<th>Owner</th>
		<th>Handling</th>
	</tr>

	<?php $j2=$this->db->query("SELECT no_ikan,b.ukuran,getvariety(ms_kat_id) variety,getownerkota(ms_peserta_id) OWNER,gethandlinkota(ms_handling_id) handling,nama_juara
											FROM tb_juara_kontes a
											JOIN tb_ikan b ON a.tb_ikan_id =b.id_inc
											JOIN tb_peserta c ON c.id_inc=b.tb_peserta_id
											JOIN ms_juara d ON d.id_inc =a.ms_juara_id
											WHERE ms_juara_id IN (1)  AND ms_kat_id IN (SELECT id_inc FROM ms_kategoriikan WHERE kat_ikan='b')
											ORDER BY ukuran ASC,ms_kat_id ASC")->result(); 
		$b2=1; foreach($j2 as $j2){ ?>
			<tr>
				<td align="center"><?= $b2 ?></td>
				<td align="center"><?= $j2->ukuran.' CM'?></td>
				<td align="center"><?= $j2->no_ikan ?></td>
				<td><?= $j2->variety ?></td>
				<td><?= $j2->OWNER ?></td>
				<td><?= $j2->handling ?></td>
			</tr>
	<?php $b2++; } ?>

</table>
<br>
<strong class="tengah">STAGE 2</strong>
<table width="100%" border="1" cellpadding="3" cellspacing="0">
	<tr>
		<th colspan="6">BIS A</th>
	</tr>
	<tr>
		<th>#</th>
		<th>Size</th>
		<th>ID Ikan</th>
		<th>Variety</th>
		<th>Owner</th>
		<th>Handling</th>
	</tr>

	<?php $bisa=$this->db->query("SELECT no_ikan,b.ukuran,getvariety(ms_kat_id) variety,getownerkota(ms_peserta_id) OWNER,gethandlinkota(ms_handling_id) handling,nama_juara
											FROM tb_juara_kontes a
											JOIN tb_ikan b ON a.tb_ikan_id =b.id_inc
											JOIN tb_peserta c ON c.id_inc=b.tb_peserta_id
											JOIN ms_juara d ON d.id_inc =a.ms_juara_id
											WHERE ms_juara_id IN (6)  AND ms_kat_id IN (SELECT id_inc FROM ms_kategoriikan WHERE kat_ikan='a')
											ORDER BY ukuran ASC,ms_kat_id ASC")->result(); 
		$ba=1; foreach($bisa as $bisa){ ?>
			<tr>
				<td align="center"><?= $ba ?></td>
				<td align="center"><?= $bisa->ukuran.' CM'?></td>
				<td align="center"><?= $bisa->no_ikan ?></td>
				<td><?= $bisa->variety ?></td>
				<td><?= $bisa->OWNER ?></td>
				<td><?= $bisa->handling ?></td>
			</tr>
	<?php $ba++; } ?>

</table><br>
<table width="100%" border="1" cellpadding="3" cellspacing="0">
	<tr>
		<th colspan="6">BIS B</th>
	</tr>
	<tr>
		<th>#</th>
		<th>Size</th>
		<th>ID Ikan</th>
		<th>Variety</th>
		<th>Owner</th>
		<th>Handling</th>
	</tr>

	<?php $bisb=$this->db->query("SELECT no_ikan,b.ukuran,getvariety(ms_kat_id) variety,getownerkota(ms_peserta_id) OWNER,gethandlinkota(ms_handling_id) handling,nama_juara
											FROM tb_juara_kontes a
											JOIN tb_ikan b ON a.tb_ikan_id =b.id_inc
											JOIN tb_peserta c ON c.id_inc=b.tb_peserta_id
											JOIN ms_juara d ON d.id_inc =a.ms_juara_id
											WHERE ms_juara_id IN (6)  AND ms_kat_id IN (SELECT id_inc FROM ms_kategoriikan WHERE kat_ikan='b')
											ORDER BY ukuran ASC,ms_kat_id ASC")->result(); 
		$bb=1; foreach($bisb as $bisb){ ?>
			<tr>
				<td align="center"><?= $bb ?></td>
				<td align="center"><?= $bisb->ukuran.' CM'?></td>
				<td align="center"><?= $bisb->no_ikan ?></td>
				<td><?= $bisb->variety ?></td>
				<td><?= $bisb->OWNER ?></td>
				<td><?= $bisb->handling ?></td>
			</tr>
	<?php $bb++; } ?>
</table>

<strong class="tengah">STAGE 3</strong>
<table cellpadding="3" cellspacing="0" border="1">
	<tr valign="top">
			<td>Most Poin</td>
			<td><?php echo  $mostpoin->nama_owner.' / '.$mostpoin->kota.' - '.$mostpoin->poin.' poin' ?></td>
		</tr>
		<tr valign="top">
			<td>Most Entry</td>
			<td><?php echo  $mostentry->nama_owner.' / '.$mostentry->kota.' - '.$mostentry->ikan.' fish' ?></td>
		</tr>
		<tr valign="top">
			<td>Most Handling</td>
			<td><?php echo $mosthandling->nama_handling.' / '.$mosthandling->kota.' - '.$mosthandling->ikan.' fish' ?></td>

		</tr>
</table>

<strong class="tengah">STAGE 4</strong>
<table width="100%" border="1" cellpadding="3" cellspacing="0">
	<tr>
		<th colspan="6">BIS B</th>
	</tr>
	<tr>
		<th>#</th>
		<th>Champion</th>
		<th>Size</th>
		<th>ID Ikan</th>
		<th>Variety</th>
		<th>Owner</th>
		<th>Handling</th>
	</tr>

	<?php $champ=$this->db->query("SELECT no_ikan,ukuran,nm_ikan variety,getownerkota(ms_peserta_id) OWNER, gethandlinkota(ms_handling_id) handling,
            CASE WHEN ms_juara_id=1 THEN f.alias 
        WHEN ms_juara_id=2 THEN  f.alias 
        WHEN ms_juara_id=3 THEN  f.alias 
        WHEN ms_juara_id=10 THEN f.alias
        WHEN ms_juara_id=11 THEN 
            CONCAT(nama_champion(ukuran),' ',e.alias)
        ELSE  CONCAT( f.alias,' ',e.alias) END juara
                                FROM tb_juara_kontes a
                                JOIN tb_ikan b ON a.tb_ikan_id=b.id_inc
                                JOIN tb_peserta c ON c.id_inc=b.tb_peserta_id
                                JOIN ms_handling d ON d.id_inc=ms_handling_id
                                JOIN ms_kategoriikan e ON e.id_inc=b.ms_kat_id
                                JOIN ms_juara f ON f.id_inc=a.ms_juara_id
                                 WHERE  ms_juara_id  IN (11)
                                ORDER BY  b.ukuran ASC,ms_kat_id ASC, f.sort ASC")->result(); 
		$bcb=1; foreach($champ as $champ){ ?>
			<tr>
				<td align="center"><?= $bcb ?></td>
				<td><?= $champ->juara ?></td>
				<td align="center"><?= $champ->ukuran.' CM'?></td>
				<td align="center"><?= $champ->no_ikan ?></td>
				<td><?= $champ->variety ?></td>
				<td><?= $champ->OWNER ?></td>
				<td><?= $champ->handling ?></td>
			</tr>
	<?php $bcb++; } ?>
</table>