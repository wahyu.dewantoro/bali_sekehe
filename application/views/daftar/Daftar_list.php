<div class="page-bar">
    <ul class="page-breadcrumb">
        <li> <span>Cart Pendaftaran</span> </li>
        <?php if($st==0){?>
            <li> <span> <span class="alert alert-danger"> [ Pendaftaran telah ditutup ]</span></span> </li>
        <?php } ?>
    </ul>

</div>
<div class="portlet box green">
    <div class="portlet-title"> 
        <div class="caption"> <i class="fa fa-list"></i>Data</div> 
    </div>
    <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" id="tb" >
            <thead> 
                <tr>
                     <th width="5%">No</th> 
                     <th>kode</th> 
                     <th>Handling</th> 
                     <th>Owner</th> 
                     <th>Ikan</th> 
                     <th>Aksi</th> 
                 </tr>
            </thead>
	       <tbody>
            <?php $start = 0; $jum=0; foreach ($daftar_data as $daftar) {?> <tr>
                    <td align='center'><?php echo ++$start ?></td> 
                    <td><?= anchor(site_url('daftar/listcartbydaftar/'.$daftar->id_inc),$daftar->kode);  ?>
                        <span class="badge <?php if($daftar->st_entri=='online'){ echo " badge-success";}else{echo " badge-primary";}?>"><?= $daftar->st_entri ?></span>
                    </td> 
                    <td><?= $daftar->handling?></td> 
                    <td><?= $daftar->pemilik_ikan?></td> 
                    <td><?= $daftar->jum_ikan?> ekor</td>
            		<td style="text-align:center" width="15%">
                        <?php
                                echo anchor('daftar/chceckout/'.$daftar->id_inc,'<i class="fa fa-sign-in"></i>','data-toggle="tooltip" title="Checkout" class="btn purple btn-xs btn-outline" onclick="return confirm(\'Apakah anda yakin?\')"'); 
                                echo anchor(site_url('daftar/listcartbydaftar/'.$daftar->id_inc),'<i class="fa fa-search"></i>','class="btn btn-xs btn-success"'); 
                                echo anchor(site_url('daftar/delete/'.$daftar->id_inc),'<i class="fa fa-trash"></i>','onclick="javasciprt: return confirm(\'apakah anda yakin hapus ?\')" class="btn btn-xs btn-danger"');
            			?>
                    </td>
	           </tr>
            <?php $jum+=$daftar->jum_ikan; } ?>
            </tbody>
            <tfoot>
                <tr> 
                    <td colspan="4">Total Ikan</td> 
                    <td colspan="2"><?php echo $jum;?></td> 
                </tr>
            </tfoot>
        </table>
    </div>
</div>

       