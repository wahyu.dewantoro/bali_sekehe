<form method="post" action="<?php echo base_url().'daftar/prosessimpanikan'?>" class="form-horizontal" enctype="multipart/form-data">
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                Form Pendaftaran
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Handling : <?php echo $rk->handling?></span>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Owner : <?php echo $rk->peserta ?></span>
            </li>
        </ul>
    </div>
    <div class="row">
        <input type="hidden" name="ms_peserta_id" value="<?php echo $rk->ms_peserta_id?>">  
        <input type="hidden" name="ms_handling_id" value="<?php echo $rk->ms_handling_id?>">  
        <div class="col-sm-6 col-sm-6">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        Form Ikan
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <div class="col-sm-12 text-center">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 120px; height: 160px;"> </div>
                                         <small> <span   class="fileinput-filename"></span></small>
                                        <div>
                                            <span class="btn-file"> <input type="file" required accept="image/*" name="gambar_ikan[]"> </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8" style="font-size:12px">
                            <div class="form-group">
                                <label class="col-sm-4 ">Jenis</label>
                                <div class="col-sm-8">
                                    <select name="ms_kat_id[]" class="form-control" required>
                                      <option value="">pilih</option>
                                      <?php foreach($ikan as $rr){?>
                                      <option value="<?php echo $rr->id_inc?>"><?php echo $rr->nm_ikan?></option>
                                      <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 ">Ukuran</label>
                                <div class="col-sm-8">
                                    <input type="number" name="ukuran[]" class="form-control" required>
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-sm-4 ">Gender</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="gender[]" required>
                                        <option value="">Pilih</option>
                                        <option value="Female">Female</option>
                                        <option value="Male">Male</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 ">Breeder</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="asal[]" required>
                                        <option value="">Pilih</option>
                                        <option value="Lokal">Lokal</option>
                                        <option value="Import">Import</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2 text-right">
                                    <button  type="button"  class="add_project_file btn purple btn-sm "><i class="fa fa-plus"></i> Tambah Form</button>
                                    <span id="count"><button type="submit"  class="btn btn-sm green "><i class="fa fa-save"></i> Submit (1)</button></span> 
                                    <input type="hidden" id="vc" value="1">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       <div class="project_images"></div>
    </div>
</form>



<script type="text/javascript">

    var rowNum = 0;

    ccc=1;

    $('.add_project_file').click(function(e) {

    e.preventDefault();

     rowNum++;











     // Form Ikan <span id='ke'>1</span>

     if(ccc<20){

             ccc+=1;

             $('#vc').val(ccc);

             $('#count').html('<button type="submit"  class="btn btn-sm green "><i class="fa fa-save"></i> Submit ('+ccc+')</button>');
     $(".project_images").append('<div class="abc"> <div class="col-sm-6 col-sm-6">'
            +'<div class="portlet box green">'
                +'<div class="portlet-title">'
                    +'<div class="caption">'
                        +'Form Ikan'
                    +'</div>'
                +'</div>'
                +'<div class="portlet-body">'
                    +'<div class="row">'
                        +'<div class="col-sm-4">'
                            +'<div class="form-group">'
                                +'<div class="col-sm-12 text-center">'
                                    +'<div class="fileinput fileinput-new" data-provides="fileinput">'
                                        +'<div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 100px; height: 150px;"> </div>'
                                         +'<small> <span   class="fileinput-filename"></span></small>'
                                        +'<div>'
                                            +'<span class="btn-file">'
                                                +'<input type="file" accept="image/*" required name="gambar_ikan[]"> </span>'
                                        +'</div>'
                                    +'</div>'
                                +'</div>'
                            +'</div>'
                        +'</div>'
                        +'<div class="col-sm-8" style="font-size:12px">'
                            +'<div class="form-group">'
                                +'<label class="col-sm-4 ">Jenis</label>'
                                +'<div class="col-sm-8">'
                                    +'<select name="ms_kat_id[]" class="form-control" required>'
                                      +'<option value="">pilih</option>'
                                      +'<?php foreach($ikan as $rr){?>'
                                      +'<option value="<?php echo $rr->id_inc?>"><?php echo $rr->nm_ikan?></option>'
                                      +'<?php } ?>'
                                    +'</select>'
                                +'</div>'
                            +'</div>'
                            +'<div class="form-group">'
                                +'<label class="col-sm-4 ">Ukuran</label>'
                                +'<div class="col-sm-8">'
                                    +'<input type="number" name="ukuran[]" required class="form-control">'
                                +'</div>'
                            +'</div>'
                             +'<div class="form-group">'
                                +'<label class="col-sm-4 ">Gender</label>'
                                +'<div class="col-sm-8">'
                                    +'<select class="form-control"  required name="gender[]">'
                                        +'<option value="">Pilih</option>'
                                        +'<option value="Female">Female</option>'
                                        +'<option value="Male">Male</option>'
                                    +'</select>'
                                +'</div>'
                            +'</div>'
                            +'<div class="form-group">'
                                +'<label class="col-sm-4 ">Breeder</label>'
                                +'<div class="col-sm-8">'
                                    +'<select class="form-control" required name="asal[]">'
                                        +'<option value="">Pilih</option>'
                                        +'<option value="Lokal">Lokal</option>'
                                        +'<option value="Import">Import</option>'
                                    +'</select>'
                                +'</div>'
                            +'</div>'
                            +'<div class="form-group">'
                                +'<div class="col-sm-6 col-sm-offset-6">'
                                      +'<a href="#" class="remove_project_file btn btn-sm red "><i class="fa fa-trash"></i></a>'
                                +'</div>'
                            +'</div>'
                        +'</div>'
                    +'</div>'
                +'</div>'
            +'</div>'
            +' </div></div>');



  

    }

  

});



// Remove parent of 'remove' link when link is clicked.

 $('.project_images').on('click', '.remove_project_file', function(e) {

            e.preventDefault();

            $(this).parents(".abc").remove();

            ccc-=1;

            $('#vc').val(ccc);

            $('#count').html('<button type="submit" class="btn btn-sm green "><i class="fa fa-save"></i> Submit ('+ccc+')</button>');

        });

   

</script>



