<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <span>Cart Pendaftaran</span>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Detail</span>
        </li>
    </ul>
    <div class="page-toolbar">
        <div class="btn-group pull-right">
            <?php 
        // if($this->session->userdata('wob_role')==2 || $this->session->userdata('wob_role')==5){
            echo anchor('daftar/chceckout/'.$rp->id_inc,'<i class="fa fa-sign-in"></i> checkout','class="btn green btn-sm btn-outline" onclick="return confirm(\'Apakah anda yakin?\')"');
        // }
        
        if($this->session->userdata('wob_role')!=4){
                echo anchor(site_url('refhandling/detail/'.$rp->id_handling), '<i class="fa fa-list"></i> kembali','class="btn purple btn-sm btn-outline"');
        }else{
                echo anchor(site_url('refhandling/detail/'.$rp->id_handling), '<i class="fa fa-list"></i> kembali','class="btn purple btn-sm btn-outline"');
        }
        ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Data Peserta</div>
            </div>
            <div class="portlet-body">
                <table class="table">
                    <tbody>
                        <tr>
                            <td width="30%">Kode Pendaftaran</td>
                            <td width="1%">:</td>
                            <td><?= $rp->kode?></td>
                        </tr>
                        <tr>
                            <td>Handling</td>
                            <td>:</td>
                            <td><?= $rp->handling?></td>
                        </tr>
                        <tr>
                            <td>Kota</td>
                            <td>:</td>
                            <td><?= $rp->kota_handling?></td>
                        </tr>
                        <tr>
                            <td>Owner</td>
                            <td>:</td>
                            <td><?= $rp->pemilik_ikan?></td>
                        </tr>
                        <tr>
                            <td>Kota</td>
                            <td>:</td>
                            <td><?= $rp->kota_pemilik?></td>
                        </tr>
                        <tr>
                            <td>Entry by</td>
                            <td>:</td>
                            <td><?= $rp->tanggal_daftar?> oleh <?= $rp->nama_daftar?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    Form Ikan 
                </div>
            </div>
            <div class="portlet-body">
                <form action="<?php echo base_url().'daftar/prosestambahikan'?>" method="post" class="form-horizontal" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <div class="col-sm-12 text-center">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 120px; height: 160px;"> </div>
                                         <small> <span   class="fileinput-filename"></span></small>
                                        <div>
                                            <span class="btn-file">
                                                <input type="file" accept="image/*" name="gambar_ikan"> </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <input type="hidden" name="tb_peserta_id" value="<?php echo $rp->id_inc?>">  
                            <input type="hidden" name="ms_peserta_id" value="<?php echo $rp->id_peserta?>">  
                            <input type="hidden" name="ms_handling_id" value="<?php echo $rp->id_handling?>">  
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Jenis</label>
                                <div class="col-sm-8">
                                    <select name="ms_kat_id" class="form-control" reuired>
                                      <option value="">pilih</option>
                                      <?php foreach($ikan as $rr){?>
                                      <option value="<?php echo $rr->id_inc?>"><?php echo $rr->nm_ikan?></option>
                                      <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Ukuran</label>
                                <div class="col-sm-8">
                                    <input type="number" name="ukuran" class="form-control">
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-sm-4 control-label">Gender</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="gender">
                                        <option value="">Pilih</option>
                                        <option value="Female">Female</option>
                                        <option value="Male">Male</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Breeder</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="asal">
                                        <option value="">Pilih</option>
                                        <option value="Lokal">Lokal</option>
                                        <option value="Import">Import</option>
                                    </select>
                                </div>
                            </div>                                                
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-4">
                                    <button type="submit" class="btn purple btn-sm"><i class="fa fa-save"> </i> Simpan </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </from>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> Data Ikan</div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <td width="5%">No</td>
                                        <td>Variety</td>
                                        <td>Ukuran</td>
                                        <td>Asal</td>
                                        <td>Gender</td>
                                        <td>Biaya</td>
                                        <td width="15%">Aksi</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(count($listikan)>0){
                                    $total=0;
                                     $no=1; foreach($listikan as $li){?>
                                    <tr>
                                        <td align="center"><?= $no; ?></td>
                                        <td><?= $li->nm_jenis; ?></td>
                                        <td><?= $li->ukuran; ?></td>
                                        <td><?= $li->asal; ?></td>                                                            
                                        <td><?= $li->gender; ?></td>
                                        <td align="right"><?php echo number_format($li->biaya,'0','','.')?></td>
                                        <td>
                                            <a class="btn btn-xs btn-warning" href="<?php echo base_url().'daftar/viewikan/'.$li->id_inc?>" data-target="#ajax" data-toggle="modal"><i class="fa fa-binoculars"></i> </a>
                                             <a class="btn btn-xs btn-primary" href="<?php echo base_url().'daftar/formeditikan/'.$li->id_inc?>" data-target="#ajax" data-toggle="modal"><i class="fa fa-edit"></i> </a>
                                            <?php  echo anchor('daftar/hapusikan/'.$li->id_inc,'<i class="fa fa-close"></i>','class="btn btn-xs btn-danger" onclick="return confirm(\'Apakah anda yakin?\')"');
                                            ?>
                                        </td>
                                    </tr>
                                    <?php $total+=$li->biaya;  $no++; } } else{ $total=0;?>
                                    <tr>
                                       <td align="center" colspan="6">Tidak ada data !</td> 
                                    </tr>
                                    <?php } ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="5">Total</th>
                                        <td align="right"><?php echo number_format($total,'0','','.')?></td>
                                        <td>
                                             <?php 
                                                    /*if($this->session->userdata('wob_role')!=4){
                                                        echo anchor(site_url('refhandling'), '<i class="fa fa-check"></i> Selesai','class="btn purple btn-sm btn-outline"'); 
                                                    }else{*/
                                                            /*echo anchor(site_url('daftar'), '<i class="fa fa-check"></i> Selesai','class="btn purple btn-sm btn-outline"');*/
                                                    //} ?>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ajax" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <span><i class="fa fa-refresh"></i> &nbsp;&nbsp;Loading... </span>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    document.getElementById("gambar_ikan").onchange = function () {

    var reader = new FileReader();



    reader.onload = function (e) {

        // get loaded data and render thumbnail.

        document.getElementById("image").src = e.target.result;

    };



    // read the image file as a data URL.

    reader.readAsDataURL(this.files[0]);

};

</script>