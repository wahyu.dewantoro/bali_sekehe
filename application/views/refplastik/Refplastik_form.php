        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    Master
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Plastik</span>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span><?php echo $button ?></span>
                                </li>
                            </ul>
                        </div>
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-green-haze">
                                    <i class="fa fa-file-text font-green-haze"></i>
                                    <span class="caption-subject bold"> <?php echo $button ?></span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form action="<?php echo $action; ?>" method="post" class="form-horizontal">
                        	    <div class="form-group form-md-line-input  has-success">
                                    <label class="col-md-2 control-label" for="varchar">Ukuran Plastik <span class="required"> * </span></label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" name="jenis_plastik" id="jenis_plastik"  value="<?php echo $jenis_plastik; ?>" />
                                        <?php echo form_error('jenis_plastik') ?>
                                    </div>
                                </div>
                        	    <div class="form-group form-md-line-input  has-success">
                                    <label class="col-md-2 control-label" for="int">  Min <span class="required"> * </span></label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" name="ukuran_min" id="ukuran_min"  value="<?php echo $ukuran_min; ?>" />
                                        <?php echo form_error('ukuran_min') ?>
                                    </div>
                                </div>
                        	    <div class="form-group form-md-line-input  has-success">
                                    <label class="col-md-2 control-label" for="int">  Max <span class="required"> * </span></label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" name="ukuran_max" id="ukuran_max"  value="<?php echo $ukuran_max; ?>" />
                                        <?php echo form_error('ukuran_max') ?>
                                    </div>
                                </div>
                        	   <div class="form-group"><div class="col-md-10 col-md-offset-2"> <input type="hidden" name="id_inc" value="<?php echo $id_inc; ?>" /> 
                        	    <button type="submit" class="btn btn-sm blue"><i class="fa fa-save"></i> Simpan</button> 
                        	    <a href="<?php echo site_url('refplastik') ?>" class="btn btn-sm red"><i class="fa fa-close"></i> Batal</a> </div></div>
                        	</form>
        </div>
</div>