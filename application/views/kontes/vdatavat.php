<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            Data Kontes
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Data Vat</span>
        </li>
    </ul>
    <div class="page-toolbar">
         <!-- <a class="btn btn-sm btn-outline green" href="<?php //echo base_url().'kontes/cetakdatasewavat'?>" target="_blank" ><i class="fa fa-print"></i> Cetak </a> -->
         <a class="btn btn-sm btn-outline purple" href="<?php echo base_url().'kontes/formsewavat'?>" data-target="#ajax" data-toggle="modal"><i class="fa fa-plus"></i> Tambah </a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-list"></i>Data</div>
                </div>
                <div class="portlet-body">
                    
                    <table class="table table-striped table-bordered table-hover" id="tb" >
                        <thead>
                            <tr>
                                <th width="5%">No</th>
                                <th>penyewa</th>
                                <th>jumlah</th>
                                <th>jenis</th>
                                <th>harga</th>
                                <th>total</th>
                                <th>bayar</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach($rk as $rk){?>
                            <tr>
                                <td align="center"><?php echo $no?></td>
                                <td><?= $rk->penyewa ?></td>
                                <td><?= $rk->jumlah ?></td>
                                <td><?= $rk->jenis ?></td>
                                <td><?= number_format($rk->harga ,0,'','.') ?></td>
                                <td><?= number_format($rk->total,0,'','.') ?></td>
                                <td align="center">
                                    <div id="label<?php echo $rk->id_inc?>"> 
                                    <?php if($rk->status_bayar=='1'){
                                            echo "Lunas";
                                        }else{
                                            echo "-";
                                            } ?>
                                    </div>
                                </td>
                                <td align="center">     
                                    <span id='bayar<?php echo $rk->id_inc?>'>
                                            <?php 
                                            if($rk->status_bayar=='1'){
                                            echo "<a href='#' data-id='".$rk->id_inc."' class='bayar btn btn-xs btn-danger' ><i class='fa fa-close'></i></a>";
                                        }else{
                                            echo "<a href='#' data-id='".$rk->id_inc."' class='bayar btn btn-xs btn-success' ><i class='fa fa-check'></i></a>";
                                            } 
                                            ?>
                                    </span>

                                    <?php //echo base_url().'kontes/cetakkwitansivat/'.$rk->id_inc?>
                                    <a class="btn btn-xs btn-danger yellow kwitansi_lampiran" data-id="<?= $rk->id_inc ?>"  href="" ><i class="fa fa-print"></i></a>
                                    
                                    <a class="btn btn-xs btn-danger" onclick="return confirm('Apakah anda yakin?')" href="<?php echo base_url().'kontes/hapussewavat/'.$rk->id_inc?>"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            <?php $no++; }?>
                        </tbody>
                    </table>
                </div>
            </div>
      </div>
   
</div>


<div class="modal fade" id="ajax" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                
                <span><i class="fa fa-refresh"></i> &nbsp;&nbsp;Loading... </span>
            </div>
        </div>
    </div>
</div>
 <div  class="visible-print" id="cetak"></div>

 <script type="text/javascript">
     $(function(){
            $(document).on('click','.bayar',function(e){
                e.preventDefault();
                    var id=$(this).attr('data-id');
                    $.post('<?php echo base_url()."kontes/aksibayarvat"?>',
                    {id:$(this).attr('data-id')},
                    function(html){
                        if(html=='1'){
                            $('#label'+id).html('Lunas');
                            $('#bayar'+id).html("<a href='#' data-id='"+id+"' class='bayar btn btn-xs btn-danger' ><i class='fa fa-close'></i></a>");
                        }else{
                            $('#label'+id).html('-');
                            $('#bayar'+id).html("<a href='#' data-id='"+id+"' class='bayar btn btn-xs btn-success' ><i class='fa fa-check'></i></a>");
                        }
                        
                    }   
                );
            });
        });

     $(document).on('click','.kwitansi_lampiran',function(e){
            e.preventDefault();
            $("#cetak").html('');

            $.get("<?= base_url().'kontes/cetakkwitansivat'?>",
                    {no:$(this).attr('data-id')},
                    function(resw){
                        $("#cetak").html(resw).hide();
                        printDiv('cetak');
                    }
                );
 
        });

       function printDiv(divName){
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }     
 </script>