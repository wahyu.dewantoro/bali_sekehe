<!DOCTYPE html>
<html>
<head>
	<title></title>
	 <style type="text/css">
	 	.center {
		    margin-left: auto;
		    margin-right: auto;
		}
		
		#customers {
		    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
		    border-collapse: collapse;
		    width: 100%;
		    font-size: 12px;
		}

		#customers td, #customers th {
		    border: 1px solid #ddd;
		    padding: 3px;
		}

		#customers tr:nth-child(even){background-color: #f2f2f2;}

		#customers tr:hover {background-color: #ddd;}

		#customers th {
		    padding-top: 3px;
		    padding-bottom: 3px;
		    text-align: center;
		    background-color: #ccffcc;
		    color: black;
		    border:0;
		}

		
		#customersas {
		    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
		    border-collapse: collapse;
		    width: 100%;
		    font-size: 12px;
		}

		#customersas td, #customersas th {
		    border: 1px solid #ddd;
		    padding: 3px;
		}

		#customersas tr:nth-child(even){background-color: #f2f2f2;}

		#customersas tr:hover {background-color: #ddd;}

		#customersas th {
		    padding-top: 3px;
		    padding-bottom: 3px;
		    text-align: center;
		    background-color: #99ffcc;
		    color: black;
		    border:0;
		}
	 </style>
</head>
<body>
	 <table id="customers"  >
	 	 <thead>
            <tr>
                <th width="5%">No</th>
                <th>Owner</th>
                <th>Kota</th>
                <th>ID Ikan</th>
                <th>Label Bak</th>
                <th>Jenis</th>
                <th>Biaya</th>
                <th>Status</th>
                
            </tr>
        </thead>
        <tbody>
            <?php $ja=0; $jl=0; $jbb=0; $no=1; foreach($sewa as $rk){?>
            <tr>
                <td align="center"><?php echo $no?></td>
                <td ><?php echo $rk->nama;?> </td>
                <td><?php echo $rk->kota;?> </td>
                <td><?php $this->Mkontes->getnoikan($rk->ikan) ?> </td>
                <td align="center"><?php echo $rk->label_bak;?> </td>
                <td><?php echo $rk->jenis?></td>
                <td align="right"><?php echo  number_format($rk->biaya,'0','','.');?> </td>
                <td align="center">
                    <div id="label<?php echo $rk->id_inc?>"> 
                    <?php if($rk->kd_bayar=='1'){
                            $jl+=$rk->biaya;
                            $jbb+=0;
                            echo "Lunas";
                        }else{
                        	$jl+=0;
                            $jbb+=$rk->biaya;
                            echo "-";
                            } ?>
                    </div>
                </td>
            </tr>
            <?php $no++;  $ja+=$rk->biaya; }?>
        </tbody>
        <tfoot>
        	<tr>
        		<td align=" " colspan="6">Jumlah Lunas</td>
        		<td align="right"><?php echo number_format($jl,'0','','.');?></td>
        		<td></td>
        	</tr>
        	<tr>
        		<td align=" " colspan="6">Jumlah Belum Lunas</td>
        		<td align="right"><?php echo number_format($jbb,'0','','.');?></td>
        		<td></td>
        	</tr>
        	<tr>
        		<td align=" " colspan="6"><b>Total</b></td>
        		<td align="right"><b><?php echo number_format($ja,'0','','.');?></b></td>
        		<td></td>
        	</tr>
        </tfoot>
	 </table>
</body>
</html>