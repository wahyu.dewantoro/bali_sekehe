
		
	
<form method="post" class="form-horizontal" action="<?php echo base_url().'kontes/proseseditpeserta'?>">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Edit Data Peserta</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            
				<input type="hidden" name="id_inc" value="<?= $rk->id_inc?>">
				<div class="form-group  ">
					<labe class="col-md-3 control-label">No Pendaftaran</labe>
					<div class="col-md-9">
						<input type="text" disabled  class="form-control"  value="<?= $rk->no_pendaftaran?>">
					</div>
				</div>
				<div class="form-group  ">
					<labe class="col-md-3 control-label">Status Bayar</labe>
					<div class="col-md-9">
						<input type="text" disabled class="form-control" value="<?php if($rk->kd_bayar=='1'){echo "Lunas";}else if($rk->kd_bayar=='0'){ echo "Ngutang";}else{echo "Belum bayar";} ?>">
					</div>
				</div>
				<div class="form-group  ">
					<labe class="col-md-3 control-label">Handling</labe>
					<div class="col-md-9">
						<select  name="ms_handling_id" class="form-control select2" required>
							<option value=""></option>
							<?php foreach($hand as $hd){?>
							<option <?php if($hd->id_inc==$rk->ms_handling_id){echo "selected";}?> value="<?php echo $hd->id_inc?>"><?php echo $hd->nama.' / '.$hd->kota?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="form-group  ">
					<labe class="col-md-3 control-label">Owner</labe>
					<div class="col-md-9">
						<!-- <input type="text"  value="<?= $rk->ms_peserta_id?>"> -->
						<select name="ms_peserta_id" class="form-control select2" required>
							<option value=""></option>
							<?php foreach($owner as $ow){?>
							<option <?php if($ow->id_inc==$rk->ms_peserta_id){echo "selected";} ?> value="<?php echo $ow->id_inc?>"><?php echo $ow->nama.' / '.$ow->kota?></option>
							<?php }?>
						</select>
					</div>
				</div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="submut" class="btn blue"><i class="fa fa-save"></i> Submit</button>
</div>
</form>
<script src="<?php echo base_url()?>assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>
