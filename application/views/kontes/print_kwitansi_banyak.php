<style type="text/css">
	@media print {

    .pagebreak { page-break-before: always; } /* page-break-after works, as well */
}



.center {margin-left: auto; margin-right: auto; }
	 	.responsive {width: auto; /*max-width: */ height: 150px; }
</style>

<h3 style="text-align: center;" ><b>Invoice Lomba Merah Putih 2019</b><br> <i>Sekehe Koi Bali</i></h3>
<?php $jumlah=0; foreach($res as $res){ 
	$peserta =$res['peserta'];
	$ikan    =$res['ikan'];
	?>

<?php if($peserta->no_kwitansi<>''){ echo 'No Kwitansi :'.$peserta->no_kwitansi.'<br>';}?>

<b>No Pendaftaran</b> : <?= $peserta->no_pendaftaran ?> ,<b>Handling</b> : <?= $peserta->nama_handling.' - '.$peserta->kota_handling ?> / <b>Owner</b> : <?= $peserta->nama_owner.' - '.$peserta->kota_owner ?>

<table width="100%" border="1" cellspacing="0" cellpadding="3">
	<thead>
		<tr>
			<th>#</th>
			<th>Ikan</th>
			<th>ID</th>
			<th>Keterangan</th>
			<th>Biaya</th>
		</tr>
	</thead>
	<tbody>
		<?php $no=1; $jum=0; foreach($ikan as $ri){ $jum+=$ri->nominal;?>
			<tr>
				<td align="center" width="5%"><?= $no ?></td>
				<td><?= $ri->nm_jenis?></td>
				<td align="center"><?= $ri->no_ikan ?></td>
				<td><?= $ri->ukuran.' cm , '.$ri->gender.', '.$ri->asal ?></td>
				<td style="text-align: right;" width="15%" align="right"><?= number_format($ri->nominal,0,'','.')?></td>
			</tr>
		<?php $no++;} ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="4">Total</td>
			<td  style="text-align: right;"><?= number_format($jum,0,'','.')?></td>
		</tr>
	</tfoot>
</table>
<?php $jumlah+=$jum;} ?>
<table width="50%">
		<tr>
			<td width="30%"><b>Total Bayar</b></td>
			<td>: <b>Rp <?= number_format($jumlah,0,'','.')?></b></td>
		</tr>
</table>
	<table width="100%" class="visible-print">
		<tr>
			<td width="70%">

				<table border=1 cellspacing="0" >
					<thead>
					<tr>
						<th colspan="2">Plastik</th>
					</tr>
					</thead>
					<tbody>
						<?php foreach($plastik as $plastik){?>
							<tr>
								<td  width="40%" align="center"><?= $plastik->plastik?></td>
								<td align="center"><?= $plastik->jumlah ?></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</td>
			<td align="left">
				Denpasar, <?= date_indo(date('Y-m-d')); ?><br>
				Bendahara
				<br>
				<br>
				<br>
				<br>
				Panitia
			</td>
		</tr>
	</table>
 <div class="pagebreak"> </div>
 <h3 style="text-align: center;" ><b>Kartu Lomba Merah Putih 2019</b><br> <i>Sekehe Koi Bali</i></h3>
 <?php  foreach($resa as $ress){ 
 	
	$pesertaa =$ress['peserta'];
	$gambar =$ress['gambar'];
	// $ikan    =$res['ikan'];
	?>

	 <b>No Pendaftaran</b> : <?= $pesertaa->no_pendaftaran ?> ,<b>Handling</b> : <?= $pesertaa->nama_handling.' - '.$pesertaa->kota_handling ?> / <b>Owner</b> : <?= $pesertaa->nama_owner.' - '.$pesertaa->kota_owner ?>
	 <?php 
			$kolom  = 5; 
			$gambar = array_chunk($gambar,$kolom);
		echo '<table border="0" cellpadding="10">';
		foreach ($gambar as $chunk) {
		    echo '<tr >';
		    foreach ($chunk as $ikane) {
		        echo '<td valign="top" width="150px">';?>
		        <table  style=" font-size:12px">
		        	<tr>
		        		<td valign="top"   ><img class="responsive" src="<?php echo base_url().$ikane->gambar_ikan?>"></td>
		        	</tr>
		        	<tr>
		        		<td>No Ikan : <b><?php echo  $ikane->no_ikan //sprintf("%04d",$ikane->no_ikan)?></b></td>
		        	</tr>
		        	<tr>
		        		<td  valign="top" ><?php echo $ikane->nm_ikan?> / <?php echo $ikane->ukuran?> cm </td>
		        	</tr>
		        	<tr>
		        		<td  valign="top" ><?php echo $ikane->gender?> / <?php echo $ikane->asal?> </td>
		        	</tr>
		        </table>
		        <?php echo '</td>';
		    }
		    echo '</tr>';
		}
		echo '</table>';

	 ?>
 
<?php } ?>
