<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Detail Ikan</h4>
  </div>
  <div class="modal-body">
  		<div class="row">
  			<div class="col-md-8">
  				<div class="pull-left">
  					<img style="max-width:100%"   src="<?php echo base_url().$rk->gambar_ikan?>">	
  				</div>
  			</div>
  			<div class="col-md-4">
  				<div class="pull-left">
	  				<table  style="font-size:12px">
						<tr valign="top">
							<td>ID Ikan</td>
							<td>:</td>
							<td><?= $rk->no_ikan?></td>
						</tr>
						<tr valign="top">
							<td>Owner</td>
							<td>:</td>
							<td><?= $rk->nama_peserta?> - <?= $rk->kota_peserta?></td>
						</tr>
						<tr valign="top">
							<td>Handling</td>
							<td>:</td>
							<td><?= $rk->nama_handling?> - <?= $rk->kota_handling?></td>
						</tr>
						
						<tr valign="top">
							<td>Variety</td>
							<td>:</td>
							<td><?= $rk->nm_ikan?></td>
						</tr>
						<tr valign="top">
							<td>Size</td>
							<td>:</td>
							<td><?= $rk->ukuran?> cm</td>
						</tr>
						<tr valign="top">
							<td>Breeder</td>
							<td>:</td>
							<td><?= $rk->asal?></td>
						</tr>
						<tr valign="top">
							<td>Gender</td>
							<td>:</td>
							<td><?= $rk->gender?></td>
						</tr>
						<tr valign="top">
							<td>Juara</td>
							<td>:</td>
							<td><?php echo $this->Mkontes->juarabyikan($rk->id_inc)?></td>
						</tr>
			    		
			    	</table>		
		    	</div>
  			</div>
  		</div>
    	
    	


  </div>
</div>