<html>
<head>
	<title><?php echo $rk->no_kwitansi?></title>
	 <style type="text/css">
	 	.center {
		    margin-left: auto;
		    margin-right: auto;
		}
		#customers {
		    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
		    border-collapse: collapse;
		    width: 100%;
		}

		#customers td, #customers th {
		    border: 1px solid #ddd;
		    padding: 3px;
		}

		#customers tr:nth-child(even){background-color: #f2f2f2;}

		#customers tr:hover {background-color: #ddd;}

		#customers th {
		    padding-top: 3px;
		    padding-bottom: 3px;
		    text-align: left;
		    background-color: #4CAF50;
		    color: white;
		}
	 </style>
</head>
<body>
 	
	 <?php 
	 	$kolom = 5;
	 	$gambar = array_chunk($rn,$kolom);
		echo '<table  cellpadding="10">';
		foreach ($gambar as $chunk) {
		    echo '<tr >';
		    foreach ($chunk as $ikan) {
		        echo '<td valign="top" width="100px">';?>
		        <table class="center" style="border:1px solid black; font-size:9px">
		        	<tr>
		        		<td valign="top"  colspan="3"><img width="120px" height="150px"  src="<?php echo base_url().$ikan->gambar_ikan?>"></td>
		        	</tr>
		        	<tr>
		        		<td valign="top" width="30px" >Ikan</td>
		        		<td valign="top" width="5px" >:</td>
		        		<td valign="top" width="85px"><?php echo $ikan->no_ikan?></td>
		        	</tr>
		        	<tr>
		        		<td valign="top" >Ukuran</td>
		        		<td valign="top" >:</td>
		        		<td valign="top" ><?php echo $ikan->ukuran?> cm</td>
		        	</tr>
		        	<tr valign="top">
		        		<td valign="top" >Jenis</td>
		        		<td valign="top" >:</td>
		        		<td valign="top" ><?php echo $ikan->jenis?></td>
		        	</tr>
		        	<tr>
		        		<td valign="top" >Gender</td>
		        		<td valign="top" >:</td>
		        		<td valign="top" ><?php echo $ikan->gender?></td>
		        	</tr>
		        	<tr>
		        		<td valign="top" >Asal</td>
		        		<td valign="top" >:</td>
		        		<td valign="top" ><?php echo $ikan->asal?></td>
		        	</tr>
		        </table>

		        <?php echo '</td>';
		    }
		    echo '</tr>';
		}
		echo '</table>';

	 ?>

	 <table border="0" width="100%" >
	 		<tr>
	 			<td colspan="3">
	 					 <table id="customers"  >
					       
					            <tr  >
					                <th>No</th>
					                <th>Variety</th>
					                <th>Size</th>
					                <th width="15%">L/I</th>
					                <th width="15%">Gender</th>
					                <th width="15%">Prize</th>
					                
					            </tr>
					   		 
					            <?php 
					            if(count($kwitansi)>0){
					            $total=0;
					             $no=1; foreach($kwitansi as $li){?>
					            
					            <tr >
					                <td align="centers"><?= $no; ?></td>
					                <td><?= $li->nm_jenis; ?></td>
					                <td><?= $li->ukuran; ?> cm</td>
					                <td><?= $li->asal; ?></td>                                                            
					                <td><?= $li->gender; ?></td>
					                <td align="right"><?php echo number_format($li->biaya,'0','','.')?></td>
					                
					            </tr>
					            <?php $total+=$li->biaya;  $no++; }  } else{ $total=0;?>
					            <tr>
					               <td align="center" colspan="6">Tidak ada data !</td> 
					            </tr>
					            <?php } ?>
					         
					     </table>
	 			</td>
	 		</tr>
	 		<tr>
	 			<td >Plastik</td>
	 			<td  >
	 				<?php if(count($listbak)){?>
                                                <table >
                                                    <thead>
                                                        <tr>
                                                            <th>Bak</th>
                                                            <th>Jumlah</th>
                                                            <th>Biaya</th>
                                                            <th>Total</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $bak=0; foreach($listbak as $lb){?>
                                                        <tr>
                                                            <td><?php echo $lb->nama_bak ?></td>
                                                            <td align="center"><?php echo $lb->jumlah_bak?></td>
                                                            <td align="right"><?php echo number_format($lb->biaya,'0','','.')?></td>
                                                            <td align="right"> <?php echo number_format( $lb->total,'0','','.') ?></td>
                                                        </tr>
                                                        <?php $bak+=$lb->total; }?>
                                                    </tbody>
                                                </table>
                                                <?php } else{$bak=0;}?>
	 			</td>
	 			<td  align="right">
	 				 <table border="0" cellpadding="3" cellspacing="0">
                        <tr>
                            <td>Ikan</td>
                            <td>:</td>
                            <td  ><b><?php echo number_format($total,'0','','.')?></b></td>
                        </tr>
                        <tr>
                            <td>Bak</td>
                            <td>:</td>
                            <td ><b><?php echo number_format($bak,'0','','.')?></b></td>
                        </tr>
                        <tr>
                            <td>Total</td>
                            <td>:</td>
                            <td align="right"><b><?php echo number_format($bak+$total,'0','','.')?></b></td>
                        </tr>
                    </table>
	 			</td>
	 		</tr>
	 </table>
 

     	 

</body>
</html>