<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            Data Kontes
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Data Pembayaran</span>
        </li>
    </ul>

    <div class="page-toolbar">
        <button class="btn btn-sm btn-primary" id="bayar_banyak"><i class="fa fa-money"></i> Bayar</button>
        <button class="btn btn-sm btn-success" id="cetak_banyak"><i class="fa fa-print"></i> kwitansi</button>
        <button class="btn btn-sm btn-info" id="label"><i class="fa fa-image"></i> Label</button>
        <?php echo anchor('kontes/pembayaran','<i class="fa fa-refresh"></i> Refresh','class="btn btn-sm purple btn-outline"');?>
    </div>
</div>

<!-- Nav tabs -->

<ul class="nav nav-tabs portlet box green">
  <li class="active"><a href="#tagihan" data-toggle="tab">Tagihan</a></li>
  <li ><a href="#lunas" data-toggle="tab">Lunas</a></li>
</ul>
<!-- Tab panes -->
<div class="tab-content portlet-body">

  <div class="tab-pane active" id="tagihan">
    <table class="table table-striped table-bordered table-hover tb">
        <thead>
            <tr>
                <th rowspan="2" width="5%">No</th>
                <th rowspan="2">No Pendaftaran</th>
                <th rowspan="2">No Kwitansi</th>
                <th colspan="2">Handling</th>
                <th colspan="2">Owner</th>
                <th rowspan="2">Ikan</th>
                <th rowspan="2">Total</th>
                <th rowspan="2" width="15%"></th>
            </tr>
            <tr>
                <th>Nama</th>
                <th>Kota</th>
                <th>Nama</th>
                <th>Kota</th>
            </tr>
        </thead>
        <tbody>
            <?php $no=1; foreach($tagihan as $rt){?>
                <tr  id="<?= $rt->no_pendaftaran ?>">
                    <td align="center"><input type="checkbox" id="cekbox" name="cekbox[]" value="<?= $rt->no_pendaftaran ?>"/> <?= $no ?></td>
                    <td><a href="#"> <?= $rt->no_pendaftaran ?></a><span class="badge <?php if($rt->st_entri=='online'){ echo " badge-success";}else{echo " badge-primary";}?>"><?= $rt->st_entri ?></span></td>
                    <td align="center"><?= $rt->no_kwitansi ?></td>
                    <td><?= $rt->nama_handling ?></td>
                    <td><?= $rt->kota_handling ?></td>
                    <td><?= $rt->nama_owner?></td>
                    <td><?= $rt->kota_owner?></td>
                    <td><?= $rt->ikan?></td>
                    <td align="right"><?= number_format($rt->jumlah,0,'','.');?></td>
                    <td align="center">
                            <?php echo anchor(base_url().'kontes/cetakkwitansi?no='.$rt->no_pendaftaran,'<i class="fa fa-print"></i>','class=" btn btn-xs btn-info" onclick="window.open(this.href); return false;" onkeypress="window.open(this.href); return false;"')?>
                    </td>
                </tr>
            <?php $no++; } ?>
        </tbody>
    </table>
  </div>
   <div class="tab-pane " id="lunas">
    <table class="table table-striped table-bordered table-hover tb">
        <thead>
            <tr>
                <th rowspan="2" width="5%">No</th>
                <th rowspan="2">No Pendaftaran</th>
                <th rowspan="2">No Kwitansi</th>
                <th colspan="2">Handling</th>
                <th colspan="2">Owner</th>
                <th rowspan="2">Ikan</th>
                <th rowspan="2">Total</th>
                <th rowspan="2" width="15%"></th>
            </tr>
            <tr>
                <th>Nama</th>
                <th>Kota</th>
                <th>Nama</th>
                <th>Kota</th>
            </tr>
        </thead>
        <tbody>
            <?php $nao=1; foreach($lunas as $rta){?>
                <tr  id="<?= $rta->no_pendaftaran ?>">
                    <td align="center"><input type="checkbox" id="cekbox" name="cekbox[]" value="<?= $rta->no_pendaftaran ?>"/> <?= $nao ?></td>
                    <td><a href="#"> <?= $rta->no_pendaftaran ?></a><span class="badge <?php if($rta->st_entri=='online'){ echo " badge-success";}else{echo " badge-primary";}?>"><?= $rta->st_entri ?></span></td>
                    <td align="center"><?= $rta->no_kwitansi ?></td>
                    <td><?= $rta->nama_handling ?></td>
                    <td><?= $rta->kota_handling ?></td>
                    <td><?= $rta->nama_owner?></td>
                    <td><?= $rta->kota_owner?></td>
                    <td><?= $rta->ikan?></td>
                    <td align="right"><?= number_format($rta->jumlah,0,'','.');?></td>
                    <td align="center">
                            <?php echo anchor(base_url().'kontes/cetakkwitansi?no='.$rta->no_pendaftaran,'<i class="fa fa-print"></i>','class=" btn btn-xs btn-info" onclick="window.open(this.href); return false;" onkeypress="window.open(this.href); return false;"'); 
                            echo anchor('kontes/cetaklabelikan/'.$rta->no_pendaftaran,'<i class="fa fa-image"></i>','class="btn btn-xs purple" data-togle="tooltips" title="Label ikan" target="_blank"') ;
                            ?>
                    </td>
                </tr>
            <?php $nao++; } ?>
        </tbody>
    </table>
  </div>
</div>

 <script type="text/javascript">

     $( document ).ready(function() {
 
            $(document).on('click','#bayar_banyak',function(e){
                e.preventDefault();
                var val = [];
                $(':checkbox:checked').each(function(i){
                  val[i] = $(this).val();
                });

                if(val!=''){
                var go_to_url ="<?php echo base_url(); ?>kontes/aksibayarkontesbanyak?no="+val;
                  window.location.href=go_to_url;
                }

            });

  
 

        $(document).on('click','#cetak_banyak',function(e){
            e.preventDefault();
            $("#cetak").html('');
            var val = [];
            $(':checkbox:checked').each(function(i){
              val[i] = $(this).val();
            });

            if(val!=''){
                valu=val.join();
           
                 var go_to_url ="<?php echo base_url(); ?>kontes/cetakkwitansibanyak?no="+valu;
                  window.open(go_to_url, '_blank');
            }
        });

         $(document).on('click','#label',function(e){
                e.preventDefault();
                var val = [];
                $(':checkbox:checked').each(function(i){
                  val[i] = $(this).val();
                });

                if(val!=''){
                    // alert(val);
                    var url = "<?php echo base_url() ; ?>"+"kontes/cetaklabelikanbanyak?nomer="+val;
                    window.open(url, '_blank');
                }
            });

    });



     
 

 </script>



