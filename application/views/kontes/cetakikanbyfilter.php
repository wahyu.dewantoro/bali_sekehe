<!DOCTYPE html>
<html>
<head>
	<title>Data Ikan</title>
</head>
<body>
	<table cellpadding="3" cellspacing="0" border="1" style="font-size: 12px">
		<thead>
			<tr>
				
				
				
				
				<th rowspan="2">No</th>
				<th colspan="6">Ikan</th>
				<th colspan="3">Handling</th>
				<th rowspan="2">Keterangan</th>
			</tr>
			<tr>
				<th>ID</th>
				<th>Kelas</th>
				<th>Variety</th>
				<th>Ukuran</th>
				<th>M/F</th>
				<th>Breeder</th>
				<th>Nama</th>
				<th>Kota</th>
				<th>Telp</th>
			</tr>
		</thead>
		<tbody>
			<?php $no=1; foreach($ikan as $rk){?>
				<tr>
					<td align="center"><?= $no;?></td>
					<td><?= $rk->no_ikan ?></td>
					<td align="center"><?= $rk->kat_ikan ?></td>
					<td><?= $rk->nm_ikan ?></td>
					<td><?= $rk->ukuran ?> cm</td>
					<td><?= $rk->gender ?></td>
					<td><?= $rk->asal ?></td>
					<td><?= $rk->nama ?></td>
					<td><?= $rk->kota ?></td>
					<td><?= $rk->telp ?></td>
					<td></td>
				</tr>
			<?php $no++;} ?>
		</tbody>


	</table>
</body>
</html>