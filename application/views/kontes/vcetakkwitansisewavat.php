<style type="text/css">
	#watermark {
        position: fixed;
        text-align: center;
        /*bottom:   27cm;*/
        left:     5cm;
        font-size: 100px;
        opacity: 0.1;
        /** Change image dimensions**/
/*        width:    8cm;
        height:   8cm;
*/
        /** Your watermark should be behind every content**/
        z-index:  -1000;
           transform: rotate(0deg);
    }
</style>
<h2 style="text-align: center;" > Lomba Merah Putih Koi Show 2019</h2><hr>
<h3  style="text-align: center;"><b>Bukti Pembayaran Vat</b></h3>
<div id="watermark">
 <p>ASLI</p>           
 </div>
<table border="0">
	
	<!-- <tr valign="top">
		<td>Tanggal</td>
		<td>: <?php // longdate_indo(date('Y-m-d',strtotime($rk->tanggal_bayar)))?></td>
	</tr> -->
	<tr valign="top">
		<td>Terima dari</td>
		<td>: <?= $rk->penyewa?></td>
	</tr>
	<tr valign="top">
		<td>Keterangan</td>
		<td>: biaya sewa vat sebanyak <?= $rk->jumlah ?> buah (<?= $rk->jenis ?>) / @Rp.<?php echo number_format($rk->harga,0,'','.')?></td>
	</tr>
	<tr valign="top">
		<td>Jumlah</td>
		<td>: Rp. <?= number_format($rk->total,0,'','.')?></td>
	</tr>
	<tr valign="top">
		<td>Terbilang</td>
		<td>: <b><?= terbilang($rk->total)  ?> Rupiah</b></td>
	</tr>
</table>
<table width="100%">
	<tr valign="bottom">
		<td width="50%">
			<i>Noted: Kwitansi sah apabila di terdapat tanda tangan panitia dan  ber 	stempel.
			</i>
		</td>
		<td align="center">
			Denpasar, <?= date_indo(date('Y-m-d'))?><br>
			Panitia<br><br><br>

			Bendahara
		</td>
	</tr>
</table>
<br><br><br>
<h3 style="text-align: center;">------------------------------------------------------------------------------------</h3>
<br>

<h2 style="text-align: center;" > Lomba Merah Putih Koi Show 2019</h2><hr>
<h3  style="text-align: center;"><b>Bukti Pembayaran Vat</b></h3>
<div id="watermark">
 <p>COPY</p>           
 </div>
<table border="0">
	
	<!-- <tr valign="top">
		<td>Tanggal</td>
		<td>: <?php // longdate_indo(date('Y-m-d',strtotime($rk->tanggal_bayar)))?></td>
	</tr> -->
	<tr valign="top">
		<td>Terima dari</td>
		<td>: <?= $rk->penyewa?></td>
	</tr>
	<tr valign="top">
		<td>Keterangan</td>
		<td>: biaya sewa vat sebanyak <?= $rk->jumlah ?> buah (<?= $rk->jenis ?>) / @Rp.<?php echo number_format($rk->harga,0,'','.')?></td>
	</tr>
	<tr valign="top">
		<td>Jumlah</td>
		<td>: Rp. <?= number_format($rk->total,0,'','.')?></td>
	</tr>
	<tr valign="top">
		<td>Terbilang</td>
		<td>: <b><?= terbilang($rk->total)  ?> Rupiah</b></td>
	</tr>
</table>
<table width="100%">
	<tr valign="bottom">
		<td width="50%">
			<i>Noted: Kwitansi sah apabila di terdapat tanda tangan panitia dan  ber 	stempel.
			</i>
		</td>
		<td align="center">
			Denpasar, <?=  date_indo(date('Y-m-d',strtotime($rk->tanggal_bayar)))
 ?><br>
			Panitia<br><br><br>

			Bendahara
		</td>
	</tr>
</table>