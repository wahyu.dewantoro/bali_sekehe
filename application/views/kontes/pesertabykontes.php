<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            Data Kontes
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Data Peserta</span>
        </li>
    </ul>
    <div class="page-toolbar">
         <div class="pull-right">
             <button class="btn btn-sm btn-info" id="label"><i class="fa fa-image"></i> Label</button>
             <!-- <button class="btn btn-sm btn-primary" id="kartu"><i class="fa fa-file-text"></i> Kartu</button> -->
         </div>
    </div>
</div>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-list"></i>Data</div>
    </div>
    <div class="portlet-body">
        
        <table class="table table-striped table-bordered table-hover" id="tb" >
            <thead>
                <tr>
                    <th width="5%">No</th>
                    <th>Kode</th>
                    <th>Handling</th>
                    <th>Kota</th>
                    <th>Pemilik</th>
                    <th>Kota</th>
                    <th>Ikan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php $no=1; foreach($rk as $rk){?>
                <tr>
                    <td align="center">
                        <input type="checkbox" id="cekbox" name="cekbox[]" value="<?= $rk->no_pendaftaran ?>"/>
                        <?php echo $no?></td>
                    <td align="center">

                        <?php
                           echo anchor('kontes/detailpesertakontes/'.$rk->id_inc,$rk->no_pendaftaran,' data-togle="tooltips" title="Detail"') ;
                          ?> </td>
                    <td><?php echo $rk->handling;?> </td>
                    <td><?php echo $rk->kota_handling;?> </td>
                    <td><?php echo $rk->pemilik_ikan;?> </td>
                    <td><?php echo $rk->kota_pemilik;?> </td>
                    <td align="center"><?php echo $rk->ikan;?> </td>
                    <td align="center"> 
                           

                        <?php 
                        echo anchor('kontes/cetaklabelikan/'.$rk->no_pendaftaran,'<i class="fa fa-image"></i>','class="btn btn-xs purple" data-togle="tooltips" title="Label ikan" target="_blank"') ;
                        // echo anchor('kontes/kelengkapankontes/'.$rk->no_pendaftaran,'<i class="fa fa-file-text"></i>','class="btn btn-xs btn-warning" data-togle="tooltips" title="Kartu lomba" target="_blank"') ;
                        
                         
                        ?> 
                    </td>
                </tr>
                <?php $no++; }?>
            </tbody>
        </table>
       
    </div>
</div>
<div class="visible-print"  id="cetak"></div>
<script type="text/javascript">
    $( document ).ready(function() {
        $(document).on('click','#label',function(e){
                e.preventDefault();
                var val = [];
                $(':checkbox:checked').each(function(i){
                  val[i] = $(this).val();
                });

                if(val!=''){
                    // alert(val);
                    var url = "<?php echo base_url() ; ?>"+"kontes/cetaklabelikanbanyak?nomer="+val;
                    window.open(url, '_blank');
                }
            });


        $(document).on('click','#kartu',function(e){
                e.preventDefault();
                var val = [];
                $(':checkbox:checked').each(function(i){
                  val[i] = $(this).val();
                });

                if(val!=''){
                    // alert(val);
                    var url = "<?php echo base_url() ; ?>"+"kontes/kelengkapankontesbanyak?nomer="+val;
                    window.open(url, '_blank');
                }
            });
    });

    
</script>