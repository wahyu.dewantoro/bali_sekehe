        <style type="text/css">
            .center {margin-left: auto; margin-right: auto; }
        .responsive {width: auto; /*max-width: */ height: 150px; }
        </style>
        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    Data kontes
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Data Peserta</span>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Detail</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div class="btn-group pull-right">
                                    <?php if($hand==false){?>
                                    <a class="btn btn-sm blue btn-outline" href="<?php echo base_url().'kontes/formeditpeserta/'.$rp->id_inc?>" data-target="#ajax" data-toggle="modal"><i class="fa fa-edit"></i> Edit</a>
                                    <?php 
                                }
                                    echo anchor(site_url('kontes/pesertabykontes'), '<i class="fa fa-list"></i> list','class="btn purple btn-sm btn-outline"'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-list"></i>Data Peserta
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>No Pendaftaran</td>
                                                    <td>:</td>
                                                    <td><?= $rp->no_pendaftaran?></td>
                                                </tr>
                                                <tr>
                                                    <td>Daftar</td>
                                                    <td>:</td>
                                                    <td><?= $rp->tanggal_daftar?></td>
                                                </tr>
                                                <tr>
                                                    <td>Handling</td>
                                                    <td>:</td>
                                                    <td><?= $rp->handling?> - <?= $rp->kota_handling?></td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>Owner</td>
                                                    <td>:</td>
                                                    <td><?= $rp->pemilik_ikan?> - <?= $rp->kota_pemilik?></td>
                                                </tr>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                          
                            <div class="col-md-8">
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-list"></i> Data Ikan</div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                               <div class="table-responsive">
                                                <?php 
                                                    $kolom  = 4; 
                                                    $gambar = array_chunk($listikan,$kolom);
                                                            echo '<table border="0" cellpadding="10">';
                                                            foreach ($gambar as $chunk) {
                                                                echo '<tr >';
                                                                foreach ($chunk as $ikane) {
                                                                    echo '<td valign="top" width="150px">';?>
                                                                    <table  style=" font-size:12px">
                                                                        <tr>
                                                                            <td valign="top"   ><img class="responsive" src="<?php echo base_url().$ikane->gambar_ikan?>"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center"><button data-gambar='<?= $ikane->gambar_ikan ?>' class="rotasi btn btn-xs btn-success"><i class="fa fa-rotate-right"></i></button> <a class="btn btn-xs purple" href="<?php echo base_url().'kontes/editikan/'.$ikane->id_inc?>" data-target="#ajax" data-toggle="modal"><i class="fa fa-edit"></i> </a> </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>No Ikan : <b><?php echo  $ikane->no_ikan ?></b> </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td  valign="top" ><?php echo $ikane->nm_jenis?> / <?php echo $ikane->ukuran?> cm </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td  valign="top" ><?php echo $ikane->gender?> / <?php echo $ikane->asal?> </td>
                                                                        </tr>
                                                                    </table>
                                                                    <?php echo '</td>';
                                                                }
                                                                echo '</tr>';
                                                            }
                                                            echo '</table>';

                                                         ?>

                                               
                                                </div>
                                            </div>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
 
 <div class="modal fade" id="ajax" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                
                <span><i class="fa fa-refresh"></i> &nbsp;&nbsp;Loading... </span>
            </div>
        </div>
    </div>
</div>

 
 
<script type="text/javascript">
    $( document ).ready(function() {
 
            $(document).on('click','.rotasi',function(e){
                e.preventDefault();
                    var gambar=$(this).attr('data-gambar');
                    
                    $.post("<?php echo base_url().'kontes/rotasi_gambar'?>",
                    {gbr:gambar},
                    function(html){
                         // location.reload(true);
                         window.location.reload(true);
                    }
                );

               
            });
    });
</script>