
		
	
<form method="post" class="form-horizontal" action="<?php echo base_url().'refkontes/proseseditbak'?>">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Edit Data Vat</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
				<input type="hidden" name="id_inc" value="<?= $rk->id_inc?>">
				<input type="hidden" name="ms_kontes_id" value="<?= $rk->ms_kontes_id?>">
				<div class="form-group has-success">
					<label class="control-label col-md-2">Vat <small>*</small></label>
					<div class="col-md-10">
						<input type="text" name="nama_bak" value="<?= $rk->nama_bak?>" class="form-control" required>
					</div>
				</div>
				<div class="form-group has-success">
					<label class="control-label col-md-2">Jumlah <small>*</small></label>
					<div class="col-md-10">
						<input type="text" name="jumlah" value="<?= $rk->jumlah?>" onkeypress="return hanyaAngka(event)" class="form-control" required>
					</div>
				</div>
				<div class="form-group has-success">
					<label class="control-label col-md-2">Biaya <small>*</small></label>
					<div class="col-md-10">
						<input type="text" name="biaya" value="<?= $rk->biaya?>" class="form-control" onkeypress="return hanyaAngka(event)" required>
					</div>
				</div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="submut" class="btn blue"><i class="fa fa-save"></i> Submit</button>
</div>
</form>

<script type="text/javascript">
    function hanyaAngka(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
         
            return false;
            return true;
        }
</script>