<div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    Bank Data
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Kontes</span>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span><?php echo $button ?></span>
                </li>
            </ul>
        </div>
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="fa fa-file-text font-green-haze"></i>
                    <span class="caption-subject bold"> <?php echo $button ?></span>
                </div>
            </div>
            <div class="portlet-body form">
                <form action="<?php echo $action; ?>" method="post" class="form-horizontal" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input  has-success">
                                <label class="col-md-4 control-label" for="varchar">  Kontes <span class="required"> * </span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="nama_kontes" id="nama_kontes"  value="<?php echo $nama_kontes; ?>" required />
                                </div>
                            </div>
                            <div class="form-group form-md-line-input  has-success">
                                <label class="col-md-4 control-label" for="date">  Mulai <span class="required"> * </span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control  date-picker" name="tanggal_mulai" id="tanggal_mulai"  value="<?php echo $tanggal_mulai; ?>" required />
                                </div>
                            </div>
                            <div class="form-group form-md-line-input  has-success">
                                <label class="col-md-4 control-label" for="date">  Selesai <span class="required"> * </span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control date-picker" name="tanggal_selesai" id="tanggal_selesai"  value="<?php echo $tanggal_selesai; ?>" required />
                                </div>
                            </div>
                            <div class="form-group form-md-line-input  has-success">
                                <label class="col-md-4 control-label" for="varchar">Lokasi <span class="required"> * </span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="tempat_kontes" id="tempat_kontes"  value="<?php echo $tempat_kontes; ?>" required />
                                </div>
                            </div>
                            <div class="form-group form-md-line-input  has-success">
                                <label class="col-md-4 control-label" for="varchar">Logo<span class="required"> * </span></label>
                                <div class="col-md-8">
                                    <input type="file" class="form-control" accept="image/*" name="logo_kontes" id="logo_kontes"  value="<?php echo $logo_kontes; ?>" required />
                                </div>
                            </div>
                            <div class="form-group form-md-line-input  has-success">
                                <label class="col-md-4 control-label" for="int">Pendaftaran<span class="required"> * </span></label>
                                <div class="col-md-8">
                                    <!-- <input type="text"   value="<?php echo $status_kontes; ?>" required /> -->
                                    <select class="form-control" name="status_kontes" id="status_kontes">
                                        <option value='0'>Tutup</option>
                                        <option value='1'>Buka</option>
                                    </select>
                                </div>
                            </div>
                            <?php foreach($bak as $bak){?>
                                <div class="form-group form-md-line-input  has-success">
                                <label class="col-md-4 control-label">Bak <?php echo $bak->nama_bak?></label>
                                <div class="col-md-5">
                                    <input type="hidden" name="nama_bak[]" value="<?php echo $bak->nama_bak ?>">
                                    <input type="number" placeholder="Biaya sewa" class="form-control" onkeypress="return hanyaAngka(event)" name="biayabak[]">
                                </div>
                                <div class="col-md-3">
                                    <input type="number" class="form-control" placeholder="Jumlah" onkeypress="return hanyaAngka(event)" name="jumlah[]">
                                </div>
                            </div>
                            <?php }?>
                        </div>
                        <div class="col-md-6">
                       
                            <?php foreach($biaya as $rb){?>
                            <div class="form-group form-md-line-input  has-success">
                                <label class="col-md-4 control-label">Biaya <?php echo $rb->min?> cm - <?php echo $rb->max?> cm</label>
                                <div class="col-md-8">
                                    <input type="hidden" name="minbiaya[]" value="<?php echo $rb->min ?>">
                                    <input type="hidden" name="maxbiaya[]" value="<?php echo $rb->max ?>">
                                    <input type="number" class="form-control"  onkeypress="return hanyaAngka(event)" name="biayaukuran[]">
                                </div>
                            </div>
                            <?php }?>
                            
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-sm blue"><i class="fa fa-save"></i> Simpan</button> 
                                    <a href="<?php echo site_url('refkontes') ?>" class="btn btn-sm red"><i class="fa fa-close"></i> Batal</a> 
                                </div>
                            </div>
                        </div>
                    </div>

        	    
                
        	   
        	</form>
        </div>
</div>
<script type="text/javascript">
    function hanyaAngka(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
         
            return false;
            return true;
        }
</script>