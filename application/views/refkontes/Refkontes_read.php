<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            Bank Data
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Kontes</span>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Detail</span>
        </li>
    </ul>
    <div class="page-toolbar">
        <div class="btn-group pull-right">
            <?php echo anchor(site_url('refkontes'), '<i class="fa fa-list"></i> List','class="btn purple btn-sm btn-outline"'); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Data Kontes
                </div>
                <div class="actions">
                    <a class="btn btn-xs purple btn-outline" href="<?php echo base_url().'refkontes/formeditkontes/'.$kontes->id_inc?>" data-target="#ajax" data-toggle="modal"><i class="fa fa-edit"></i>  </a>
                     <!-- <a class="btn purple btn-sm" data-toggle="modal" href="#draggable"><i class="fa fa-edit"></i> Edit  </a> -->
                </div>

            </div>
            <div class="portlet-body">
                <table class="table">
                  <tbody>
                   <tr>
                        <td width="15%">Kontes</td>
                        <td width="1%">:</td>
                        <td><?= $kontes->nama_kontes?></td>
                    </tr>
                   <tr>
                        <td>Mulai</td>
                        <td>:</td>
                        <td><?= $kontes->tanggal_mulai?></td>
                    </tr>
                   <tr>
                        <td>Selesai</td>
                        <td>:</td>
                        <td><?= $kontes->tanggal_selesai?></td>
                    </tr>
                   <tr>
                        <td>Lokasi</td>
                        <td>:</td>
                        <td><?= $kontes->tempat_kontes?></td>
                    </tr>
                   <tr>
                        <td>Pendaftaran</td>
                        <td>:</td>
                        <td><?= $kontes->pendaftaran?></td>
                    </tr>
                   <tr>
                        <td>Logo kontes</td>
                        <td>:</td>
                        <td><img width="300px" src="<?= base_url().$kontes->logo_kontes?>"> </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>        
    </div>
    <div class="col-md-6">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Biaya Pendaftaran & Sewa Vat</div>
            </div>
            <div class="portlet-body">

            <table class="table  table-bordered">
                <thead>
                    <tr>
                        <th>Bak</th>
                        <th>Jumlah</th>
                        <th>Biaya</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($bak as $rn){?>
                    <tr>
                        <td><?php echo  $rn->nama_bak ?></td>
                        <td align="center"><?php echo  $rn->jumlah ?></td>
                        <td align="right"><?php echo number_format($rn->biaya,'0','','.')?></td>
                        <td align="center"><a class="btn btn-xs blue btn-outline" href="<?php echo base_url().'refkontes/formeditbak/'.$rn->id_inc?>" data-target="#ajax" data-toggle="modal"><i class="fa fa-edit"></i>  </a></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <hr>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Ukuran</th>
                        <th>Biaya</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no=1; foreach($biaya as $rb){?>
                    <tr>
                        <td align="center"><?= $no?></td>
                        <td><?php echo $rb->ukuran?></td>
                        <td align="right"><?php echo number_format($rb->biaya,'0','','.')?></td>
                    </tr>
                    <?php $no++;}?>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>




<div class="modal fade" id="ajax" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                
                <span><i class="fa fa-refresh"></i> &nbsp;&nbsp;Loading... </span>
            </div>
        </div>
    </div>
</div>
