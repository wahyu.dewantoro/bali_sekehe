 
            <form method="post" action="<?php echo base_url().'refkontes/prosesupdate'?>"  enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Edit Data Kontes</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Kontes</label>
                        <input type="hidden" name="id_inc" value="<?php echo $kontes->id_inc?>">
                        <input type="text" name="nama_kontes" class="form-control" value="<?php echo $kontes->nama_kontes?>">
                    </div>
                    <div class="form-group">
                        <label>Mulai</label>
                        <input type="text" name="tanggal_mulai" class="form-control date-picker" value="<?php echo date('Y-m-d',strtotime($kontes->tanggal_mulai)) ?>">
                    </div>
                    <div class="form-group">
                        <label>Selesai</label>
                        <input type="text" name="tanggal_selesai" class="form-control  date-picker" value="<?php echo date('Y-m-d',strtotime($kontes->tanggal_selesai)) ?>">
                    </div>
                    <div class="form-group">
                        <label>Lokasi</label>
                        <input type="text" name="tempat_kontes" class="form-control" value="<?php echo $kontes->tempat_kontes?>">
                    </div>
                    <div class="form-group">
                        <label>Pendaftaran</label>
                        <select  name="status_kontes" class="form-control">
                            <option value='0'>Tutup</option>
                            <option <?php if($kontes->status_kontes=='1'){echo "selected";} ?> value='1'>Buka</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Logo</label>
                        <input type="hidden" name="lawas" value="<?php echo $kontes->logo_kontes?>">
                        <input type="file" name="logo_kontes" class="form-control" accept="image/*">
                        <img src="<?php echo base_url().$kontes->logo_kontes?>" width='200px'>
                    </div>
                    <div class="form-group">
                        <label>Headjudge</label>
                        <input type="text" name="nama_headjudge" value="<?= $kontes->nama_headjudge?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>TTD headjudge</label>
                        <input type="hidden" name="lawasttdhj" value="<?= $kontes->ttd_headjudge?>">
                        <input type="file" accept="image/*" name="ttd_headjudge"  class="form-control">
                        <?php if($kontes->ttd_headjudge!=''){?>
                            <image src="<?php echo base_url().$kontes->ttd_headjudge?>" width="200px" >
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <label>Chairman</label>
                        <input type="text" name="nama_chairman" value="<?= $kontes->nama_chairman?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>TTD Chairman</label>
                        <input type="hidden" name="lawasttdcm" value="<?= $kontes->ttd_chairman?>">
                        <input type="file" name="ttd_chairman"  class="form-control" accept="image/*">
                        <?php if($kontes->ttd_chairman!=''){?>
                            <image src="<?php echo base_url().$kontes->ttd_chairman?>" width="200px" >
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <label>Chairman Of Commite</label>
                        <input type="text" name="nama_commite" value="<?= $kontes->nama_commite?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>TTD Chairman Of Commite</label>
                        <input type="hidden" name="lawasttdcmc" value="<?= $kontes->ttd_commite?>">
                        <input type="file" name="ttd_commite"  class="form-control" accept="image/*">
                        <?php if($kontes->ttd_commite!=''){?>
                            <image src="<?php echo base_url().$kontes->ttd_commite?>" width="200px" >
                        <?php } ?>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn green"><i class="fa fa-save"></i> Submit</button>
                </div>
            </form>
<script type="text/javascript">
      //init date pickers
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });
</script>