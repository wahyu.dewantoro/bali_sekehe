<div class="page-bar">
    <ul class="page-breadcrumb">
        <li> Participants <i class="fa fa-circle"></i></li>
        <li> <span>Handling</span> <i class="fa fa-circle"></i></li>
        <li> <span><?php echo $hand->nama?></span> <i class="fa fa-circle"></i> </li>
        <li> <span><?php echo $hand->kota?></span> </li>
    </ul>
    <div class="page-toolbar">
        <div class="btn-group pull-right">
            <?php  if($uh==false){ echo anchor(site_url('refhandling'), '<i class="fa fa-list"></i> List','class="btn purple btn-sm btn-outline"');} ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption"> <i class="fa fa-edit"></i>Tambah Owner</div>
            </div>
            <div class="portlet-body">
                <form class="form-horizontal" method="post" action="<?php echo base_url().'refhandling/prosesaddowner'?>"> 
                    <div class="form-group has-success">
                        <input type="hidden" name="ms_handling_id" value="<?php echo $hand->id_inc?>">
                        <label class="col-md-3 control-label">Owner <span>*</span></label>
                        <div class="col-md-9">
                            <input type="text" name="nama" id="nama" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group has-success">
                        <label class="col-md-3 control-label">Kota <span>*</span></label>
                        <div class="col-md-9">
                            <input type="text" name="kota" id="kota" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button class="btn btn-sm btn-info" type="submit"><i class="fa fa-save"></i> Simpan</button>
                            <button class="btn btn-sm btn-warning" type="reset"><i class="fa fa-refresh"></i> Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-users"></i> Owner
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Nama</th>
                            <th>Kota</th>
                            <th width="30%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $nn=1; foreach($owner as $ow){?>
                        <tr>
                            <td align="center"><?php echo $nn?></td>
                            <td><?php echo  anchor('daftar/daftarfromkontes/'.$ow->id_inc, $ow->nama); ?></td>
                            <td><?php echo $ow->kota?></td>
                            <td align="center">
                                <?php echo anchor('daftar/daftarfromkontes/'.$ow->id_inc,'<i class="fa fa-plus"></i>  ikan','class="btn btn-xs btn-outline purple"'); 
                                 if($this->session->userdata('wob_pengguna')==$ow->pengguna_id or $this->session->userdata('wob_role')==1){ ?>
                                    <button class="btn edit green btn-xs btn-outline" data-id="<?= $ow->id_peserta ?>"><i class="fa fa-edit"></i> Edit</button>

                                  <?php echo anchor('refhandling/hapushandlingowner/'.$ow->id_inc.'/'.$ow->ms_handling_id,'<i class="fa fa-trash"></i> hapus','class="btn btn-xs btn-outline red" onclick="return confirm(\'Apakah anda yakin hapus ?\')"');}?>
                            </td>
                        </tr>
                        <?php $nn++; }?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

  <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                  <form action="<?= base_url().'refhandling/updateowner'?>" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Edit Data</h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="simpan btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        
                    </div>
                  </form>
                </div>
            </div>
        </div>
<script type="text/javascript">
     $(function(){
            $(document).on('click','.edit',function(e){
                e.preventDefault();
                $("#myModal").modal('show');
                $.post("<?= base_url().'refhandling/dataowner'?>",
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }
                );
            });
        });

</script>