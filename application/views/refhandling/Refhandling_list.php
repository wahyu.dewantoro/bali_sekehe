<div class="page-bar">

    <ul class="page-breadcrumb">

        <li>

            Participants 

            <i class="fa fa-circle"></i>

        </li>

        <li>

            <span>Handling</span>

        </li>

    </ul>

    <div class="page-toolbar">

        <div class="btn-group pull-right">

            <?php echo anchor(site_url('refhandling/create'), '<i class="fa fa-plus"></i> Tambah','class="btn purple btn-sm btn-outline"'); ?>

        </div>

    </div>

</div>

<div class="portlet box green">

    <div class="portlet-title">

        <div class="caption">

            <i class="fa fa-list"></i>Data</div>

    </div>

    <div class="portlet-body">

        <table class="table table-striped table-bordered table-hover" id="tb" >

            <thead>

                <tr>

                    <th width ="5%">No</th>

                    <th>Nama</th>

                    <th>Kota</th>

                    <th>Telepon</th>

                    <th>Jumlah Owner</th>

                    <th width="15%">Aksi</th>

                </tr>

            </thead>

	    <tbody>

            <?php

            $start = 0;

            foreach ($refhandling_data as $refhandling)

            {

                ?>

                <tr>

        		    <td align='center'><?php echo ++$start ?></td>

        		    <td>

                    <?php    echo anchor(site_url('refhandling/detail/'.$refhandling->id_inc),$refhandling->nama); ?>

                    </td>

        		    <td><?php echo $refhandling->kota ?></td>

        		    <td><?php echo $refhandling->telp ?></td>

                    <td><?php echo $refhandling->owner ?></td>

        		    <td style="text-align:center" >

                        <div class="btn-group">

                            <?php 

            			         echo anchor(site_url('refhandling/detail/'.$refhandling->id_inc),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-info"'); 

                                echo anchor(site_url('refhandling/update/'.$refhandling->id_inc),'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"'); 

                                if($refhandling->owner==0){ if($refhandling->pengguna_id==$pengguna_id || $this->session->userdata('wob_role')=='1' ){	echo anchor(site_url('refhandling/delete/'.$refhandling->id_inc),'<i class="fa fa-trash"></i>','onclick="javasciprt: return confirm(\'apakah anda yakin hapus ?\')" class="btn btn-xs btn-danger"'); }}

            			         ?>

                        </div>

                    </td>

	           </tr>

                <?php } ?>

        </tbody>

    </table>

</div>

</div>

       