<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            Report
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>General</span>
        </li>
    </ul>
    <div class="page-toolbar">
        <div class="btn-group pull-right">

        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
          <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Rekap per Range Ukuran</div>
            </div>
            <div class="portlet-body">
                <table class="table tb table-striped table-bordered table-hover" >
                    <thead>
                        <tr>
                            <th width ="5%">No</th>
                            <th>Range Ukuran</th>
                            <th>Jumlah</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $uk=1; foreach($ukuran as $ru){?>
                        <tr>
                            <td align="center"><?php echo $uk?></td>
                            <td><?php echo $ru->range_ukuran ?> cm</td>
                            <td align="center"><?php echo $ru->jumlah?> ekor</td>
                        </tr>
                        <?php $uk++;}?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Rekap per Variety</div>
            </div>
            <div class="portlet-body">
                <table class=" tb table table-striped table-bordered table-hover" >
                    <thead>
                        <tr>
                            <th width ="5%">No</th>
                            <th>Variety</th>
                            <th>Jumlah</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $vr=1; foreach($variety as $rv){?>
                        <tr>
                            <td align="center"><?php echo $vr?></td>
                            <td><?php echo $rv->variety ?> </td>
                            <td align="center"><?php echo $rv->jumlah?> ekor</td>
                        </tr>
                        <?php $vr++;}?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
</div>
<div class="row">
    <div class="col-md-6">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Rekap Ikan per Handling</div>
            </div>
            <div class="portlet-body">
                <table class=" tb table table-striped table-bordered table-hover" >
                    <thead>
                        <tr>
                            <th width ="5%">No</th>
                            <th>Handling</th>
                            <th>Kota</th>
                            <th>Jumlah</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $hd=1; foreach($handling as $rh){?>
                        <tr>
                            <td align="center"><?php echo $hd?></td>
                            <td><?php echo $rh->nama ?> </td>
                            <td><?php echo $rh->kota ?> </td>
                            <td align="center"><?php echo $rh->jumlah?> ekor</td>
                        </tr>
                        <?php $hd++;}?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
     <div class="col-md-6">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Rekap Ikan per Owner</div>
            </div>
            <div class="portlet-body">
                <table class=" tb table table-striped table-bordered table-hover" >
                    <thead>
                        <tr>
                            <th width ="5%">No</th>
                            <th>Owner</th>
                            <th>Kota</th>
                            <th>Jumlah</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $ow=1; foreach($owner as $ro){?>
                        <tr>
                            <td align="center"><?php echo $hd?></td>
                            <td><?php echo $ro->nama ?> </td>
                            <td><?php echo $ro->kota ?> </td>
                            <td align="center"><?php echo $ro->jumlah?> ekor</td>
                        </tr>
                        <?php $ow++;}?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
