 <div class="modal-content">
  
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-title">Detail Poin handling : <?php echo $handling?></h4>
    </div>
    <div class="modal-body"> 
    	<table class="table table-bordered table-striped">
    			<thead>
    				<tr>
    					<th>#</th>
    					<th>Owner</th>
    					<th>Id Ikan</th>
    					<th>Variety</th>
    					<th>Ukuran</th>
    					<th>Prize</th>
    					<th>Poin</th>
    				</tr>
    			</thead>
    			<tbody>
    				<?php $no=1; foreach($rk as $rk){?>
    				<tr>
    					<td align="center"><?php echo $no?></td>
    					<td><?php echo $rk->nama.' - '.$rk->kota?></td>
    					<td align="center"><?php echo $rk->no_ikan?></td>
    					<td><?php echo $rk->nm_ikan?></td>
    					<td align="center"><?php echo $rk->ukuran?> cm</td>
    					<td><?php echo $rk->nama_juara?></td>
    					<td align="center"><?php echo number_format($rk->poin,'0','','.') ?></td>
    				</tr>
    				<?php $no++; }?>
    			</tbody>
    	</table>
    </div> 
</div>  