<style type="text/css">
    .center {
        margin-left: auto;
        margin-right: auto;
    }
    
    #customers {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
        font-size: 12px;
    }

    #customers td, #customers th {
        border: 1px solid #ddd;
        padding: 3px;
    }

    #customers tr:nth-child(even){background-color: #f2f2f2;}

    #customers tr:hover {background-color: #ddd;}

    #customers th {
        padding-top: 3px;
        padding-bottom: 3px;
        text-align: center;
        background-color: #ccffcc;
        color: black;
        border:0;
    }

    
    #customersas {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
        font-size: 12px;
    }

    #customersas td, #customersas th {
        border: 1px solid #ddd;
        padding: 3px;
    }

    #customersas tr:nth-child(even){background-color: #f2f2f2;}

    #customersas tr:hover {background-color: #ddd;}

    #customersas th {
        padding-top: 3px;
        padding-bottom: 3px;
        text-align: center;
        background-color: #99ffcc;
        color: black;
        border:0;
    }
   </style>


                <table id="customers">
                    <thead>
                        <tr>
                            <th width ="5%">No</th>
                            <th>Handling</th>
                            <th>Kota</th>
                            <th>Owner</th>
                            <th>Ikan</th>
                   
                            <th>Biaya Ikan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $start = 0; $t=0;
                        foreach ($rk as $rk)
                        {
                            ?>
                            <tr>
                                <td align='center'><?php echo ++$start ?></td>
                                <td><?= $rk->handling?></td>
                                <td><?= $rk->kota_handling?></td>
                                <td align="center"><?= $rk->owner?> orang</td>
                                <td align="center"><?= $rk->ikan?> ekor</td>
               
                                <td align="right"><?= number_format($rk->biaya_ikan,'0','','.');?></td>
                            </tr>
                            <?php  $t+=$rk->biaya_ikan;  } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5"><b>Total</b></td>
                                <td align="right"><b><?= number_format($t,'0','','.');?></b></td>
                            </tr>
                        </tfoot>
                </table>
           