<style>
.flex-container {
  display: flex;
  flex-wrap: nowrap;
  /*background-color: DodgerBlue;*/
}

.flex-container > div {
  /*background-color: #f1f1f1;*/
  width: 300px;
  margin: 10px;
  text-align: center;
  line-height: 50px;
  font-size: 25px;
  border: 1px solid;
}
</style>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            Report
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Label Peserta</span>
        </li>
    </ul>
    <div class="page-toolbar">
        <a href="<?php echo base_url('laporan/cetaklabelserti')?>" target="_blank" class="btn btn-sm btn-warning"><i class="fa fa-file"></i> PDF</a>
    </div>
</div>
<div class="row">
    <div>
        <div class="flex-container">
            <?php foreach($rk as $rk){?>
              <div class="col-md-3"> <?php echo ucwords($rk->handling).' - '.$rk->kota?><br> <?php echo $rk->jumlah?> ekor</div>
            <?php } ?>
        </div>
    </div>
</div>