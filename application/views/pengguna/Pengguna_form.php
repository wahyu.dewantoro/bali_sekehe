        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    Sistem
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Pengguna</span>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span><?php echo $button ?></span>
                                </li>
                            </ul>
                        </div>
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-green-haze">
                                    <i class="fa fa-file-text font-green-haze"></i>
                                    <span class="caption-subject bold"> <?php echo $button ?></span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                            <form action="<?php echo $action; ?>" method="post" class="form-horizontal">
                    	    <div class="form-group form-md-line-input  has-success">
                                <label class="col-md-2 control-label" for="varchar">Nama <span class="required"> * </span></label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="nama" id="nama"  value="<?php echo $nama; ?>" />
                                    <?php echo form_error('nama') ?>
                                </div>
                            </div>
                    	    <div class="form-group form-md-line-input  has-success">
                                <label class="col-md-2 control-label" for="varchar">Username <span class="required"> * </span></label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="username" id="username"  value="<?php echo $username; ?>" />
                                    <?php echo form_error('username') ?>
                                </div>
                            </div>
                    	    <div class="form-group form-md-line-input  has-success">
                                <label class="col-md-2 control-label" for="varchar">Password <span class="required"> * </span></label>
                                <div class="col-md-10">
                                    <input type="password" class="form-control" name="password" id="password"  value="<?php echo $password; ?>" />
                                    <?php echo form_error('password') ?>
                                </div>
                            </div>
                            <input type="hidden" class="form-control" name="ms_role_id" id="ms_role_id"  value="<?php echo $ms_role_id; ?>" />
                    	    <div class="form-group form-md-line-input  has-success">
                                <label class="col-md-2 control-label" for="int">Kontes <span class="required"> * </span></label>
                                <div class="col-md-10">
                                    <!-- <input type="text"  value="<?php echo $ms_kontes_id; ?>" /> -->
                                    <select class="form-control select2" name="ms_kontes_id" id="ms_kontes_id" >
                                        <option value=""></option>
                                        <?php foreach($kontes as $rk){?>
                                        <option <?php if($rk->id_inc==$ms_kontes_id){echo "selected";}?> value="<?php echo $rk->id_inc?>"><?php echo $rk->nama_kontes?></option>
                                        <?php }?>
                                    </select>
                                    <?php echo form_error('ms_kontes_id') ?>
                                </div>
                            </div>
                    	     
                    	   <div class="form-group"><div class="col-md-10 col-md-offset-2"> <input type="hidden" name="id_inc" value="<?php echo $id_inc; ?>" /> 
                    	    <button type="submit" class="btn btn-sm blue"><i class="fa fa-save"></i> Simpan</button> 
                    	    <a href="<?php echo site_url('pengguna') ?>" class="btn btn-sm red"><i class="fa fa-close"></i> Batal</a> </div></div>
                    	</form>
        </div>
</div>