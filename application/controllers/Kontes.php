<?php
 if (!defined('BASEPATH')) exit('No direct script access allowed');

class Kontes extends CI_Controller

{

    function __construct()

    {

        parent::__construct(); 
        $this->load->model('Mkontes'); 
        $this->kontes_id   =19; 
        $this->pengguna_id =$this->session->userdata('wob_pengguna'); 
        $this->role        =$this->session->userdata('wob_role'); 
        $this->handling_id =$this->session->userdata('handling_id');
    }



    public function pesertabykontes(){
        $where        ="";
        $data['hand'] =false;
        $data['rk']   =$this->Mkontes->pesertabykontes($where);
        $this->template->load('blank','kontes/pesertabykontes',$data);
    }



    function pembayaran(){
        $tagihan=$this->db->query("SELECT no_pendaftaran,a.no_kwitansi,d.nama nama_handling,d.kota kota_handling,c.nama nama_owner,c.kota kota_owner,COUNT(1) ikan,SUM(biayabykontes(ukuran)) jumlah,get_status_entri(a.pengguna_id) st_entri
                FROM tb_peserta a
                JOIN tb_ikan b ON a.id_inc=b.tb_peserta_id
                JOIN ms_peserta c ON c.id_inc=a.ms_peserta_id
                JOIN ms_handling d ON d.id_inc=a.ms_handling_id
                WHERE kd_bayar IS NULL and checkout=1
                GROUP BY no_pendaftaran,ms_handling_id,ms_peserta_id,a.no_kwitansi
                ORDER BY a.id_inc desc")->result();
        $lunas=$this->db->query("SELECT no_pendaftaran,a.no_kwitansi,d.nama nama_handling,d.kota kota_handling,c.nama nama_owner,c.kota kota_owner,COUNT(1) ikan,SUM(biayabykontes(ukuran)) jumlah,get_status_entri(a.pengguna_id) st_entri
                FROM tb_peserta a
                JOIN tb_ikan b ON a.id_inc=b.tb_peserta_id
                JOIN ms_peserta c ON c.id_inc=a.ms_peserta_id
                JOIN ms_handling d ON d.id_inc=a.ms_handling_id
                WHERE kd_bayar=1 and checkout=1
                GROUP BY no_pendaftaran,ms_handling_id,ms_peserta_id,a.no_kwitansi
                ORDER BY a.id_inc desc")->result();
        $data=array(
            'tagihan' =>$tagihan,
            'lunas'   =>$lunas
        );

        $this->template->load('blank','kontes/pembayaran',$data);   
    }


    function formbayar(){
        $nomer=$this->input->post('nomer');
        $res=$this->db->query("SELECT no_pendaftaran,d.nama nama_handling,d.kota kota_handling,c.nama nama_owner,c.kota kota_owner,SUM(biayabykontes(ukuran)) total,
                                GROUP_CONCAT(getvariety(ms_kat_id),' ',ukuran,' cm ',gender,' (',asal,') => ',biayabykontes(ukuran) SEPARATOR '#') deskripsi
                                FROM tb_peserta a
                                JOIN tb_ikan b ON a.id_inc=b.tb_peserta_id
                                JOIN ms_peserta c ON c.id_inc=a.ms_peserta_id
                                JOIN ms_handling d ON d.id_inc=a.ms_handling_id
                                WHERE no_pendaftaran=?
                                GROUP BY no_pendaftaran,ms_handling_id,ms_peserta_id",array($nomer))->row();
        if($res){
            $ikan=explode('#',$res->deskripsi);
            $html='<table width="100%" >
                    <tr valign="top">
                        <td width="20%">Handling</td>
                        <td width="1%">:</td>
                        <td>'.$res->nama_handling.' - '.$res->kota_handling.'</td>
                    </tr>
                    <tr  valign="top">
                        <td  >Owner</td>
                        <td width="1%">:</td>
                        <td>'.$res->nama_owner.' - '.$res->kota_owner.'</td>
                    </tr>
                    <tr  valign="top">
                        <td >Ikan</td>
                        <td width="1%">:</td>
                        <td> <ol>';

                        for($i=0;$i<count($ikan);$i++){
                            $html.='<li>'.$ikan[$i].'</li>';
                        }
                $html.='</ol></td>
                    </tr>
                    <tr>
                        <td><b>Total</b></td>
                        <td>:</td>
                        <td><b>Rp.'.number_format($res->total,0,'','.').'</b></td>
                    </tr>
                </table>
                <input type="hidden" id="no_pendaftaran" name="no_pendaftaran" value="'.$res->no_pendaftaran.'">
                ';

                echo $html;

        }else{
            redirect('kontes/pembayaran');
        }

    }

    function formbayarbanyak(){
        $nomer=$this->input->post('nomer');
        $vn=implode(',', $nomer);
        
        $resa=$this->db->query("SELECT no_pendaftaran,d.nama nama_handling,d.kota kota_handling,c.nama nama_owner,c.kota kota_owner,SUM(biayabykontes(ukuran)) total,
                                GROUP_CONCAT(getvariety(ms_kat_id),' ',ukuran,' cm ',gender,' (',asal,') => ',biayabykontes(ukuran) SEPARATOR '#') deskripsi
                                FROM tb_peserta a
                                JOIN tb_ikan b ON a.id_inc=b.tb_peserta_id
                                JOIN ms_peserta c ON c.id_inc=a.ms_peserta_id
                                JOIN ms_handling d ON d.id_inc=a.ms_handling_id
                                WHERE no_pendaftaran in ($vn)
                                GROUP BY no_pendaftaran,ms_handling_id,ms_peserta_id")->result();
        
        if($resa){
            $html='';
            $jumlah=0;
            foreach($resa as $res){
            $jumlah+=$res->total;
            $ikan=explode('#',$res->deskripsi);
            $html.='<table width="100%" >
                    <tr valign="top">
                        <td width="20%">Handling</td>
                        <td width="1%">:</td>
                        <td>'.$res->nama_handling.' - '.$res->kota_handling.'</td>
                    </tr>
                    <tr  valign="top">
                        <td  >Owner</td>
                        <td width="1%">:</td>
                        <td>'.$res->nama_owner.' - '.$res->kota_owner.'</td>
                    </tr>
                    <tr  valign="top">
                        <td >Ikan</td>
                        <td width="1%">:</td>
                        <td> <ol>';

                        for($i=0;$i<count($ikan);$i++){
                            $html.='<li>'.$ikan[$i].'</li>';
                        }
                $html.='</ol></td>
                    </tr>
                    <tr>
                        <td><b>Tagihan</b></td>
                        <td>:</td>
                        <td><b>Rp.'.number_format($res->total,0,'','.').'</b></td>
                    </tr>
                </table>
                <input type="hidden" id="no_pendaftaran_banyak" name="no_pendaftaran_banyak[]" value="'.$res->no_pendaftaran.'">
                <hr>
                ';
            }
                $html.="<table width='100%'>
                        <tr>
                            <td width='20%'><b>Total</b></td>
                            <td>:</td>
                            <td><b>Rp ".number_format($jumlah,0,'','.')."</b></td>
                        </tr>
                        </table>";


                echo $html;


        }else{
            redirect('kontes/pembayaran');
        }

    }

    function aksibayarkontesbanyak(){
        
        $nomer=$this->input->get('no');
        $user =$this->pengguna_id;
        $res  =$this->db->query("SELECT no_pendaftaran,d.nama nama_handling,d.kota kota_handling,c.nama nama_owner,c.kota kota_owner,SUM(biayabykontes(ukuran)) total,
                                GROUP_CONCAT(getvariety(ms_kat_id),' ',ukuran,' cm ',gender,' (',asal,') => ',biayabykontes(ukuran) SEPARATOR '#') deskripsi
                                FROM tb_peserta a
                                JOIN tb_ikan b ON a.id_inc=b.tb_peserta_id
                                JOIN ms_peserta c ON c.id_inc=a.ms_peserta_id
                                JOIN ms_handling d ON d.id_inc=a.ms_handling_id
                                WHERE no_pendaftaran in ($nomer) and (kd_bayar is null or kd_bayar=0)
                                GROUP BY no_pendaftaran,ms_handling_id,ms_peserta_id")->row();
        
        if($res){
            

            // proses bayar
            $this->db->trans_start();
            // update peserta
            $this->db->set('kd_bayar',1);
            // $this->db->set('no_kwitansi',$nk);
            $this->db->where("no_pendaftaran in ($nomer)",'',false);
            $this->db->update('tb_peserta');

            // update kode bayar ikan
            $this->db->set('bayar',1);
            $this->db->where("tb_peserta_id IN (SELECT id_inc FROM tb_peserta WHERE no_pendaftaran in ($nomer))","",false);
            $this->db->update('tb_ikan');
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE)
            {
                // echo "0";
                $this->session->set_flashdata('msg', '<div class="note note-danger"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <h4>Oppss</h4> <p>Data gagal di proses.</p> </div>');
            }else{
                // echo "1";
                // redirect('kontes/cetakkwitansibanyak?no='.urlencode($nomer));
                $this->session->set_flashdata('msg', '<div class="note note-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <h4>Sukses</h4> <p>Data lunas.</p> </div>');
            }
        }else{
            $this->session->set_flashdata('msg', '<div class="note note-warning"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <h4>Oppss</h4> <p>Data tidak ditemukan.</p> </div>');
            // echo "2";
        }
        redirect('kontes/pembayaran');
    }


    function cetakkwitansibanyak(){
        $nomerb       =urldecode($this->input->get('no'));
        $nomer        =explode(',',$nomerb);
        $datakwitansi =array();
        foreach($nomer as $nomer){
            $peserta=$this->db->query("SELECT c.nama nama_handling,c.kota kota_handling,d.nama nama_owner,d.kota kota_owner,no_pendaftaran,no_kwitansi
                                        FROM tb_peserta a
                                        JOIN ms_handling c ON c.id_inc=a.ms_handling_id
                                        JOIN ms_peserta d ON d.id_inc=a.ms_peserta_id
                                        WHERE no_pendaftaran='$nomer'")->row();
            $er=$peserta->no_kwitansi;
            $ikan=$this->db->query("SELECT nm_ikan nm_jenis,no_ikan, ukuran,gender,asal,biayabykontes(ukuran) nominal
                    FROM tb_ikan c 
                                        JOIN ms_kategoriikan b ON c.ms_kat_id=b.id_inc
                                        JOIN tb_peserta d ON d.id_inc=c.tb_peserta_id
                                        WHERE no_pendaftaran ='$nomer' AND no_kwitansi='$er'
                                       ORDER BY no_ikan ASC")->result();

            
            $gambar=$this->db->query("SELECT  no_ikan,nm_ikan , ukuran,gender,asal,if( gambar_ikan is null,'noimage.jpg',gambar_ikan) gambar_ikan
                                        FROM tb_ikan a
                                        JOIN ms_kategoriikan e ON e.id_inc=a.ms_kat_id
                                        WHERE tb_peserta_id  in (select id_inc from tb_peserta where no_pendaftaran='$nomer')
                                        ORDER BY no_ikan ASC")->result();

            $data=array(
                'peserta' =>$peserta,
                'ikan'    =>$ikan,
                'gambar'=>$gambar
            );
            $datakwitansi[]=$data;
        }

        $plastik=$this->db->query("SELECT plastik(ukuran) plastik,COUNT(1) jumlah
                                FROM tb_ikan WHERE tb_peserta_id IN (SELECT id_inc FROM tb_peserta WHERE no_pendaftaran IN ($nomerb))
                                GROUP BY plastik
                                ORDER BY plastik ASC")->result();
        
        $asd=array('res'=>$datakwitansi,
            'resa'=>$datakwitansi,
                'plastik'=>$plastik);
        $html=$this->load->view('kontes/print_kwitansi_banyak',$asd,true);
        $this->load->library('m_pdf');
        $this->m_pdf->pdf->AddPage();
        $this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output("kelengkapankontes.pdf",'I');
    }
 
    function cetakkwitansi(){
        $nomer=urldecode($this->input->get('no'));
        $peserta=$this->db->query("SELECT c.nama nama_handling,c.kota kota_handling,d.nama nama_owner,d.kota kota_owner,no_pendaftaran,no_kwitansi
                                    FROM tb_peserta a
                                    JOIN ms_handling c ON c.id_inc=a.ms_handling_id
                                    JOIN ms_peserta d ON d.id_inc=a.ms_peserta_id
                                    WHERE no_pendaftaran='$nomer'")->row();


        $ikan=$this->db->query("SELECT nm_ikan nm_jenis,no_ikan, ukuran,gender,asal,biayabykontes(ukuran) nominal
                    FROM tb_ikan c 
                                        JOIN ms_kategoriikan b ON c.ms_kat_id=b.id_inc
                                        JOIN tb_peserta d ON d.id_inc=c.tb_peserta_id
                                        WHERE no_pendaftaran ='$nomer' 
                                       ORDER BY no_ikan ASC")->result();
      
        
        $plastik=$this->db->query("SELECT plastik(ukuran) plastik,COUNT(1) jumlah
                                FROM tb_ikan WHERE tb_peserta_id IN (SELECT id_inc FROM tb_peserta WHERE no_pendaftaran IN ($nomer))
                                GROUP BY plastik
                                ORDER BY plastik ASC")->result();

        $gambar=$this->db->query("SELECT  no_ikan,nm_ikan , ukuran,gender,asal,if( gambar_ikan is null,'noimage.jpg',gambar_ikan) gambar_ikan
                                        FROM tb_ikan a
                                        JOIN ms_kategoriikan e ON e.id_inc=a.ms_kat_id
                                        WHERE tb_peserta_id  in (select id_inc from tb_peserta where no_pendaftaran='$nomer')
                                        ORDER BY no_ikan ASC")->result();

        $data=array(
            'peserta' =>$peserta,
            'ikan'    =>$ikan,
            'plastik' =>$plastik,
            'gambar'=>$gambar
        );
        $html=$this->load->view('kontes/print_kwitansi',$data,true);

        $this->load->library('m_pdf');
        $this->m_pdf->pdf->AddPage();
        $this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output("kelengkapankontes.pdf",'I');
    }

    function cetakkwitansi_bayar(){
        $nokw=urldecode($this->input->get('no'));
        $rk=$this->db->query("SELECT no_kwitansi,GROUP_CONCAT( pemilik_ikan ,' - ',kota_pemilik ,' ( ',handling,'-',kota_handling,' )' SEPARATOR ' <br> ') keterangan,SUM(jumlah) jumlah,get_count_ikan_pembayaran(no_kwitansi) ikan ,tgl_bayar
                            FROM (SELECT no_kwitansi,no_pendaftaran,tanggal_insert tgl_bayar,handling,kota_handling,pemilik_ikan,kota_pemilik,SUM(jumlah) jumlah 
                            FROM tb_pembayaran
                            GROUP BY no_kwitansi,handling,kota_handling,pemilik_ikan,kota_pemilik) asd
                            WHERE no_kwitansi=?
                            GROUP BY no_kwitansi",array($nokw))->row();
        if($rk){
            $this->load->view('kontes/printkwitansi_bayar',array('rk'=>$rk));
        }
    }
 


    function aksibayarvat(){
         $id=$this->input->post('id'); 
         $cek=$this->db->query("SELECT status_bayar FROM tb_sewabak WHERE id_inc=$id")->row();
        if($cek->status_bayar=='1'){
            $val=null;
            $html='0';
            $this->db->set('status_bayar','null',false);
            $this->db->set('tanggal_bayar','null',false);
        }else{
            $val='1';
            $html='1';
            $this->db->set('status_bayar','1');
            $this->db->set('tanggal_bayar','now()',false);
        }
        $this->db->where('id_inc',$id);
        $this->db->update('tb_sewabak');
        echo $html;
    }





    function detailpesertakontes($id){

        $rk   =$this->Mkontes->pendaftarbyid($id); 
        $role =$this->role;       
        $data['hand']=false;

        if($rk){
            $data['rp']       =$rk;
            $data['listikan'] =$this->Mkontes->listikanbypeserta($rk->id_inc);
            $this->template->load('blank','kontes/detailpeserta',$data);
        }else{
            redirect('kontes/pesertabykontes');
        }

    }



    function proseseditikan(){
         $id_inc    =$this->input->post('id_inc',true);
         $ms_kat_id =$this->input->post('ms_kat_id',true);
         $ukuran    =$this->input->post('ukuran',true);
         $asal      =$this->input->post('asal',true);
         $gender    =$this->input->post('gender',true);

    if(!empty($_FILES['gambar_ikan']['name'])){
         $name   =$_FILES['gambar_ikan']['name']; 
         $tmp    =$_FILES['gambar_ikan']['tmp_name']; 
         $exp    =explode('.',$name); 
         $gambar ='ikan/'.date('YmdHis').'.'.$exp[count($exp)-1];
    }else{
        $gambar =$this->input->post('lawas',true);
    }

        $this->db->where('id_inc',$id_inc); 
        $this->db->set('ms_kat_id',$ms_kat_id); 
        $this->db->set('ukuran',$ukuran); 
        $this->db->set('gambar_ikan',$gambar); 
        $this->db->set('asal',$asal); 
        $this->db->set('gender',$gender);
        $rk=$this->db->update('tb_ikan');
        if($rk){
            if(!empty($_FILES['gambar_ikan']['name'])){
                move_uploaded_file($tmp,$gambar);
            }
                $this->session->set_flashdata('msg', '<div class="note note-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <h4>Berhasil </h4> <p>Data telah disimpan.</p> </div>');
            } else {    
                $this->session->set_flashdata('msg', '<div class="note note-danger"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <h4>Oppss</h4> <p>Data gagal disimpan.</p> </div>');
            }
            // redirect('kontes/ikan');
            redirect($_SERVER['HTTP_REFERER']);

    }


    function proseseditikandua(){
         $id_inc    =$this->input->post('id_inc');
         $ms_kat_id =$this->input->post('ms_kat_id');
         $ukuran    =$this->input->post('ukuran');
         $asal      =$this->input->post('asal');
         $gender    =$this->input->post('gender');


    if(!empty($_FILES['gambar_ikan']['name'])){
         $name   =$_FILES['gambar_ikan']['name']; 
         $tmp    =$_FILES['gambar_ikan']['tmp_name']; 
         $exp    =explode('.',$name); 
         $gambar ='ikan/'.date('YmdHis').'.'.$exp[count($exp)-1];
    }else{
        $gambar=$this->input->post('lawas');
    }

        $this->db->where('id_inc',$id_inc); 
        $this->db->set('ms_kat_id',$ms_kat_id); 
        $this->db->set('ukuran',$ukuran); 
        $this->db->set('gambar_ikan',$gambar); 
        $this->db->set('asal',$asal); 
        $this->db->set('gender',$gender);
        $rk=$this->db->update('tb_ikan');
        if($rk){
            if(!empty($_FILES['gambar_ikan']['name'])){
                move_uploaded_file($tmp,$gambar);
            }

                $this->session->set_flashdata('msg', '<div class="note note-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <h4>Berhasil </h4> <p>Data telah disimpan.</p> </div>');

            } else {    

                $this->session->set_flashdata('msg', '<div class="note note-danger"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <h4>Oppss</h4> <p>Data gagal disimpan.</p> </div>');
            }

            header('Location: ' . $_SERVER['HTTP_REFERER']);
    }



    function ikan(){
        $data['rk']=$this->Mkontes->ikanbykontes();
        $this->template->load('blank','kontes/ikanbykontes',$data);

    }



     function editikan($id){
        $data['rk']   =$this->db->query("SELECT * FROM tb_ikan WHERE id_inc='$id'")->row();
        $data['ikan'] =$this->db->query("SELECT id_inc,nm_ikan FROM ms_kategoriikan")->result();
        $this->load->view('kontes/editikan',$data);
    }


    function kelengkapankontes($id){
        $this->load->library('M_pdf');
        $rk=$this->db->query("SELECT concat('Owner - ',e.nama) owner, concat('Handling - ',b.nama) handling,nama_kontes,no_pendaftaran,tempat_kontes,a.tanggal_insert tanggal
                                        FROM tb_peserta a
                                        JOIN  ms_handling b ON a.`ms_handling_id`=b.id_inc
                                        join ms_peserta e on e.id_inc=a.ms_peserta_id
                                        JOIN ms_kontes c ON c.`id_inc`=a.`ms_kontes_id`
                                        WHERE    no_pendaftaran='$id'")->row();
        // checkout='1' and kd_bayar is not null AND

        $rn=$this->db->query("SELECT  no_ikan,nm_ikan , ukuran,gender,asal,if( gambar_ikan is null,'noimage.jpg',gambar_ikan) gambar_ikan
                                        FROM tb_ikan a
                                        JOIN ms_kategoriikan e ON e.id_inc=a.ms_kat_id
                                        WHERE tb_peserta_id  in (select id_inc from tb_peserta where no_pendaftaran='$id')
                                        ORDER BY no_ikan ASC")->result();
        $data['rn'] =$rn;
        $data['rk'] =$rk;
        $html       =$this->load->view('kontes/kelengkapankonteskartuikan',$data,true);
        $mpdf       = new mPDF('utf-8', array(210,290),'','','1','1','1','1');
        $mpdf->WriteHTML($html);
        $mpdf->Output("output.pdf", 'I');
    }

    function kelengkapankontesbanyak(){
        $this->load->library('M_pdf');
        $nomer= $this->input->get('nomer');
        
        $data['nomer']=$nomer;
        $html       =$this->load->view('kontes/kelengkapankonteskartuikanbanyak',$data,true);

        $mpdf       = new mPDF('utf-8', array(210,290),'','','2','2','2','2');
        $mpdf->WriteHTML($html);
        $mpdf->Output("output.pdf",'I');
    }

    function cetaklabelikannew(){
        $nomer= $this->input->post('nomer');
        $ikan=$this->db->query("SELECT  no_ikan,nm_ikan , ukuran,gender,asal,IF( gambar_ikan IS NULL,'noimage.jpg',gambar_ikan) gambar_ikan
                                        FROM tb_ikan a
                                        JOIN ms_kategoriikan e ON e.id_inc=a.ms_kat_id                                        
                                        WHERE bayar IS NOT NULL AND tb_peserta_id IN (SELECT id_inc FROM tb_peserta WHERE no_pendaftaran='$nomer')
                                        ORDER BY no_ikan ASC")->result();

        // echo $this->db->last_query();
        $data=array('rn'=>$ikan);
        $this->load->view('kontes/print_label_ikan',$data);
    }


    function cetaklabelikan($id){
        $this->load->library('M_pdf');
         
        $rn=$this->db->query("SELECT  no_ikan,nm_ikan , ukuran,gender,asal,IF( gambar_ikan IS NULL,'noimage.jpg',gambar_ikan) gambar_ikan
        FROM tb_ikan a
        JOIN ms_kategoriikan e ON e.id_inc=a.ms_kat_id                                        
        WHERE bayar IS NOT NULL AND tb_peserta_id IN (SELECT id_inc FROM tb_peserta WHERE no_pendaftaran='$id')
        ORDER BY no_ikan ASC")->result();
        $data['rn'] =$rn;
        $html       =$this->load->view('kontes/kelengkapankonteslabel',$data,true);
        $mpdf       = new mPDF('utf-8', array(70,270),'','','1','1','1','1');
        $mpdf->showImageErrors = true;
        $mpdf->AddPage();
        $mpdf->WriteHTML($html);
        $mpdf->Output("labelikan".date('YmdHis').".pdf", 'I');
    
    }

    function cetaklabelikanbanyak(){
        $nomer=$this->input->get('nomer');
        $this->load->library('M_pdf');
         
        $rn=$this->db->query("SELECT  no_ikan,nm_ikan , ukuran,gender,asal,IF( gambar_ikan IS NULL,'noimage.jpg',gambar_ikan) gambar_ikan
        FROM tb_ikan a
        JOIN ms_kategoriikan e ON e.id_inc=a.ms_kat_id                                        
        WHERE bayar IS NOT NULL AND tb_peserta_id IN (SELECT id_inc FROM tb_peserta WHERE no_pendaftaran in ($nomer))
        ORDER BY no_ikan ASC")->result();
        $data['rn'] =$rn;
        $html       =$this->load->view('kontes/kelengkapankonteslabel',$data,true);
        $mpdf       = new mPDF('utf-8', array(70,270),'','','1','1','1','1');
        $mpdf->showImageErrors = true;
        $mpdf->AddPage();
        $mpdf->WriteHTML($html);
        $mpdf->Output("labelikan".date('YmdHis').".pdf", 'I');
 
    }



    function formeditpeserta($id){

        $data['rk']    =$this->Mkontes->pesertabyinc($id);
        $data['hand']  =$this->Mkontes->handlingall();
        $data['owner'] =$this->Mkontes->ownerall();

        $this->load->view('kontes/formeditpeserta',$data);

    }



    function proseseditpeserta(){

        $id=$_POST['id_inc']; 
        $this->db->where('id_inc',$id); 
        $this->db->set('ms_handling_id',$_POST['ms_handling_id']); 
        $this->db->set('ms_peserta_id',$_POST['ms_peserta_id']); 
        $this->db->update('tb_peserta');
        redirect('kontes/detailpesertakontes/'.$id);
    }



    function dokumencek(){

        $kontes         =$this->kontes_id; 
        $data['kontes'] =$this->Mkontes->getkategorikontes($kontes); 
        $data['ukuran'] =$this->Mkontes->getukuranbykontes($kontes); 
        $data['ikan']   =$this->Mkontes->getiakanbykontes($kontes);
        $this->template->load('blank','kontes/vdokumencekikan',$data);
    }


    function cetakfotoikan(){

         // ini_set("memory_limit","256M"); 
         $this->load->library('M_pdf'); 
         $kelas      =$this->input->get('kelas',true); 
         /*$ukuran_min =$this->input->get('ukuran_min',true); 
         $ukuran_max =$this->input->get('ukuran_max',true); */
         $ukuran_min ='56'; 
         $ukuran_max ='65'; 
         $breder     =$this->input->get('breder',true); 
         $gender     =$this->input->get('gender',true); 
         $kontes     =$this->kontes_id; 
         $rk         =$this->db->query("SELECT nama_kontes FROM ms_kontes WHERE id_inc='$kontes'")->row();
    

        $data['kontes']     =$this->Mkontes->getkategorikontesdua($kontes,$kelas,$ukuran_min,$ukuran_max,$breder,$gender); 
        $data['ukuran']     =$this->Mkontes->getukuranbykontes($kontes,$kelas,$ukuran_min,$ukuran_max,$breder,$gender); 
        $data['ki']         =$kontes; 
        $data['kelas']      =$kelas; 
        $data['ukuran_min'] =$ukuran_min; 
        $data['ukuran_max'] =$ukuran_max; 
        $data['breder']     =$breder; 
        $data['gender']     =$gender;

        $html=$this->load->view('kontes/vcetafotoikan',$data,true);
       

        /*$this->m_pdf->pdf->SetHTMLHeader('<table border="0" cellspacing="0" width="100%" cellpadding="3" style="font-size:10px"> <tr> <td align="center"><h4><b>Daftar Foto Ikan</b></h4></td> </tr> <tr> <td  align="center"><b>ZNA Tulungaguung</b> </td> </tr> </table><hr>');
        $this->m_pdf->pdf->AddPage(); */
        $this->m_pdf->pdf->WriteHTML($html);

        $filePath="tmp/fotoikan".date('YmdHis').'.pdf';
        $this->m_pdf->pdf->Output($filePath, 'F');
        $this->load->helper('download');
            force_download($filePath, null);   
    }



    function cetakfotoikanukuranbesar(){
    $this->load->library('M_pdf'); 
    $kontes         =19; 
    $rn=$this->db->query("SELECT GROUP_CONCAT(id_uk) id_uk FROM (SELECT  CONCAT(MIN,MAX) id_uk FROM ms_biayakontes  ORDER BY MAX DESC LIMIT 3 ) df")->row();
        $var =$rn->id_uk;

        

        $rg  =$this->Mkontes->getukuranbykontesukbesar($kontes,$var);

        if($rg){
            $rk             =$this->db->query("SELECT nama_kontes FROM ms_kontes WHERE id_inc='$kontes'")->row();
            $data['kontes'] =$this->Mkontes->getkategorikontesukbesar($kontes,$var);
            $data['ukuran'] =$rg;
            $data['ki']     =$kontes;
            $html=$this->load->view('kontes/vcetafotoikanbesar',$data,true);
            $this->m_pdf->pdf->WriteHTML($html);
            $this->m_pdf->pdf->Output("dokumen_cek_ikan.pdf", 'I');
        }else{

             $this->session->set_flashdata('msg', '<div class="note note-danger"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <h4>Oppss</h4> <p>Data ukuran besar tidak ada.</p> </div>');
            redirect('kontes/dokumencek');

        }

    }



    function cetakceklistikan(){
        ini_set('max_execution_time', 0);
        $this->load->library('M_pdf');

        $kontes         =$this->kontes_id;

        $rk             =$this->db->query("SELECT nama_kontes FROM ms_kontes WHERE id_inc='$kontes'")->row();

        $data['kontes'] =$this->Mkontes->getkategorikontes($kontes);

        $data['ukuran'] =$this->Mkontes->getukuranbykontes($kontes);
        $data['ki']=$kontes;
        $html=$this->load->view('kontes/vcetakchecklistikan',$data,true);
       /*  $this->m_pdf->pdf->SetHTMLHeader('<table border="0" cellspacing="0" width="100%" cellpadding="3" style="font-size:10px"> 
                                                <tr>
                                                <td align="center"><h4><b>Checklist Ikan</b></h4></td>
                                                </tr>
                                                <tr>
                                                    <td  align="center"><b>2<sup>nd</sup> ZNA Tulungagung Koi Show 2019</b> </td>
                                                </tr>   
                                            </table><hr>');
            $this->m_pdf->pdf->AddPage();*/
            $this->m_pdf->pdf->WriteHTML($html);
            $filePath ="tmp/ceklistikan".date('YmdHis').".pdf";

            $this->m_pdf->pdf->Output($filePath, 'F');
            $this->load->helper('download');
            force_download($filePath, null);   

    }



    function datavat(){
         // $data =$this->Mkontes->getvatBykontes();
        $rk=$this->db->query("select * from tb_sewabak")->result();
         $data=array('rk'=>$rk);
         $this->template->load("blank","kontes/vdatavat",$data);
    }

 

    function cetakkwitansivat(){
        $kontes =$this->kontes_id;
        $id=$this->input->get('no',true);

        $cek=$this->db->query("SELECT * FROM tb_sewabak WHERE id_inc=$id")->row();
        if($cek->status_bayar<>'1'){
            $this->db->set('status_bayar','1');
            $this->db->set('tanggal_bayar','now()',false);
            $this->db->where('id_inc',$id);
            $this->db->update('tb_sewabak');
        }
    
        $data['rk']=$this->db->query("SELECT * FROM tb_sewabak WHERE id_inc=$id")->row();
        // print_r(expression)
        $html=$this->load->view('kontes/vcetakkwitansisewavat',$data);
    
    }

    function formsewavat(){
         $data['id_inc']  =set_value('');
         $data['penyewa'] =set_value('');
         $data['jumlah']  =set_value('');
         $data['jenis']   =set_value('');
         $data['harga']   =set_value('');
         $data['action']  =base_url().'kontes/prosessewavat';
         $this->load->view('kontes/vformsewavat',$data);
    }



    function prosessewavat(){
        
        $jenis=$this->input->post('jenis',true);
        $penyewa=$this->input->post('penyewa',true);
        $jumlah=$this->input->post('jumlah',true);

        if($jenis=='sewa'){
            $harga=500000;

        }else if($jenis=='bawa pulang'){
            $harga=500000;
        }else{
            $harga=0;
        }

        $total=$harga*$jumlah;

        $asw=array(
            'jenis'=>$jenis,
            'penyewa'=>$penyewa,
            'jumlah'=>$jumlah,
            'harga'=>$harga,
            'total'=>$total,
        );  
        $this->db->insert('tb_sewabak',$asw);
        redirect('kontes/datavat');

    }

  /*  function proseseditsewavat(){

         $ms_bak_id     =$_POST['ms_bak_id'];         
         $label_bak     =$_POST['label_bak'];    
         $ms_peserta_id =$_POST['ms_peserta_id'];

        if(!empty($_POST['ikan'])){
        $ikan          =$_POST['ikan'];
        $rikan='';
        for($i=0;$i<count($ikan);$i++){

            $rikan.=$ikan[$i].',';

        }

        $vikan=substr($rikan,0,-1);



        }else{

            $vikan=null;

        }



        $data=array(

            'ms_bak_id'     =>$ms_bak_id,

            'label_bak'     =>$label_bak,

            'ms_peserta_id' =>$ms_peserta_id,

            'ikan'          =>$vikan

            );

        $wh['id_inc']=$_POST['id_inc'];

        $this->db->update('tb_sewabak',$data,$wh);

        redirect('kontes/datavat');

    }

*/

/*

    function formeditsewavat($id){

        $rk=$this->db->query("SELECT * FROM tb_sewabak WHERE id_inc='$id'")->row();

        $kontes                =$this->kontes_id;

         $data['owner']         =$this->Mkontes->ownerall();

         $data['li']=$this->Mkontes->ikanbykontes($kontes);

         $data['vat']           =$this->db->query("SELECT id_inc,nama_bak FROM ms_bakkontes WHERE ms_kontes_id=$kontes")->result();

         $data['ms_bak_id']     =set_value('ms_bak_id',$rk->ms_bak_id);

         $data['label_bak']     =set_value('label_bak',$rk->label_bak);

         $data['ms_peserta_id'] =set_value('ms_peserta_id',$rk->ms_peserta_id);

         $exp=explode(',',trim($rk->ikan));

         $data['ikan']          =set_value('ikan',$exp);

         $data['id_inc']=set_value('id_inc',$rk->id_inc);

         $data['action']=base_url().'kontes/proseseditsewavat';

          $this->load->view('kontes/vformsewavat',$data);

    }*/



    function viewikandetail($id){

        $data['rk']=$this->Mkontes->dataIkanById($id);

        $this->load->view('kontes/viewikandetail',$data);

    }

    function viewikandetailab($id){
        $data['rk']=$this->Mkontes->dataIkanById($id);

        $this->load->view('kontes/viewikandetailab',$data);        
    }



    function hapussewavat($id){

        $rk=$this->db->query("DELETE FROM tb_sewabak WHERE id_inc='$id'");

        redirect('kontes/datavat');

    }



    function cetakFotoIkanForm(){
        $data=array('kelas'=>array('A','B','C','D','E'), 'kelass'=>array('A','B','C','D','E') ); 
        $this->template->load('blank','kontes/    formcetakikan',$data);
    }



    function cetaklistikanbyfilter(){

        /*echo "<pre>";

        print_r($this->input->get());*/

        $kelas      =urldecode($this->input->get('kelas',true));        
        $ukuran_min =urldecode($this->input->get('ukuran_min',true));
        $ukuran_max =urldecode($this->input->get('ukuran_max',true));
        $breder     =urldecode($this->input->get('breder',true));
        $gender     =urldecode($this->input->get('gender'));

        $and='';
        $and.=$kelas<>''?" and kat_ikan='$kelas'":"";
        if($ukuran_min<>'' && $ukuran_max<>''){
            $and.=" and ukuran >= $ukuran_min and ukuran <= $ukuran_max";
        }

        $and.=$breder<>''?" and asal = '$breder'":"";
        $and .=$gender<>''?" and gender ='$gender'":"";
        $sql="SELECT no_ikan,kat_ikan,nm_ikan,ukuran,gender,asal,nama,kota,telp FROM tb_ikan a JOIN ms_kategoriikan b ON a.`ms_kat_id`=b.`id_inc` JOIN tb_peserta c ON c.`id_inc`=a.`tb_peserta_id` JOIN ms_handling d ON d.`id_inc`=ms_handling_id WHERE no_ikan IS NOT NULL ".$and." ORDER BY kat_ikan ASC,nm_ikan ASC,ukuran ASC";

        $this->load->library('M_pdf');
        $ikan=$this->db->query($sql)->result();
        $data=array('ikan'=>$ikan);
        $html=$this->load->view('kontes/cetakikanbyfilter',$data,true);
        $this->m_pdf->pdf->SetHTMLHeader('<table border="0" cellspacing="0" width="100%" cellpadding="3" style="font-size:10px"> <tr> <td align="center"><h4><b>Data Ikan</b></h4></td> </tr> <tr> <td  align="center"><b>1<sup>ST</sup> Madiun Young Koi Show</b> </td> </tr> </table><hr>');
        $this->m_pdf->pdf->AddPage();
        $this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output("data ikan.pdf", 'I');

    }


    function detailBAyar($no_pendaftaran){
        // $pp=$this->Mdaftar->pendaftarbyid($id);
        $this->load->model('Mdaftar');
        $pp=$this->db->query("SELECT c.id_inc id_handling,d.id_inc id_peserta,a.id_inc,nama_kontes,c.nama handling,c.kota kota_handling,d.nama pemilik_ikan,d.kota kota_pemilik,DATE_FORMAT(a.tanggal_insert,'%d %M %Y, %H:%i:%s') tanggal_daftar,getNamaPengguna(a.pengguna_id) nama_daftar,no_pendaftaran kode,checkout,ms_handling_id
                                    FROM tb_peserta a
                                    JOIN ms_kontes b ON a.ms_kontes_id=b.id_inc
                                    JOIN ms_handling c ON c.id_inc=a.ms_handling_id
                                    join ms_peserta d on d.id_inc=a.ms_peserta_id
                                    WHERE a.no_pendaftaran=?",array($no_pendaftaran))->row();
        if($pp){
            $data=array(
                'rp'       =>$pp,
                'ikan'     =>$this->db->query("SELECT id_inc,nm_ikan FROM ms_kategoriikan order by sort asc")->result(),
                'listikan' =>$this->Mdaftar->listikanbypeserta($pp->id_inc),
                );
            $this->template->load('blank','kontes/detail_pembayaran',$data);
        }else{
            redirect('daftar');
        }
    }

        function prosestambahikan(){
            $this->db->trans_start();

                $tb_peserta_id =$this->input->post('tb_peserta_id');
                $name          =$_FILES['gambar_ikan']['name'];
                $tmp           =$_FILES['gambar_ikan']['tmp_name'];            
                $exp           =explode('.',$name);
                $gambar        ='ikan/'.date('YmdHis').'.'.$exp[count($exp)-1];
                $this->db->set('tb_peserta_id',$tb_peserta_id);
                $this->db->set('ms_kat_id',$this->input->post('ms_kat_id'));
                $this->db->set('ukuran',$this->input->post('ukuran'));
                $this->db->set('gambar_ikan',$gambar);
                $this->db->set('gender',$this->input->post('gender'));
                $this->db->set('asal',$this->input->post('asal'));

                $rk=$this->db->insert('tb_ikan');
                if($rk){
                     move_uploaded_file($tmp,$gambar);
                }
            $this->db->trans_complete();


        if($this->db->trans_status() === TRUE){
                $this->session->set_flashdata('msg', '<div class="note note-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <h4>Berhasil </h4> <p>Data telah disimpan.</p> </div>');
            } else {    

                $this->session->set_flashdata('msg', '<div class="note note-danger"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <h4>Oppss</h4> <p>Data gagal disimpan.</p> </div>');
            }
             
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }

    function viewikan($id){
        $rk=$this->db->query("select gambar_ikan from tb_ikan where id_inc='$id'")->row();
        echo '<div class="modal-content"><div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button> </div> <div class="modal-body"> <p class="text-center">  <img style="max-height:150px;" src="'.base_url().$rk->gambar_ikan.'"></p> </div> </div>';
    }

    function formeditikan($id){
        $data['rk']   =$this->db->query("SELECT * FROM tb_ikan WHERE id_inc='$id'")->row();
        $data['ikan'] =$this->db->query("SELECT id_inc,nm_ikan FROM ms_kategoriikan")->result();
        $this->load->view('kontes/editikandua',$data);
    }


    function rotasi_gambar(){
        $gambar= $this->input->post('gbr');
        $config['source_image'] = $gambar;
        $config['rotation_angle'] = '90';// 
        
        $this->load->library('image_lib',$config);
        
        //Rotate the image
        $this->image_lib->rotate();


    }
}

 