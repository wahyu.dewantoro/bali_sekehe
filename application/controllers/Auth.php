<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Auth extends CI_Controller {

     function __construct(){

        parent::__construct();

                $this->load->library('form_validation');
                $this->load->model('Mkontes');

                    }

    function ceklogin(){
        $ses=$this->session->userdata('kontes');
        if($ses==1){
            $asd=$this->session->userdata('wob_role');
            if($asd==4){
                redirect('landing');
            }else{
                redirect('welcome');
            }
        }
    }

	function index(){
        $this->ceklogin();

        $data['action']=base_url().'auth/proses.html';
        $data['title']='';
        $this->template->load('depan/halaman_depan','vlogin',$data);   
	}



    function proses(){
        $this->ceklogin();

        if(isset($_POST['username']) && isset($_POST['password'])){
            $username =$_POST['username'];
            $password =sha1($_POST['password']);
        // left join ms_kontes c on c.id_inc=a.`ms_kontes_id`
            $sql="select a.id_inc,nama,ms_role_id, nama_role,ms_kontes_id,ms_handling_id,null kontes
                    from ms_pengguna a
                    join ms_role b on b.id_inc=a.ms_role_id
                    where username=? and password=? ";

            $row=$this->db->query($sql,array($username,$password))->row();



            if($row){
                $session=array(
                        'kontes'       =>1,
                        'wob_nama'     =>$row->nama,
                        'wob_role'     =>$row->ms_role_id,
                        'wob_pengguna' =>$row->id_inc,
                        'kontes_id'    =>19,
                        'impersonate'  =>0,
                        'handling_id'  =>$row->ms_handling_id,
                        'labelkontes'  =>$row->kontes
                    );

                $this->session->set_userdata($session);
                
                if($row->ms_role_id==4){
                    // handling
                    redirect('Landing');
                }else{
                    redirect('welcome');    
                }
                

            }else{

                $this->session->set_flashdata('notif',' <div class="alert alert-danger">Username/password anda salah</div>');
                redirect('auth');
            }

        }else{
            $this->session->set_flashdata('notif',' <div class="alert alert-danger">Silahkan login terlebih dahulu</div>');
            redirect('auth');

        }

    }


    function logout(){
        $this->session->sess_destroy();
        redirect('home');
    }


    /*    function test(){
            $str = '928389083,83';
            preg_match_all('!\d+!', $str, $matches);
            print_r($matches);
        }
        */

}



