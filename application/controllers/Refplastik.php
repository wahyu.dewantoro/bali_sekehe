<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Refplastik extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Mplastik');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $refplastik = $this->Mplastik->get_all();

        $data = array(
            'refplastik_data' => $refplastik
        );

        $this->template->load('blank','refplastik/Refplastik_list', $data);
    }

    

    public function create() 
    {
        $data = array(
            'button'        => 'Form Tambah',
            'action'        => site_url('refplastik/create_action'),
            'id_inc'        => set_value('id_inc'),
            'jenis_plastik' => set_value('jenis_plastik'),
            'ukuran_min'    => set_value('ukuran_min'),
            'ukuran_max'    => set_value('ukuran_max'),
            );
        $this->template->load('blank','refplastik/Refplastik_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'jenis_plastik' => $this->input->post('jenis_plastik',TRUE),
		'ukuran_min' => $this->input->post('ukuran_min',TRUE),
		'ukuran_max' => $this->input->post('ukuran_max',TRUE),
	    );

           $this->Mplastik->insert($data);
            
            redirect(site_url('refplastik'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Mplastik->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Form Edit',
                'action' => site_url('refplastik/update_action'),
		'id_inc' => set_value('id_inc', $row->id_inc),
		'jenis_plastik' => set_value('jenis_plastik', $row->jenis_plastik),
		'ukuran_min' => set_value('ukuran_min', $row->ukuran_min),
		'ukuran_max' => set_value('ukuran_max', $row->ukuran_max),
	    );
            $this->template->load('blank','refplastik/Refplastik_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('refplastik'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_inc', TRUE));
        } else {
            $data = array(
		'jenis_plastik' => $this->input->post('jenis_plastik',TRUE),
		'ukuran_min' => $this->input->post('ukuran_min',TRUE),
		'ukuran_max' => $this->input->post('ukuran_max',TRUE),
	    );

            $this->Mplastik->update($this->input->post('id_inc', TRUE), $data);
            
            redirect(site_url('refplastik'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Mplastik->get_by_id($id);

        if ($row) {
            $this->Mplastik->delete($id);
            
            
            redirect(site_url('refplastik'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('refplastik'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('jenis_plastik', 'ukuran plastik', 'trim|required');
	$this->form_validation->set_rules('ukuran_min', '  min', 'trim|required');
	$this->form_validation->set_rules('ukuran_max', '  max', 'trim|required');

	$this->form_validation->set_rules('id_inc', 'id_inc', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Refplastik.php */
/* Location: ./application/controllers/Refplastik.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-12-02 14:30:42 */
/* http://harviacode.com */