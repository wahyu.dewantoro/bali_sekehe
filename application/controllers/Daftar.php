<?php
 if (!defined('BASEPATH')) exit('No direct script access allowed');

class Daftar extends CI_Controller {
    function __construct(){
        parent::__construct(); 
        $this->load->model('Mdaftar'); 
        $this->load->model('Mkontes'); 
        $this->load->library('form_validation'); 
        $this->kontes_id   =$this->session->userdata('kontes_id'); 
        $this->pengguna_id =$this->session->userdata('wob_pengguna'); 
        $this->role        =$this->session->userdata('wob_role'); 
        $this->handling_id =$this->session->userdata('handling_id');
    }



    public function index(){
       $role =$this->role;
       $user =$this->pengguna_id;
        
        $cc=$this->db->query("SELECT status_kontes FROM ms_kontes ")->row();
        $where="";
        if($role!='1'){
            $where.="AND a.pengguna_id='$user'";
        }       
        $st=$cc->status_kontes;
        $daftar = $this->Mdaftar->get_all($where);
        $data   = array(
            'daftar_data' => $daftar,
            'st'          =>$st
        );
        $this->template->load('blank','daftar/Daftar_list', $data);
    }

    public function create(){
        $rk =$this->Mkontes->kontesbyid($this->kontes_id);
        $rb =$this->Mkontes->bakbykontes($this->kontes_id);

        $data = array(
            'kontes'         =>$rk->nama_kontes,
            'handling'       =>$this->Mkontes->handlingall(),
            'button'         => 'Form Tambah',
            'action'         => site_url('daftar/create_action'),
            'id_inc'         => set_value('id_inc'),
            'ms_kontes_id'   => set_value('ms_kontes_id',$this->kontes_id),
            'ms_handling_id' => set_value('ms_handling_id'),
            'pemilik_ikan'   => set_value('pemilik_ikan'),
            'kota_pemilik'   => set_value('kota_pemilik'),
            'bak'            =>$rb
	   );
        $this->template->load('blank','daftar/Daftar_form', $data);
    } 

    public function create_action(){
            $data = array(
                  'ms_kontes_id'   => $this->input->post('ms_kontes_id',TRUE),
                  'ms_handling_id' => $this->input->post('ms_handling_id',TRUE),
                  'pemilik_ikan'   => $this->input->post('pemilik_ikan',TRUE),
                  'kota_pemilik'   => $this->input->post('kota_pemilik',TRUE),
                  'pengguna_id'    => $this->pengguna_id,
        	    );

                $this->db->trans_start();

                    // insert tb peserta
                    $this->db->insert('tb_peserta',$data);
                    $cc=$this->db->query("SELECT MAX(id_inc) peserta_id  FROM tb_peserta WHERE pengguna_id='".$this->pengguna_id."'")->row();
                    $peserta_id=$cc->peserta_id;
                    // insert cart bak
                    if(count($_POST['jumlahbak'])>0){
                        for($i=0;$i<count($_POST['jumlahbak']);$i++){
                            $jumlah    =$_POST['jumlahbak'][$i];
                            $ms_bak_id =$_POST['ms_bak_id'][$i];
                            if(!empty($jumlah)){
                                $this->db->set('ms_bak_id',$ms_bak_id);
                                $this->db->set('jumlah_bak',$jumlah);
                                $this->db->set('tb_peserta_id',$peserta_id);
                                $this->db->insert('tb_sewabak');
                            }
                        }
                    }

                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
                {
                        $this->db->trans_rollback();
                         $this->session->set_flashdata('msg', '<div class="note note-danger"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <h4>Oppss</h4> <p>Data gagal disimpan.</p>
                        </div>');    
                         redirect('Daftar/create');
                }
                else
                {
                        $this->db->trans_commit();
                         $this->session->set_flashdata('msg', '<div class="note note-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <h4>Berhasil </h4> <p>Data telah disimpan.</p> </div>');
                        redirect(site_url('daftar/listcartbydaftar/'.$peserta_id));

                }

    }

    function listcartbydaftar($id){
        $pp=$this->Mdaftar->pendaftarbyid($id);
        if($pp){
            $data=array(
                'rp'       =>$pp,
                'ikan'     =>$this->db->query("SELECT id_inc,nm_ikan FROM ms_kategoriikan order by sort asc")->result(),
                'listikan' =>$this->Mdaftar->listikanbypeserta($id),
                );
            $this->template->load('blank','daftar/listcartbydaftar',$data);
        }else{
            redirect('daftar');
        }
    }

    public function update($id){
        $row = $this->Mdaftar->get_by_id($id);
        if ($row) {
            $data = array(
                'button'         => 'Form Edit',
                'action'         => site_url('daftar/update_action'),                
                'id_inc'         => set_value('id_inc', $row->id_inc),
                'ms_kontes_id'   => set_value('ms_kontes_id', $row->ms_kontes_id),
                'ms_handling_id' => set_value('ms_handling_id', $row->ms_handling_id),
                'pemilik_ikan'   => set_value('pemilik_ikan', $row->pemilik_ikan),
                'kota_pemilik'   => set_value('kota_pemilik', $row->kota_pemilik),
                'tanggal_insert' => set_value('tanggal_insert', $row->tanggal_insert),
                'pengguna_id'    => set_value('pengguna_id', $row->pengguna_id),
	    );
            $this->template->load('blank','daftar/Daftar_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('daftar'));
        }
    }

    

    public function update_action() {
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_inc', TRUE));
        } else {
            $data = array(
                     'ms_kontes_id'   => $this->input->post('ms_kontes_id',TRUE),
                     'ms_handling_id' => $this->input->post('ms_handling_id',TRUE),
                     'pemilik_ikan'   => $this->input->post('pemilik_ikan',TRUE),
                     'kota_pemilik'   => $this->input->post('kota_pemilik',TRUE),
                     'tanggal_insert' => $this->input->post('tanggal_insert',TRUE),
                     'pengguna_id'    => $this->input->post('pengguna_id',TRUE),
            	    );

            $this->Mdaftar->update($this->input->post('id_inc', TRUE), $data);
            redirect(site_url('daftar'));
        }
    }

    public function delete($id) {
        $row = $this->Mdaftar->get_by_id($id);
        if ($row) {
            $this->Mdaftar->delete($id);
            redirect(site_url('daftar'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found'); 
            redirect(site_url('daftar'));
        }

    }

    function prosestambahikan(){
         $this->db->trans_start();
                $tb_peserta_id =$_POST['tb_peserta_id'];
                $name          =$_FILES['gambar_ikan']['name'];
                $tmp           =$_FILES['gambar_ikan']['tmp_name'];            
                $exp           =explode('.',$name);
                $gambar        ='ikan/'.date('YmdHis').'.'.$exp[count($exp)-1];
                $this->db->set('tb_peserta_id',$tb_peserta_id);
                $this->db->set('ms_kat_id',$_POST['ms_kat_id']);
                $this->db->set('ukuran',$_POST['ukuran']);
                $this->db->set('gambar_ikan',$gambar);
                $this->db->set('gender',$_POST['gender']);
                $this->db->set('asal',$_POST['asal']);

                $rk=$this->db->insert('tb_ikan');
                if($rk){
                     move_uploaded_file($tmp,$gambar);
                }
            $this->db->trans_complete();


        if($this->db->trans_status() === TRUE){
                $this->session->set_flashdata('msg', '<div class="note note-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <h4>Berhasil </h4> <p>Data telah disimpan.</p> </div>');
            } else {    

                $this->session->set_flashdata('msg', '<div class="note note-danger"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <h4>Oppss</h4> <p>Data gagal disimpan.</p> </div>');
            }
        redirect('daftar/listcartbydaftar/'.$tb_peserta_id);
    }



    function hapusikan($id){
        $rk=$this->db->query("SELECT tb_peserta_id,gambar_ikan FROM tb_ikan WHERE id_inc='$id'")->row();
        if($rk){
            $rn=$this->db->query("DELETE from tb_ikan where id_inc='$id'");
            if($rn){
                @unlink($rk->gambar_ikan);
                 $this->session->set_flashdata('msg', '<div class="note note-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <h4>Berhasil </h4> <p>Data telah dihapus.</p> </div>');
            } else {    
                $this->session->set_flashdata('msg', '<div class="note note-danger"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <h4>Oppss</h4> <p>Data gagal dihapus.</p> </div>');
            }
            redirect('daftar/listcartbydaftar/'.$rk->tb_peserta_id);            
        }

    }



    function formeditikan($id){
        $data['rk']   =$this->db->query("SELECT * FROM tb_ikan WHERE id_inc='$id'")->row();
        $data['ikan'] =$this->db->query("SELECT id_inc,nm_ikan FROM ms_kategoriikan")->result();
        $this->load->view('daftar/editikan',$data);
    }

    function proseseditikan(){
       
       $id_inc    =$this->input->post('id_inc',true);
       $ms_kat_id =$this->input->post('ms_kat_id',true);
       $ukuran    =$this->input->post('ukuran',true);
       $asal      =$this->input->post('asal',true);
       $gender    =$this->input->post('gender',true);

    if(!empty($_FILES['gambar_ikan']['name'])){
        $name =$_FILES['gambar_ikan']['name'];
        $tmp  =$_FILES['gambar_ikan']['tmp_name'];
        $exp    =explode('.',$name);
        $gambar ='ikan/'.date('YmdHis').'.'.$exp[count($exp)-1];
    }else{
        $gambar=$_POST['lawas'];
    }

        $this->db->where('id_inc',$id_inc); 
        $this->db->set('ms_kat_id',$ms_kat_id); 
        $this->db->set('ukuran',$ukuran); 
        $this->db->set('gambar_ikan',$gambar); 
        $this->db->set('asal',$asal); 
        $this->db->set('gender',$gender);
        $rk=$this->db->update('tb_ikan');
        if($rk){

            if(!empty($_FILES['gambar_ikan']['name'])){
                move_uploaded_file($tmp,$gambar);
            }
                $this->session->set_flashdata('msg', '<div class="note note-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <h4>Berhasil </h4> <p>Data telah disimpan.</p> </div>');
            } else {    
                $this->session->set_flashdata('msg', '<div class="note note-danger"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <h4>Oppss</h4> <p>Data gagal disimpan.</p> </div>');
            }
            redirect('daftar/listcartbydaftar/'.$_POST['tb_peserta_id']);
    }



    function viewikan($id){
        $rk=$this->db->query("select gambar_ikan from tb_ikan where id_inc='$id'")->row();
        echo '<div class="modal-content"><div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button> </div> <div class="modal-body"> <p class="text-center">  <img style="max-width:300px;" src="'.base_url().$rk->gambar_ikan.'"></p> </div> </div>';
    }



    function chceckout($id){

            // $kontes=$this->kontes_id;
        $this->db->trans_start();
            
            // update peserta
            $this->db->set('no_kwitansi','ambilnokwitansi()',false);
            $this->db->set('checkout',1);
            $this->db->where('id_inc',$id);
            $this->db->update('tb_peserta');
            
            $this->db->set('no_ikan','ambilnoikan()',false);
            $this->db->where('tb_peserta_id',$id);
            $this->db->update('tb_ikan');

        $this->db->trans_complete();

		if ($this->db->trans_status() === FALSE){
                $this->session->set_flashdata('msg', '<div class="note note-danger"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <h4>Oppss</h4> <p>Data gagal dicheckout.</p> </div>');
            } else {    
            	$this->session->set_flashdata('msg', '<div class="note note-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <h4>Berhasil </h4> <p>Data telah dicheckout.</p> </div>'); }
        redirect('daftar');
    }

    function daftarfromkontes($id){
        // cek
        $rk=$this->db->query("SELECT a.*,CONCAT(b.nama,' - ',b.kota) peserta, CONCAT(c.nama,' - ',c.kota) handling
                                FROM ms_handling_owner a
                                JOIN ms_peserta b ON a.ms_peserta_id=b.id_inc
                                JOIN ms_handling c ON c.id_inc=a.ms_handling_id
                                WHERE a.id_inc='$id'")->row();
        if($rk){
            $data['rk']=$rk;
            $data['ikan']=$this->db->query("SELECT id_inc,nm_ikan FROM ms_kategoriikan ORDER BY sort ASC")->result();
            $this->template->load('blank','daftar/daftarfromkontes',$data);
        }

    }


    function prosessimpanikan(){
        // $_FILES['gambar_ikan']
        if( count($_FILES['gambar_ikan']['name'])>0){
            
            $this->db->trans_start();
        // cek
            $ee=$this->db->query("SELECT MAX(id_inc) tb_peserta_id FROM tb_peserta WHERE  pengguna_id='".$this->pengguna_id."' and  ms_handling_id='".$this->input->post('ms_handling_id',TRUE)."' AND ms_peserta_id='".$this->input->post('ms_peserta_id',TRUE)."' AND ms_kontes_id ='".$this->kontes_id."' AND checkout is null ")->row();

            if(!empty($ee->tb_peserta_id)){
                $tb_peserta_id =$ee->tb_peserta_id;             
            }else{

             $data = array(
                'ms_kontes_id'   => $this->kontes_id,
                'ms_handling_id' => $this->input->post('ms_handling_id',TRUE),
                'ms_peserta_id'  => $this->input->post('ms_peserta_id',TRUE),
                'pengguna_id'    => $this->pengguna_id,
                );

                 $this->db->insert('tb_peserta',$data);
                 $rn            =$this->db->query("SELECT MAX(id_inc) tb_peserta_id FROM tb_peserta WHERE pengguna_id='". $this->pengguna_id."'")->row();
                 $tb_peserta_id =$rn->tb_peserta_id; 
            }


           for($i=0; $i<count($_FILES['gambar_ikan']['name']);$i++ ){

                  $_FILES['gbr_ikan']['name']     = $_FILES['gambar_ikan']['name'][$i];
                  $_FILES['gbr_ikan']['type']     = $_FILES['gambar_ikan']['type'][$i];
                  $_FILES['gbr_ikan']['tmp_name'] = $_FILES['gambar_ikan']['tmp_name'][$i];
                  $_FILES['gbr_ikan']['error']     = $_FILES['gambar_ikan']['error'][$i];
                  $_FILES['gbr_ikan']['size']     = $_FILES['gambar_ikan']['size'][$i];

                // proses upload gambar 
                $config['upload_path']   = 'ikan/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; 
                $config['encrypt_name']  =TRUE;
                $this->load->library('upload', $config);
                if ($this->upload->do_upload('gbr_ikan'))
                {
                    $gbr                      = $this->upload->data();
                    $config['image_library']  ='gd2';
                    $config['source_image']   =$config['upload_path'].$gbr['file_name'];
                    $config['create_thumb']   = FALSE;
                    $config['maintain_ratio'] = TRUE;
                    // $config['quality']        = '70%';
                    /*$config['width']          = 400;
                    $config['height']         = 600;*/
                    $config['new_image']      = 'ikan/'.$gbr['file_name'];
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();
     
                    $gambar=$config['new_image'];
                    
                    $this->db->set('tb_peserta_id',$tb_peserta_id);
                    $this->db->set('ms_kat_id',$this->input->post('ms_kat_id')[$i]);
                    $this->db->set('ukuran',$this->input->post('ukuran')[$i]);
                    $this->db->set('gambar_ikan',$gambar);
                    $this->db->set('gender',$this->input->post('gender')[$i]);
                    $this->db->set('asal',$this->input->post('asal')[$i]);
                    $rk=$this->db->insert('tb_ikan');

                 
                }
            
           }

            $this->db->trans_complete();
                if ($this->db->trans_status() === true){
                  $this->session->set_flashdata('msg', '<div class="note note-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <h4>Berhasil </h4> <p>Data telah disimpan.</p> </div>');
                }else{
                    $this->session->set_flashdata('msg', '<div class="note note-danger"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <h4>Oppss</h4> <p>Data gagal disimpan.</p> </div>');
                }

            } else {    

                $this->session->set_flashdata('msg', '<div class="note note-danger"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <h4>Oppss</h4> <p>Anda belum mengisi data.</p> </div>');
            }
             redirect(site_url('daftar/listcartbydaftar/'.$tb_peserta_id));

    }

}

 