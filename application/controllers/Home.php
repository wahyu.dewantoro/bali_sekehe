<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Home extends CI_Controller {

     function __construct(){
        parent::__construct();
        $this->kontes_id=19;  
        $this->load->model('Mkontes');

    }

     function ceklogin(){
        $ses=$this->session->userdata('kontes');
        if($ses==1){
            $asd=$this->session->userdata('wob_role');
            if($asd==4){
                redirect('landing');
            }else{
                redirect('welcome');
            }
        }
    }

	function index(){
    $this->ceklogin();
        $rk=$this->Mkontes->getKontesId($this->kontes_id);
        $this->template->load('depan/halaman_depan','depan/beranda',array('data'=>$rk));

    }

    function signup(){
      $this->ceklogin();
        $this->template->load('depan/halaman_depan','depan/form_signup');        
    }

    function cekemail(){
        $email = $this->input->post('email',true);
        // SELECT COUNT(1) FROM ms_pengguna WHERE username=''
        $this->db->where('username',$email);
        $this->db->from('ms_pengguna');
        echo $this->db->count_all_results(); 
    }

    function prosessignup(){
      $this->ceklogin();
         $nama     =$this->input->post('nama',true);
         $kota     =$this->input->post('kota',true);
         $telp     =$this->input->post('telp',true);
         $email    =$this->input->post('email',true);
         $password =$this->input->post('password',true);

         // cek
         $this->db->where('username',$email);
        $this->db->from('ms_pengguna');
        $rd=$this->db->count_all_results(); 
        if($rd>0){
            echo '<script type="text/javascript">'; 
            echo 'alert("Registrasi gagal,email '.$email.' Sudah terdaftar ");'; 
            echo 'window.location.href = "'.base_url('home/signup').'";';
            echo '</script>';
            die();
        }

         $this->db->trans_start();
         // pengguna
         $this->db->set('nama', $nama);
         $this->db->set('username',$email); 
         $this->db->set('password',sha1($password) );
         $this->db->set('ms_role_id',4);
         $this->db->insert('ms_pengguna');

         // get id pengguna

         $this->db->where('nama', $nama);
         $this->db->where('username',$email); 
         $this->db->where('password',sha1($password) );
         $this->db->select('MAX(id_inc) pengguna_id',false);
         $pengguna_id=$this->db->get('ms_pengguna')->row()->pengguna_id;

         // handling
         $this->db->set('nama',$nama);
         $this->db->set('kota',$kota);
         $this->db->set('telp',$telp);
         $this->db->set('tgl_insert','now()',false);
         $this->db->set('pengguna_id',$pengguna_id);
         $this->db->insert('ms_handling');

         // get id handling
         $this->db->where('nama',$nama);
         $this->db->where('kota',$kota);
         $this->db->where('telp',$telp);
         $this->db->select('MAX(id_inc) handling_id',false);
         $ms_handling_id=$this->db->get('ms_handling')->row()->handling_id;

         
         // update ms_handling_id
         $this->db->where('nama', $nama);
         $this->db->where('username',$email); 
         $this->db->where('password',sha1($password) );
         $this->db->set('ms_handling_id',$ms_handling_id);
         $this->db->update('ms_pengguna');
        
         $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            echo '<script type="text/javascript">'; 
            echo 'alert("Registrasi gagal!");'; 
            echo 'window.location.href = "'.base_url('home/signup').'";';
            echo '</script>';
        }else{
            echo '<script type="text/javascript">'; 
            echo 'alert("Registrasi Sukses!");'; 
            echo 'window.location.href = "'.base_url('auth').'";';
            echo '</script>';
        }

    }
 


}



