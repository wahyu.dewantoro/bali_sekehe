<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Laporan extends CI_Controller {

     function __construct(){

        parent::__construct();

    	$this->kontes_id =$this->session->userdata('kontes_id'); 
    	$this->load->model('Mlaporan'); 
    	$this->load->model('Mkontes'); 
    	$this->load->library('M_pdf');
    }



	function perolehanPoin(){

		$kontes     =$this->kontes_id;

		$data['rk'] =$this->db->query("SELECT f.id_inc,f.nama,f.kota ,SUM(getPoin(ukuran,ms_juara_id)) poin FROM tb_juara_kontes a JOIN tb_ikan b ON a.tb_ikan_id=b.id_inc JOIN ms_juara c ON a.ms_juara_id=c.id_inc JOIN ms_kategoriikan d ON d.id_inc=b.ms_kat_id JOIN tb_peserta e ON e.id_inc=b.tb_peserta_id JOIN `ms_peserta` f ON f.id_inc=e.ms_peserta_id GROUP BY f.nama,f.kota HAVING poin >0	ORDER BY poin DESC")->result();

		$this->template->load('blank','laporan/perolehanPoin',$data);

	}

	function perolehanPoinhandling(){
		$kontes     =$this->kontes_id;
		$data['rk'] =$this->db->query("SELECT f.id_inc,f.nama,f.kota , SUM(getPoin(ukuran,ms_juara_id)) poin
										FROM tb_juara_kontes a
										JOIN tb_ikan b ON a.tb_ikan_id=b.id_inc
										JOIN ms_juara c ON a.ms_juara_id=c.id_inc
										JOIN ms_kategoriikan d ON d.id_inc=b.ms_kat_id
										JOIN tb_peserta e ON e.id_inc=b.tb_peserta_id
										JOIN `ms_handling` f ON f.id_inc=e.ms_handling_id
										WHERE a.ms_kontes_id=$kontes 
										GROUP BY f.nama,f.kota
										having poin >0
										ORDER BY poin DESC")->result();
		$this->template->load('blank','laporan/vperolehanPoinhandling',$data);
	}

 

 	function labelpeserta(){
 		$id=$this->kontes_id;
 		$data['rk']=$this->Mlaporan->getLabelserti($id);
 		$this->template->load('blank','laporan/labelpeserta',$data);
 	}

 	function cetaklabelserti(){

 		$this->load->library('M_pdf');
		$id         =$this->kontes_id;
		$data['rn'] =$this->Mlaporan->getLabelserti($id);
		$html       =$this->load->view('laporan/cetaklabelsertifikat',$data,true);
		$mpdf       = new mPDF('utf-8', array(210,290),'','','1','1','1','1');
        $mpdf->AddPage();
        $mpdf->WriteHTML($html);
         $mpdf->Output("labelsertifikat.pdf", 'I');
    }

	function sertifikat(){
		$id         =$this->kontes_id; 
		$data['rn'] =$this->Mlaporan->getLabelserti($id); 
		$this->template->load('blank','laporan/sertifikatlist',$data);
	}	 


	function cetaktandaterimaserti(){

		$id =$this->kontes_id;

		$rn =$this->Mlaporan->getLabelserti($id);
		$html ='<h3>Pengambilan Sertifikat</h3>';
		$html .='<table border="1" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th width="200px">Handling</th>
                            <th>Kota</th>
                            <th>Ikan</th>
                            <th>Penerima</th>
                            <th width="150px" colspan="2">TTD</th>
                        </tr>
                    </thead>
                    <tbody>';                     
                        $start = 0;
                        foreach ($rn as $rk)
                        {                      	
                        $cc=$start%2;
                        $kiri=$cc<>0?$start+1:null;
                        $kanan=$cc==0?$start+1:null;
                        // if( $cc>0){  $kiri=$start; }else{} 
                       $html.='<tr>
                                <td align="center">'. ++$start .'</td>
                                <td>'. $rk->handling .'</td>
                                <td>'. $rk->kota .'</td>
                                <td align="center">'. $rk->jumlah .' ekor</td>
                                <td></td>
                                <td>'.$kiri.' </td>
                                <td>'.$kanan.' </td>
                            </tr>';
                         } 
                        $html.='</tbody>
                </table>';
               // echo $html;
                $this->load->library('M_pdf');
				$mpdf       = new mPDF('utf-8', array(210,290),'','','2','2','2','2');
				$mpdf->AddPage();
				$mpdf->WriteHTML($html);
				$mpdf->Output("tandaterimasertifikat.pdf", 'I');
	}


	function cetaksertifikat($hande){

		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', '30000');
		$hand=urldecode(urldecode($hande));
		$id            =$this->kontes_id;
		$rg=$this->Mkontes->getKontesId($id);
		$t1=$rg['kontes']->tanggal_mulai;
		$t2=$rg['kontes']->tanggal_selesai;

		// echo $t1.' = '.$t2;

		if(date('my',strtotime($t1))==date('my',strtotime($t2)) ){
			$data['tanggal']=date('d',strtotime($t1)).' - '.date('d F Y',strtotime($t2));
		}else{
			$data['tanggal']=date('d F Y',strtotime($t1)).' - '.date('d F Y',strtotime($t2));
		}

		/*$data['ttdhj']     =$rg['kontes']->ttd_headjudge;
		$data['ttdch']     =$rg['kontes']->ttd_chairman;
		$data['ttdcom']    =$rg['kontes']->ttd_commite;
		$data['headjudge'] =$rg['kontes']->nama_headjudge;
		$data['chairman']  =$rg['kontes']->nama_chairman;
		$data['commite']   =$rg['kontes']->nama_commite;
		$data['kontes']    =$rg['kontes']->nama_kontes;
		$data['tempat']    =$rg['kontes']->tempat_kontes;*/
		$data['rn']        =$this->Mlaporan->getdatasertifikat($id,$hand);
		$rd                =$this->db->query("SELECT nama,kota FROM ms_handling WHERE nama='$hand'")->row();
		$label             =$rd->nama.'_'.$rd->kota.'.pdf';
		// $data['label']     =$label;
		$html =$this->load->view('laporan/templatesertifikat',$data,true);
		$mpdf =new mPDF('utf-8','A4-L','0','0','0','0','0','0');
		$img  =base_url().'back.jpg';
		$mpdf->SetWatermarkImage($img);
		$mpdf->watermarkImageAlpha = 0.9;
		$mpdf->showWatermarkImage  = false;
		$mpdf->WriteHTML($html);
		$mpdf->Output($label, 'I');
	}



	function bayarbyhandling(){
		$kontes =$this->kontes_id;
		$data['rk']=$this->Mlaporan->getBayarbyhandling($kontes);
 		$this->template->load('blank','laporan/vbayarbyhandling',$data);

	}



	function detailbayarhandling($handling){
		$kontes=$this->kontes_id;
		$data['owner']=$this->Mkontes->pesertabyhandling($kontes,$handling);
		$data['handling']=$handling;
		$this->template->load('blank','laporan/detailbayarhandling',$data);
	}

	function cetakrekapbayarhandling(){
		$kontes =$this->kontes_id;
		$data['rk']=$this->Mlaporan->getBayarbyhandling($kontes);
		$html=$this->load->view('laporan/vcetakrekapbayarhandling',$data,true);
 		$rk            =$this->db->query("SELECT nama_kontes FROM ms_kontes WHERE id_inc='$kontes'")->row();
 		$this->m_pdf->pdf->SetHTMLHeader('<table border="0" cellspacing="0" width="100%" cellpadding="3" style="font-size:10px"> <tr> <td align="center"><h3>Rekap Pembayaran Per Handling</h3></td> </tr> <tr> <td  align="center"><b>'.$rk->nama_kontes.'</b> </td> </tr> </table><hr>');
        $this->m_pdf->pdf->AddPage();
        $this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output("rekapbayarperhandling.pdf", 'I');
	}



	function cetakbayarbyhandling($handling){

		$rn            =$this->db->query("SELECT nama,kota FROM ms_handling WHERE id_inc=$handling")->row();
		$kontes        =$this->kontes_id;
		$data['owner'] =$this->Mkontes->pesertabyhandling($kontes,$handling);
		$rk            =$this->db->query("SELECT nama_kontes FROM ms_kontes WHERE id_inc='$kontes'")->row();
		$html=$this->load->view('laporan/vcetakbayarbyhandling',$data,true);
		$this->m_pdf->pdf->SetHTMLHeader('<table border="0" cellspacing="0" width="100%" cellpadding="3" style="font-size:10px"> <tr> <td align="center"><h3>Invoice '.$rn->nama.' - '.$rn->kota.'</h3></td> </tr> <tr> <td  align="center"><b>'.$rk->nama_kontes.'</b> </td> </tr> </table><hr>');
        $this->m_pdf->pdf->AddPage();
        $this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output("invoice".$rn->nama.".pdf", 'I');

	}



	function detailpoinowner($id){

		$kontes        =$this->kontes_id;
		$dd            =$this->db->query("SELECT nama, kota FROM ms_peserta WHERE id_inc=$id")->row();
		$data['owner'] =$dd->nama.' - '.$dd->kota;
		$data['id']    =$id;
		$data['rk']    =$this->Mlaporan->getdetailpoinByowner($kontes,$id);
		$this->load->view('laporan/vdetailpoinowner',$data);
	}



	function detailpoinhandling($id){

		$kontes=$this->kontes_id;

		$dd=$this->db->query("SELECT nama, kota FROM ms_handling WHERE id_inc=$id")->row();

		$data['handling']=$dd->nama.' - '.$dd->kota;

		$data['id']=$id;

		$data['rk']=$this->Mlaporan->getdetailpoinByhandling($kontes,$id);

		$this->load->view('laporan/vdetailpoinhandling',$data);

	}



	function dataJuara(){

		$kontes=$this->kontes_id;

		$data['rk']=$this->db->query("SELECT * FROM v_reguler_winner WHERE ms_kontes_id=$kontes")->result();

		$this->template->load('blank','laporan/vadatajuarareguler',$data);

	}



	function dataJuaramain(){

		$kontes=$this->kontes_id;

		$data['rk']=$this->db->query("SELECT * FROM v_main_winner WHERE ms_kontes_id=$kontes")->result();

		$this->template->load('blank','laporan/vadatajuaramain',$data);	

	}



	function cetakmainjuaraexcel(){

			$kontes=$this->kontes_id;

			$rk=$this->db->query("SELECT * FROM v_main_winner WHERE ms_kontes_id=$kontes")->result();

			header("Content-type: application/vnd-ms-excel");

			header("Content-Disposition: attachment; filename=juarautama.xls");

 

			$html="<h3>Juara Utama</h3>";

			$html.="<table  border='1'>

                    <thead>

                        <tr>
                            <th>No</th>
                            <th>ID Ikan</th>
                            <th>Variety</th>
                            <th>Size</th>
                            <th>Prize</th>
                            <th>Owner</th>
                            <th>Handling</th>
                        </tr>

                    </thead>

                    <tbody>";

                     

                        $start = 0;

                       foreach ($rk as $rk) {

                          $html.="<tr>

                                <td align='center'>". ++$start."</td>

                                <td>". $rk->no_ikan."</td>

                                <td>". $rk->nm_ikan."</td>

                                <td>". $rk->ukuran_cm." cm</td>

                                <td>". ucwords($rk->prize)."</td>
                                <td>". $rk->owner." </td>
                                <td>". $rk->handling." </td>
                            </tr>";

                             } 

                        $html.="</tbody>

                </table>";



                echo $html;

	}





	function general(){

		$kontes=$this->kontes_id;

		// rekap per ukuran

		$ukuran=$this->Mlaporan->getRekapukuran($kontes);

		$variety=$this->Mlaporan->getRekapVariety($kontes);

		$handling=$this->Mlaporan->getRekapikanhandling($kontes);

		$owner=$this->Mlaporan->getRekapikanowner($kontes);

		$data=array(

			'ukuran'=>$ukuran,

			'variety'=>$variety,

			'handling'=>$handling,

			'owner'=>$owner

			);



		$this->template->load('blank','laporan/vgeneral',$data);

	}


	function rincian_handling(){
		$rk=$this->db->query("SELECT DISTINCT nama_handling FROM ( SELECT no_ikan,ukuran,nm_ikan variety,getownerkota(ms_peserta_id) nama_owner, gethandlinkota(ms_handling_id) nama_handling,
            CASE WHEN ms_juara_id=1 THEN f.alias 
        WHEN ms_juara_id=2 THEN  f.alias 
        WHEN ms_juara_id=3 THEN  f.alias 
        WHEN ms_juara_id=10 THEN f.alias
        WHEN ms_juara_id=11 THEN 
            CONCAT(nama_champion(ukuran),' ',e.alias)
        ELSE  CONCAT( f.alias,' ',e.alias) END juara
                                FROM tb_juara_kontes a
                                JOIN tb_ikan b ON a.tb_ikan_id=b.id_inc
                                JOIN tb_peserta c ON c.id_inc=b.tb_peserta_id
                                JOIN ms_handling d ON d.id_inc=ms_handling_id
                                JOIN ms_kategoriikan e ON e.id_inc=b.ms_kat_id
                                JOIN ms_juara f ON f.id_inc=a.ms_juara_id
                                 WHERE  ms_juara_id NOT IN (4,5) 
                                ORDER BY nama_handling ASC) qwer ORDER BY nama_handling")->result();
		$data=array('handling'=>$rk);
		$html=$this->load->view('rincianhandling',$data,true);

		/*$this->m_pdf->pdf->SetHTMLHeader('<table border="0" cellspacing="0" width="100%" cellpadding="3" style="font-size:10px"> <tr> <td  align="center"> Sekehe Koi Bali 2019</b> </td> </tr> <tr> <td   align="center"><b>Daftar Juara</b></td></tr> </table><hr>');
        $this->m_pdf->pdf->AddPage();*/

		$this->m_pdf->pdf->WriteHTML($html);



        $this->m_pdf->pdf->Output("rincian.pdf", 'I');
 

	}
 	
 	function cetakpemanggilanjuara(){
 		 
        $mostpoin=$this->db->query("SELECT * FROM (
                                    SELECT TRIM(nama) nama_owner,TRIM(kota) kota,SUM(getpoin(ukuran,ms_juara_id)) poin 
                                    FROM tb_juara_kontes a 
                                    JOIN tb_ikan b ON a.tb_ikan_id=b.id_inc
                                    JOIN tb_peserta c ON c.id_inc=b.tb_peserta_id
                                    JOIN ms_peserta d ON d.id_inc=c.ms_peserta_id
                                    GROUP BY nama_owner,kota
                                    ORDER BY poin DESC,nama_owner ASC) asr LIMIT 1")->row();
        $mostentry=$this->db->query("SELECT * FROM (
                                    SELECT TRIM(nama) nama_owner,TRIM(kota) kota,COUNT(1) ikan
                                    FROM tb_ikan b 
                                    JOIN tb_peserta c ON c.id_inc=b.tb_peserta_id
                                    JOIN ms_peserta d ON d.id_inc=c.ms_peserta_id
                                    GROUP BY nama_owner,kota
                                    ORDER BY ikan DESC,nama_owner ASC ) asr LIMIT 1")->row();
        $mosthandling=$this->db->query("SELECT * FROM(
                                            SELECT TRIM(nama) nama_handling,TRIM(kota) kota,COUNT(1) ikan
                                            FROM tb_ikan b 
                                            JOIN tb_peserta c ON c.id_inc=b.tb_peserta_id
                                            JOIN ms_handling d ON d.id_inc=c.ms_handling_id
                                            GROUP BY nama_handling,kota
                                            ORDER BY ikan DESC,nama_handling ASC ) awe LIMIT 1")->row();
         $data=array(
            'mostpoin'     =>$mostpoin,
            'mostentry'    =>$mostentry,
            'mosthandling' =>$mosthandling,
        );
 		$this->load->view('pemanggilanjuara',$data);
 	}

}



