<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdaftar extends CI_Model
{

    public $table = 'tb_peserta';
    public $id = 'id_inc';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all($and)
    {
        return $this->db->query("SELECT f.id_inc kh,a.id_inc,b.nama handling,b.kota kota_handling, e.nama pemilik_ikan,e.kota kota_pemilik, no_pendaftaran kode,COUNT(tb_peserta_id) jum_ikan,SUM(biayabykontes(ukuran)) biaya,get_status_entri(a.pengguna_id) st_entri
                                    FROM tb_peserta a
                                    JOIN ms_handling b ON a.`ms_handling_id`=b.id_inc
                                    JOIN ms_peserta e ON e.id_inc=a.ms_peserta_id
                                    JOIN ms_handling_owner f ON f.ms_handling_id=a.ms_handling_id AND f.ms_peserta_id=a.ms_peserta_id
                                    LEFT JOIN tb_ikan c ON c.`tb_peserta_id`=a.`id_inc`
                                    WHERE  checkout IS NULL $and
                                    GROUP BY f.id_inc,a.`id_inc`,b.nama,b.kota,pemilik_ikan,kota_pemilik,kode")->result();

        /*return $this->db->query("SELECT f.id_inc kh,a.id_inc,b.nama handling,b.kota kota_handling, e.nama pemilik_ikan,e.kota kota_pemilik, CONCAT( DATE_FORMAT(tanggal_insert,'%m%d'),a.ms_kontes_id,LPAD( a.`id_inc`,3, '0')) kode,COUNT(tb_peserta_id) jum_ikan
                                    FROM tb_peserta a
                                    JOIN ms_handling b ON a.`ms_handling_id`=b.id_inc
                                    join ms_peserta e on e.id_inc=a.ms_peserta_id
                                    JOIN ms_handling_owner f ON f.ms_handling_id=a.ms_handling_id AND f.ms_peserta_id=a.ms_peserta_id
                                    LEFT JOIN tb_ikan c ON c.`tb_peserta_id`=a.`id_inc`
                                    WHERE a.ms_kontes_id='$id' and checkout is null $and
                                    GROUP BY f.id_inc kh,a.`id_inc`,b.nama,b.kota,pemilik_ikan,kota_pemilik,kode
                                    ORDER BY a.`id_inc` DESC")->result();*/
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id_inc', $q);
	$this->db->or_like('ms_kontes_id', $q);
	$this->db->or_like('ms_handling_id', $q);
	$this->db->or_like('pemilik_ikan', $q);
	$this->db->or_like('kota_pemilik', $q);
	$this->db->or_like('tanggal_insert', $q);
	$this->db->or_like('pengguna_id', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id_inc', $q);
	$this->db->or_like('ms_kontes_id', $q);
	$this->db->or_like('ms_handling_id', $q);
	$this->db->or_like('pemilik_ikan', $q);
	$this->db->or_like('kota_pemilik', $q);
	$this->db->or_like('tanggal_insert', $q);
	$this->db->or_like('pengguna_id', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $rk=$this->db->insert($this->table, $data);
        if($rk) {
                $this->session->set_flashdata('msg', 
                        '<div class="note note-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Berhasil </h4>
                            <p>Data telah disimpan.</p>
                        </div>');                
            } else {    
                $this->session->set_flashdata('msg', 
                        '<div class="note note-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Oppss</h4>
                            <p>Data gagal disimpan.</p>
                        </div>');    
            }
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $rk=$this->db->update($this->table, $data);
        if($rk) {
                $this->session->set_flashdata('msg', 
                        '<div class="note note-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Berhasil </h4>
                            <p>Data telah disimpan.</p>
                        </div>');                
            } else {    
                $this->session->set_flashdata('msg', 
                        '<div class="note note-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Oppss</h4>
                            <p>Data gagal disimpan.</p>
                        </div>');    
            }
    }

    // delete data
    function delete($id)
    {

        // hapus gambar
        $rp=$this->db->query("SELECT  gambar_ikan FROM tb_ikan WHERE tb_peserta_id=$id")->result();
        foreach($rp as $rp){
            @unlink($rp->gambar_ikan);
        }

        $this->db->where($this->id, $id);
        $rk=$this->db->delete($this->table);
        if($rk) {
                $this->session->set_flashdata('msg', 
                        '<div class="note note-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Berhasil </h4>
                            <p>Data telah dihapus.</p>
                        </div>');                
            } else {    
                $this->session->set_flashdata('msg', 
                        '<div class="note note-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Oppss</h4>
                            <p>Data gagal dihapus.</p>
                        </div>');    
            }
    }


    function pendaftarbyid($id){
        return $this->db->query("SELECT c.id_inc id_handling,d.id_inc id_peserta,a.id_inc,nama_kontes,c.nama handling,c.kota kota_handling,d.nama pemilik_ikan,d.kota kota_pemilik,DATE_FORMAT(a.tanggal_insert,'%d %M %Y, %H:%i:%s') tanggal_daftar,getNamaPengguna(a.pengguna_id) nama_daftar,no_pendaftaran kode,checkout,ms_handling_id
                                    FROM tb_peserta a
                                    JOIN ms_kontes b ON a.ms_kontes_id=b.id_inc
                                    JOIN ms_handling c ON c.id_inc=a.ms_handling_id
                                    join ms_peserta d on d.id_inc=a.ms_peserta_id
                                    WHERE a.id_inc=$id and checkout is null")->row();
    }

    function sewabakbypeserta($id){
        return $this->db->query("SELECT nama_bak,biaya,jumlah_bak,biaya*jumlah_bak total
                                FROM ms_bakkontes a
                                LEFT JOIN tb_sewabak b ON a.`id_inc`=b.ms_bak_id
                                WHERE tb_peserta_id=$id")->result();
    }


    function listikanbypeserta($id){
        // $kontes=$this->session->userdata('kontes_id');
        return $this->db->query("SELECT a.id_inc,nm_ikan nm_jenis, ukuran,gambar_ikan,asal,biayabykontes(ukuran) biaya,gender
                                    FROM tb_ikan a
                                    JOIN ms_kategoriikan b ON a.ms_kat_id=b.id_inc
                                    WHERE tb_peserta_id='$id'
                                    ORDER BY a.id_inc DESC")->result();
    }

    function listbakbypeserta($id){
        return $this->db->query("SELECT nama_bak,jumlah_bak,biaya,jumlah_bak*biaya  total
                                                FROM tb_sewabak a
                                                JOIN ms_bakkontes b ON a.ms_bak_id=b.id_inc
                                                WHERE tb_peserta_id='$id'")->result();
    }
}

/* End of file Mdaftar.php */
/* Location: ./application/models/Mdaftar.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-12-06 14:44:31 */
/* http://harviacode.com */