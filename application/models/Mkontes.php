<?php
if (!defined('BASEPATH'))
   exit('No direct script access allowed');


class Mkontes extends CI_Model

{
 public $table = 'ms_kontes'; 
 public $id = 'id_inc'; 
 public $order = 'DESC';

 function __construct() {
    parent::__construct(); 
    }


    // get all

    function get_all(){    

        return $this->db->query("SELECT id_inc,nama_kontes,DATE_FORMAT(tanggal_mulai,'%d-%m-%Y') tanggal_mulai,DATE_FORMAT(tanggal_selesai,'%d-%m-%Y') tanggal_selesai,tempat_kontes,IF(status_kontes,'Buka','Tutup') pendaftaran FROM ms_kontes where is_deleted is null ORDER BY id_inc DESC")->result();

    }

    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    

    // get total rows

    function total_rows($q = NULL) {
        $this->db->like('id_inc', $q); 
        $this->db->or_like('nama_kontes', $q); 
        $this->db->or_like('tanggal_mulai', $q); 
        $this->db->or_like('tanggal_selesai', $q); 
        $this->db->or_like('tempat_kontes', $q); 
        $this->db->or_like('logo_kontes', $q); 
        $this->db->or_like('status_kontes', $q); 
        $this->db->or_like('tanggal_insert', $q); 
        $this->db->or_like('pengguna_id', $q); 
        $this->db->from($this->table);
        return $this->db->count_all_results();

    }



    // get data with limit and search

    function get_limit_data($limit, $start = 0, $q = NULL) {
         $this->db->order_by($this->id, $this->order); 
         $this->db->like('id_inc', $q); 
         $this->db->or_like('nama_kontes', $q); 
         $this->db->or_like('tanggal_mulai', $q); 
         $this->db->or_like('tanggal_selesai', $q); 
         $this->db->or_like('tempat_kontes', $q); 
         $this->db->or_like('logo_kontes', $q); 
         $this->db->or_like('status_kontes', $q); 
         $this->db->or_like('tanggal_insert', $q); 
         $this->db->or_like('pengguna_id', $q); 
         $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();

    }



    // insert data

    function insert($data)

    {

        $rk=$this->db->insert($this->table, $data);

        if($rk) {

                $this->session->set_flashdata('msg', 

                        '<div class="note note-success">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                            <h4>Berhasil </h4>

                            <p>Data telah disimpan.</p>

                        </div>');                

            } else {    

                $this->session->set_flashdata('msg', 

                        '<div class="note note-danger">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                            <h4>Oppss</h4>

                            <p>Data gagal disimpan.</p>

                        </div>');    

            }

    }



    // update data

    function update($id, $data)

    {

        $this->db->where($this->id, $id);

        $rk=$this->db->update($this->table, $data);

        if($rk) {

                $this->session->set_flashdata('msg', 

                        '<div class="note note-success">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                            <h4>Berhasil </h4>

                            <p>Data telah disimpan.</p>

                        </div>');                

            } else {    

                $this->session->set_flashdata('msg', 

                        '<div class="note note-danger">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                            <h4>Oppss</h4>

                            <p>Data gagal disimpan.</p>

                        </div>');    

            }

    }



    // delete data

    function delete($id)

    {

        /*$this->db->where($this->id, $id);

        $rk=$this->db->delete($this->table);*/

        $rk=$this->db->query("UPDATE ms_kontes SET is_deleted='1' WHERE id_inc='$id'");

        if($rk) {

                $this->session->set_flashdata('msg', 

                        '<div class="note note-success">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                            <h4>Berhasil </h4>

                            <p>Data telah dihapus.</p>

                        </div>');                

            } else {    

                $this->session->set_flashdata('msg', 

                        '<div class="note note-danger">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                            <h4>Oppss</h4>

                            <p>Data gagal dihapus.</p>

                        </div>');    

            }

    }



    function getKontesId($id){

        $data['kontes'] =$this->db->query("SELECT id_inc,nama_kontes,status_kontes,DATE_FORMAT(tanggal_mulai,'%d-%m-%Y') tanggal_mulai,DATE_FORMAT(tanggal_selesai,'%d-%m-%Y') tanggal_selesai,tempat_kontes,IF(status_kontes,'Buka','Tutup') pendaftaran,logo_kontes ,nama_headjudge,ttd_headjudge,nama_chairman,ttd_chairman,nama_commite,ttd_commite

                                            FROM ms_kontes 

                                            WHERE id_inc='$id'")->row();







        $data['bak']   =$this->db->query("SELECT id_inc,nama_bak,biaya,jumlah FROM ms_bakkontes WHERE ms_kontes_id=$id")->result();

        $data['biaya'] =$this->db->query("SELECT id_inc,CASE WHEN MIN='0' THEN  CONCAT('Up to ',MAX,' cm') ELSE  CONCAT(MIN,' cm - ',MAX,' cm') END ukuran,biaya FROM ms_biayakontes WHERE ms_kontes_id=$id order by min")->result();

        return $data;

    }



    function kontesbyid($id){

        return $this->db->query("SELECT id_inc,nama_kontes,DATE_FORMAT(tanggal_mulai,'%d-%m-%Y') mulai,DATE_FORMAT(tanggal_selesai,'%d-%m-%Y') selesai,tempat_kontes lokasi,logo_kontes,IF(status_kontes IS NULL,'Tutup','Buka' ) pendaftaran FROM ms_kontes WHERE id_inc=$id")->row();

    }



    function handlingall($where=""){

        return $this->db->query("SELECT id_inc,nama,kota FROM ms_handling ".$where)->result();

    }



    function ownerall($where=""){

        return $this->db->query("SELECT * FROM ms_peserta ".$where)->result();

    }



    function ownerbykontes(){

     return $this->db->query("SELECT * FROM ms_peserta ")->result();   

    }



    function pesertabyinc($id){
        return $this->db->query("SELECT * from tb_peserta where id_inc='$id'")->row();
    }



    function bakbykontes($id){

        return $this->db->query("SELECT id_inc,nama_bak  FROM ms_bakkontes WHERE ms_kontes_id='$id'")->result();

    }



    function pesertabykontes($and=''){

        return $this->db->query("SELECT a.id_inc,no_pendaftaran,no_kwitansi,b.nama handling,b.kota kota_handling,e.nama pemilik_ikan,e.kota kota_pemilik,COUNT(d.`tb_peserta_id`) ikan,SUM( biayabykontes(ukuran)) biaya,kd_bayar
                                FROM tb_peserta a
                                JOIN ms_handling b ON a.`ms_handling_id`=b.id_inc
                                join ms_peserta e on e.id_inc=a.ms_peserta_id
                                LEFT JOIN tb_ikan d ON d.tb_peserta_id=a.id_inc
                                WHERE  checkout='1' and kd_bayar in (0,1) $and
                                GROUP BY a.id_inc,no_kwitansi,no_pendaftaran,handling,kota_handling,pemilik_ikan,kota_pemilik,kd_bayar
                                ORDER BY kd_bayar asc, no_kwitansi DESC")->result();

    }





    function pendaftarbyid($id){

        return $this->db->query("SELECT a.id_inc,nama_kontes,c.nama handling,c.kota kota_handling,e.nama pemilik_ikan,e.kota kota_pemilik,DATE_FORMAT(a.tanggal_insert,'%d %M %Y, %H:%i:%s') tanggal_daftar,no_kwitansi kode,no_pendaftaran
                                    FROM tb_peserta a
                                    JOIN ms_kontes b ON a.ms_kontes_id=b.id_inc
                                    JOIN ms_handling c ON c.id_inc=a.ms_handling_id
                                    join ms_peserta e on e.id_inc=a.ms_peserta_id
                                    WHERE a.id_inc=$id and checkout ='1'")->row();

    }



    function sewabakbypeserta($id){

        return $this->db->query("SELECT nama_bak,biaya,jumlah_bak,biaya*jumlah_bak total

                                FROM ms_bakkontes a

                                LEFT JOIN tb_sewabak b ON a.`id_inc`=b.ms_bak_id

                                WHERE tb_peserta_id=$id")->result();

    }





    function listikanbypeserta($id){

        $kontes=$this->session->userdata('kontes_id');

        return $this->db->query("SELECT a.id_inc,nm_ikan nm_jenis, ukuran,ukuran ukuran_cm,if(gambar_ikan is null,'noimage.jpg',gambar_ikan) gambar_ikan,asal,biayabykontes(ukuran) biaya,gender,no_ikan
                                    FROM tb_ikan a
                                    JOIN ms_kategoriikan b ON a.ms_kat_id=b.id_inc
                                    WHERE tb_peserta_id='$id'
                                    ORDER BY no_ikan ASC")->result();

    }



    /*function listbakbypeserta($id){

        return $this->db->query("SELECT nama_bak,jumlah_bak,biaya,jumlah_bak*biaya  total

                                                FROM tb_sewabak a

                                                JOIN ms_bakkontes b ON a.ms_bak_id=b.id_inc

                                                WHERE tb_peserta_id='$id'")->result();

    }*/



    function ikanbykontes(){
        return $this->db->query("SELECT a.id_inc,no_pendaftaran,c.nama handling,f.nama  pemilik, no_ikan,nm_ikan jenis,nm_ikan ,kat_ikan kategori,ukuran,gender,asal,CASE WHEN gambar_ikan IS NULL THEN 'noimage.jpg' ELSE  gambar_ikan END gambar_ikan
                            FROM tb_ikan a
                            JOIN tb_peserta b ON a.tb_peserta_id=b.`id_inc`
                            JOIN ms_handling c ON c.`id_inc`=b.ms_handling_id
                            join ms_peserta f on f.id_inc=b.ms_peserta_id
                            JOIN ms_kategoriikan e ON e.id_inc=a.ms_kat_id
                            WHERE checkout='1' and kd_bayar in (0,1)
                            ORDER BY no_ikan ASC")->result();
    }





    function getkategorikontes($kontes,$kat=null,$min=null,$max=null,$breder=null,$gender=null){



        $and="";



        $and.=$breder<>''?" and asal='$breder'":"";

        $and.=$gender<>''?" and gender='$gender'":"";



        if($min<>'' && $max<>''){

            $and.=" and ukuran >= $min and ukuran <= $max";

        }



        $and.=$kat<>''?" and kat_ikan='$kat'":"";



        return $this->db->query("SELECT DISTINCT kat_ikan

                                FROM tb_ikan a

                                JOIN ms_kategoriikan b ON a.ms_kat_id=b.id_inc

                                JOIN tb_peserta c ON c.id_inc=a.tb_peserta_id

                                WHERE ms_kontes_id=$kontes AND checkout='1'  $and

                                ORDER BY kat_ikan ASC")->result();

    }



    function getkategorikontesdua($kontes,$kat=null,$min=null,$max=null,$breder=null,$gender=null){



        $and="";



        $and.=$breder<>''?" and asal='$breder'":"";

        $and.=$gender<>''?" and gender='$gender'":"";



        if($min<>'' && $max<>''){

            $and.=" and ukuran >= $min and ukuran <= $max";

        }



        $and.=$kat<>''?" and kat_ikan='$kat'":"";



        return $this->db->query("SELECT DISTINCT kat_ikan,nm_ikan

                                FROM tb_ikan a

                                JOIN ms_kategoriikan b ON a.ms_kat_id=b.id_inc

                                JOIN tb_peserta c ON c.id_inc=a.tb_peserta_id

                                WHERE ms_kontes_id=$kontes AND checkout='1'  $and

                                ORDER BY kat_ikan,nm_ikan ASC")->result();

    }



    function getkategorikontesukbesar($kontes,$var){

        return $this->db->query("SELECT DISTINCT b.*

                                FROM tb_ikan a

                                JOIN ms_kategoriikan b ON a.ms_kat_id=b.id_inc

                                JOIN tb_peserta c ON c.id_inc=a.tb_peserta_id

                                WHERE ms_kontes_id=$kontes AND checkout='1' AND REPLACE(getRangeUkuran(ms_kontes_id,ukuran),'|','') IN ($var)

                                ORDER BY id_inc ASC")->result();

    }



    function getukuranbykontes($kontes,$kat=null,$min=null,$max=null,$breder=null,$gender=null){



        $and="";



        $and.=$breder<>''?" and asal='$breder'":"";

        $and.=$gender<>''?" and gender='$gender'":"";



        if($min<>'' && $max<>''){

            $and.=" and ukuran >= $min and ukuran <= $max";

        }

        $and.=$kat<>''?" and kat_ikan='$kat'":"";

        return $this->db->query("SELECT DISTINCT kat_ikan,nm_ikan, CONCAT(REPLACE(getRangeUkuran(ms_kontes_id,ukuran),'|',' - '),' cm')  range_ukuran

                                FROM tb_ikan a

                                JOIN tb_peserta b ON b.id_inc=a.tb_peserta_id

                                JOIN ms_kategoriikan c ON c.`id_inc`=a.`ms_kat_id`

                                WHERE ms_kontes_id=$kontes AND checkout='1' $and

                                ORDER BY range_ukuran ASC,kat_ikan ASC,nm_ikan asc")->result();

    }



    function getukuranbykontesukbesar($kontes,$var){

        return $this->db->query("SELECT DISTINCT ms_kat_id, CONCAT(REPLACE(getRangeUkuran(ms_kontes_id,ukuran),'|',' - '),' cm')  range_ukuran FROM tb_ikan a JOIN tb_peserta b ON b.id_inc=a.tb_peserta_id WHERE ms_kontes_id=$kontes AND checkout='1' AND REPLACE(getRangeUkuran(ms_kontes_id,ukuran),'|','') IN ($var) ORDER BY ms_kat_id ASC,range_ukuran ASC")->result();
    }



    function getiakanbykontes($kontes){

        return $this->db->query("SELECT a.id_inc,kat_ikan,nm_ikan,ms_kat_id,gambar_ikan,CONCAT(ukuran,' cm') ukuran,gender,asal,no_ikan, CONCAT(REPLACE(getRangeUkuran(ms_kontes_id,ukuran),'|',' - '),' cm') range_ukuran

                                    FROM tb_ikan a

                                    JOIN tb_peserta c ON c.id_inc=a.tb_peserta_id

                                    join ms_kategoriikan d on d.id_inc=a.ms_kat_id

                                    WHERE ms_kontes_id=$kontes and checkout='1'

                                    ORDER BY range_ukuran ASC,kat_ikan ASC ,nm_ikan ASC")->result();

    }



    function pesertabyhandling($kontes,$handling){

        return $this->db->query("SELECT a.id_inc,nama owner,kota,no_kwitansi,CASE WHEN kd_bayar='1' THEN 'Lunas' ELSE 'Belum bayar' END bayar

                                FROM tb_peserta  a

                                JOIN ms_peserta b ON a.ms_peserta_id=b.id_inc

                                WHERE b.ms_kontes_id=$kontes AND ms_handling_id=$handling

                                ORDER BY no_kwitansi ASC")->result();

    }



    function getrekapkontes($kontes){

        $ikan=$this->db->query("SELECT COUNT(a.id_inc) res

                                        FROM tb_ikan a

                                        JOIN tb_peserta b ON a.tb_peserta_id=b.id_inc

                                        WHERE checkout='1' AND b.ms_kontes_id=$kontes")->row();

        if($ikan->res>0){

            $data['ikan']=$ikan->res;

        }else{

            $data['ikan']=0;

        }

        

        $peserta=$this->db->query("SELECT COUNT(ms_peserta_id) res FROM (

                                                SELECT DISTINCT ms_peserta_id 

                                                FROM tb_peserta WHERE checkout='1' 

                                                AND ms_kontes_id=$kontes) asd")->row();

        if($peserta->res>0){

            $data['peserta']=$peserta->res;

        }else{

            $data['peserta']=0;

        }

        



        $pt=$this->db->query("SELECT nama,kota,COUNT(a.id_inc) ikan

                                                    FROM tb_ikan a

                                                    JOIN tb_peserta b ON a.tb_peserta_id=b.id_inc

                                                    JOIN ms_peserta c ON c.id_inc=b.ms_peserta_id

                                                    WHERE checkout='1' AND b.ms_kontes_id=$kontes

                                                    GROUP BY nama,kota  ORDER BY ikan DESC")->row();

        // if(count($pt)>0){

            $data['peserta_terbanyak']=$pt;

      /*  }else{

            $data['peserta_terbanyak']=0;

        }

        */

        

        $kb=$this->db->query("SELECT kota, COUNT(a.id_inc) ikan

                                FROM tb_ikan a

                                JOIN tb_peserta b ON a.tb_peserta_id=b.id_inc

                                JOIN ms_peserta c ON c.id_inc=b.ms_peserta_id

                                WHERE checkout='1' AND b.ms_kontes_id=$kontes

                                GROUP BY kota

                                ORDER BY ikan DESC,nama ASC")->row();

        // if(count($kb)>0){

            $data['kota_terbanyak']=$kb;

        /*}else{

            $data['kota_terbanyak']=null;

        }*/

        

        

        $hb=$this->db->query("SELECT nama,kota,COUNT(a.id_inc) ikan

                                FROM tb_ikan a

                                JOIN tb_peserta b ON a.tb_peserta_id=b.id_inc

                                JOIN ms_handling c ON c.id_inc=b.ms_handling_id

                                WHERE checkout='1' AND b.ms_kontes_id=$kontes

                                GROUP BY nama,kota

                                ORDER BY ikan DESC,nama ASC")->row();

            $data['hb']=$hb;

        

        

        $ee=$this->db->query("SELECT nama_kontes FROM ms_kontes where id_inc=$kontes")->row();

        if(!empty($ee->nama_kontes)){

            $data['nama_kontes']=$ee->nama_kontes;

        }else{

            $data['nama_kontes']=0;

        }

        



        return $data;

    }



    function getvatBykontes($kontes){

        $data['bak']=$this->db->query("SELECT * FROM ms_bakkontes ")->result(); 
        $data['sewa']=$this->db->query("SELECT a.id_inc,label_bak,nama,kota ,ikan,nama_bak jenis,biaya,kd_bayar FROM tb_sewabak a JOIN ms_peserta b ON b.id_inc=a.ms_peserta_id JOIN ms_bakkontes c ON c.id_inc=a.ms_bak_id  ORDER BY jenis asc ,label_bak ASC")->result();
        return $data;
    }



    function getvatById($id){

        $data=$this->db->query("SELECT a.id_inc,label_bak,nama,kota ,ikan,nama_bak jenis,biaya,kd_bayar

                                        FROM tb_sewabak a

                                        JOIN ms_peserta b ON b.id_inc=a.ms_peserta_id

                                        JOIN ms_bakkontes c ON c.id_inc=a.ms_bak_id

                                        WHERE  a.id_inc=$id

                                        ORDER BY jenis asc ,label_bak ASC")->row();

        return $data;

    }



    function getnoikan($var){

        if(!empty($var)){



        

        $rk=$this->db->query("SELECT GROUP_CONCAT(no_ikan separator ', ') ikan FROM tb_ikan WHERE id_inc IN ($var)")->row();

        if($rk){

            echo $rk->ikan;

        }else{

            echo "";

        }

        }else{

            echo "";   

        }

    }



    function dataIkanById($id){

        return $this->db->query("SELECT a.id_inc,d.`nama` nama_handling,d.kota kota_handling,c.`nama` nama_peserta,c.kota kota_peserta,nm_ikan,ukuran,asal,gender,gambar_ikan,no_ikan

                                FROM tb_ikan a

                                JOIN tb_peserta b ON a.`tb_peserta_id`=b.`id_inc`

                                JOIN ms_peserta c ON c.`id_inc`=b.`ms_peserta_id`

                                JOIN ms_handling d ON d.`id_inc`=b.`ms_handling_id`

                                JOIN ms_kategoriikan e ON e.`id_inc`=a.`ms_kat_id`

                                WHERE a.`id_inc`=$id")->row();

    }



    function juarabyikan($id){

        $rk=$this->db->query("SELECT group_concat(nama_juara separator ', ') nama_juara

                                FROM tb_juara_kontes a

                                JOIN ms_juara b ON a.ms_juara_id=b.id_inc

                                WHERE tb_ikan_id=$id")->row();

        if($rk){

            $res=ucwords($rk->nama_juara);

        }else{

            $res='-';

        }

        return $res;

    }


   /* function getDataPembayaran(){
            // return $this->db->get('invoice_ikan')->result();
    }*/



}



/* End of file Mkontes.php */

/* Location: ./application/models/Mkontes.php */

/* Please DO NOT modify this information : */

/* Generated by Harviacode Codeigniter CRUD Generator 2017-12-05 13:26:33 */

/* http://harviacode.com */