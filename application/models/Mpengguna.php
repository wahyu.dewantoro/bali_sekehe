<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mpengguna extends CI_Model
{

    public $table = 'ms_pengguna';
    public $id = 'id_inc';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        return $this->db->query("SELECT a.id_inc,nama,username,nama_role,CASE WHEN nama_kontes IS NULL THEN 'All kontes' ELSE nama_kontes END nama_kontes
                                FROM ms_pengguna a
                                JOIN ms_role b ON a.ms_role_id=b.id_inc
                                LEFT JOIN ms_kontes c ON c.id_inc=a.ms_kontes_id
                                WHERE deleted IS NULL
                                ORDER BY ms_role_id ASC")->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id_inc', $q);
	$this->db->or_like('nama', $q);
	$this->db->or_like('username', $q);
	$this->db->or_like('password', $q);
	$this->db->or_like('ms_role_id', $q);
	$this->db->or_like('ms_kontes_id', $q);
	$this->db->or_like('ms_handling_id', $q);
	$this->db->or_like('tanggal_insert', $q);
	$this->db->or_like('deleted', $q);
	$this->db->or_like('tanggal_delete', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id_inc', $q);
	$this->db->or_like('nama', $q);
	$this->db->or_like('username', $q);
	$this->db->or_like('password', $q);
	$this->db->or_like('ms_role_id', $q);
	$this->db->or_like('ms_kontes_id', $q);
	$this->db->or_like('ms_handling_id', $q);
	$this->db->or_like('tanggal_insert', $q);
	$this->db->or_like('deleted', $q);
	$this->db->or_like('tanggal_delete', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $rk=$this->db->insert($this->table, $data);
        if($rk) {
                $this->session->set_flashdata('msg', 
                        '<div class="note note-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Berhasil </h4>
                            <p>Data telah disimpan.</p>
                        </div>');                
            } else {    
                $this->session->set_flashdata('msg', 
                        '<div class="note note-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Oppss</h4>
                            <p>Data gagal disimpan.</p>
                        </div>');    
            }
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $rk=$this->db->update($this->table, $data);
        if($rk) {
                $this->session->set_flashdata('msg', 
                        '<div class="note note-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Berhasil </h4>
                            <p>Data telah disimpan.</p>
                        </div>');                
            } else {    
                $this->session->set_flashdata('msg', 
                        '<div class="note note-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Oppss</h4>
                            <p>Data gagal disimpan.</p>
                        </div>');    
            }
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->set('tanggal_delete','CURRENT_TIMESTAMP',false);
        $this->db->set('deleted','1');
        $rk=$this->db->update($this->table);
        if($rk) {
                $this->session->set_flashdata('msg', 
                        '<div class="note note-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Berhasil </h4>
                            <p>Data telah dihapus.</p>
                        </div>');                
            } else {    
                $this->session->set_flashdata('msg', 
                        '<div class="note note-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Oppss</h4>
                            <p>Data gagal dihapus.</p>
                        </div>');    
            }
    }

}

/* End of file Mpengguna.php */
/* Location: ./application/models/Mpengguna.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-12-06 11:25:20 */
/* http://harviacode.com */